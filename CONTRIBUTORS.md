## Concept and Project Lead ##

* Mario Boley, University of Bonn and Fraunhofer IAIS


## Senior Contributors ##

* Björn Jacobs, University of Bonn and Fraunhofer IAIS

* Bo Kang, University of Bonn

* Pavel Tokmakov, University of Bonn

* Sandy Moens, University of Antwerp


## Contributors ##

* Elvin Efendiyev, University of Bonn

* Sebastian Bothe, Fraunhofer IAIS

* Vladimir Dzyuba, KU Leuven

* Hoang-Vu Nguyen, Max-Planck Institute for Informatics and Saarland University
