package de.unibonn.realkd.common.testing;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.common.workspace.Workspaces;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.propositions.PropositionalLogicFromTableBuilder;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.DataTableFromCSVFileBuilder;

public class TestConstants {

	public static final String TABLE_ID = "germany";

	public static final String GERMANY_GROUPS_TXT = "src//test//resources//data//germany//groups.txt";

	public static final String GERMANY_ATTRIBUTES_TXT = "src//test//resources//data//germany//attributes.txt";

	public static final String GERMANY_DATA_TXT = "src//test//resources//data//germany//data.txt";

	public static final String GERMANY_DATA_SAMPLE_TXT = "src//test//resources//data//germany//data_sample.txt";

	public static Workspace getGermanyWorkspace() {
		Workspace result = Workspaces.workspace();
		result.add(getGermanyDataTable());
		result.add(getGermanyPropositionalLogic());
		return result;
	}

	private static DataTable GERMANY_DATATABLE = null;

	private static PropositionalLogic GERMANY_PROPOSITIONS = null;

	private static DataTable getGermanyDataTable() {
		if (GERMANY_DATATABLE == null) {
			GERMANY_DATATABLE = createGermanyDatatable();
		}
		return GERMANY_DATATABLE;
	}

	private static PropositionalLogic getGermanyPropositionalLogic() {
		if (GERMANY_PROPOSITIONS == null) {
			GERMANY_PROPOSITIONS = createGermanyPropositions();
		}
		return GERMANY_PROPOSITIONS;
	}

	private static DataTable createGermanyDatatable() {
		DataTableFromCSVFileBuilder builder = new DataTableFromCSVFileBuilder();
		builder.setAttributeGroupCSVFilename(TestConstants.GERMANY_GROUPS_TXT)
				.setDataCSVFilename(TestConstants.GERMANY_DATA_TXT)
				.setAttributeMetadataCSVFilename(TestConstants.GERMANY_ATTRIBUTES_TXT).setId(TABLE_ID);
		try {
			return builder.build();
		} catch (DataFormatException e) {
			throw new RuntimeException(e);
		}
	}

	private static PropositionalLogic createGermanyPropositions() {
		return new PropositionalLogicFromTableBuilder().build(getGermanyDataTable());
	}

}
