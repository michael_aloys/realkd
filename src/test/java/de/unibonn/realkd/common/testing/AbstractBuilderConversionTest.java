/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.common.testing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.unibonn.realkd.common.BuilderConvertible;
import de.unibonn.realkd.common.RuntimeBuilder;

/**
 * @author Mario Boley
 *
 */
public abstract class AbstractBuilderConversionTest<T, K> {

	private K context;

	private BuilderConvertible<T, K> object;

	public AbstractBuilderConversionTest(K context,
			BuilderConvertible<T, K> object) {
		this.context = context;
		this.object = object;
	}

	@Test
	public void testToBuilderConsistency() {
		RuntimeBuilder<T, K> builder = object.toBuilder();
		Object clone = builder.build(context);
		assertEquals(object, clone);
	}

	@Test
	public void testBuilderConversionProducesEquivalentButDistinctObjects() {
		RuntimeBuilder<T, K> builder1 = object.toBuilder();
		RuntimeBuilder<T, K> builder2 = object.toBuilder();
		assertTrue(builder1 != builder2);
		assertEquals(builder1, builder2);
	}

}
