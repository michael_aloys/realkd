/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.algorithms.association;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.common.workspace.Workspaces;
import de.unibonn.realkd.data.Populations;
import de.unibonn.realkd.data.propositions.DefaultPropositionalLogic;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.SetBackedProposition;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.QualityMeasureId;

/**
 * @author Mario Boley
 *
 */
public class AssociationMiningBeamSearchTest {

	private static Workspace workspace;

	@BeforeClass
	public static void createData() throws DataFormatException {

		Proposition prop0 = new SetBackedProposition(0, ImmutableSet.of(0, 1, 2));
		Proposition prop1 = new SetBackedProposition(0, ImmutableSet.of(0, 1, 3));
		Proposition prop2 = new SetBackedProposition(0, ImmutableSet.of(0, 2));

		workspace = Workspaces.workspace();
		workspace.add(new DefaultPropositionalLogic("TestData", "", Populations.population("TestData population", "TestData population", "", ImmutableList.of("t0", "t1", "t2", "t3")),
				ImmutableList.of(prop0, prop1, prop2)));
	}

	@Test
	public void test() {
		AssociationMiningBeamSearch associationMiningBeamSearch = new AssociationMiningBeamSearch(workspace);
		associationMiningBeamSearch.findParameterByName("Number of results").setByString("1");
		// associationMiningBeamSearch.getOptimizationOrderParameter().set(
		// Association.POSITIVE_LIFT_COMPARATOR);
		associationMiningBeamSearch.findParameterByName(AssociationTargetFunctionParameter.NAME).setByString("lift");
		// .setComparator(Association.POSITIVE_LIFT_COMPARATOR);
		Collection<Pattern> resultPatterns = associationMiningBeamSearch.call();
		assertTrue(resultPatterns.size() == 1);
		Pattern pattern2 = resultPatterns.iterator().next();
		assertTrue("Pattern must have lift measure bound", pattern2.hasMeasure(QualityMeasureId.LIFT));
		assertTrue("Pattern must have positive lift", pattern2.value(QualityMeasureId.LIFT) > 0);
	}

}
