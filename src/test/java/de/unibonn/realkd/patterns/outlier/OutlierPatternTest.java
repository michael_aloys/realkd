package de.unibonn.realkd.patterns.outlier;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.data.Populations;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.DataTables;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.DefaultAttribute;

/**
 * Test for basic outlier object consistency.
 * 
 * @author Sebastian Bothe
 *
 * @since 0.1.2
 * 
 * @version 0.1.2
 */
public class OutlierPatternTest {

	private SortedSet<Integer> rows1;
	private SortedSet<Integer> rows2;
	private Set<Attribute<?>> attributes;
	private Set<Attribute<?>> attributes2;
	private DataTable table;

	@Before
	public void setUp() {

		// Prepare two rowsets row1 \subset row2
		rows1 = new TreeSet<>(Arrays.asList(new Integer[] { 1, 2, 3 }));
		rows2 = new TreeSet<>(Arrays.asList(new Integer[] { 1, 2, 3, 4 }));

		DefaultAttribute<String> attr1 = new DefaultAttribute<String>("ATTR_NAME_1", "DESCRIPTION_1",
				Arrays.asList("VALUE11", "VALUE2"), String.class);

		DefaultAttribute<String> attr2 = new DefaultAttribute<String>("ATTR_NAME_2", "DESCRIPTION_2",
				Arrays.asList("VALUE11", "VALUE2"), String.class);

		attributes = new HashSet<>();
		attributes.add(attr1);

		attributes2 = new HashSet<>();
		attributes2.addAll(attributes);
		attributes2.add(attr2);

		Population population = Populations.population("Population", 5);
		table = DataTables.table("Table","", "", population, ImmutableList.copyOf(attributes));
	}

	@Test
	public void testEqualsDifferentSupportedSetSameAttributes() {
		Outlier a = new Outlier(table, rows1, attributes, 0f, 0f);
		Outlier b = new Outlier(table, rows2, attributes, 0f, 0f);

		// Must match identity
		assertTrue(a.equals(a));
		assertTrue(b.equals(b));

		// and not equal for different rowsets
		assertTrue(!a.equals(b));
		assertTrue(!b.equals(a));
	}

	@Test
	public void testEqualsDifferentAttributes() {
		Outlier a = new Outlier(table, rows1, attributes, 0f, 0f);
		Outlier b = new Outlier(table, rows1, attributes2, 0f, 0f);

		// Must match identity
		assertTrue(a.equals(a));
		assertTrue(b.equals(b));

		// and not equal for different attributes
		assertTrue(!a.equals(b));
		assertTrue(!b.equals(a));
	}

}
