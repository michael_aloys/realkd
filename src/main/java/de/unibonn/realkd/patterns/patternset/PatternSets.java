/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.patternset;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.patterns.DefaultPattern;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternBuilder;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;

/**
 * <p>
 * Utility class providing factory method for constructing pattern set
 * descriptors.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public class PatternSets {

	public static PatternSetDescriptor createPatternSetDescriptor(Population dataArtifact, Set<Pattern> patterns) {
		return new PatternSetDescriptorImplementation(dataArtifact, patterns);
	}

	public static PatternSet createPatternSet(Population dataArtifact, Set<Pattern> patterns) {
		PatternSetDescriptor descriptor = createPatternSetDescriptor(dataArtifact, patterns);
		return new PatternSetImplementation(dataArtifact, descriptor);
	}

	private static class PatternSetDescriptorImplementation implements PatternSetDescriptor {

		private final Set<Pattern> patterns;

		private final SortedSet<Integer> supportSet;

		private final Population population;

		private PatternSetDescriptorImplementation(Population dataArtifact, Set<Pattern> patterns) {
			this.population = dataArtifact;
			this.patterns = patterns;
			this.supportSet = new TreeSet<>();
			calculateSupport();
		}

		@Override
		public SortedSet<Integer> indices() {
			return supportSet;
		}

		/**
		 * Support is union of all pattern supports or all ids in case of
		 * aggregating no pattern or at least one is not describing a
		 * sub-population.
		 * 
		 */
		private void calculateSupport() {
			if (patterns.isEmpty()) {
				this.supportSet.addAll(population.objectIds());
			} else {
				for (Pattern pattern : patterns) {
					if (pattern.descriptor() instanceof SubPopulationDescriptor) {
						this.supportSet.addAll(((SubPopulationDescriptor) pattern.descriptor()).indices());
					} else {
						this.supportSet.addAll(population.objectIds());
						return;
					}
				}
			}

		}

		@Override
		public Population population() {
			return population;
		}

		@Override
		public Set<Pattern> getPatterns() {
			return patterns;
		}

		@Override
		public PatternSetDescriptorBuilder toBuilder() {
			List<PatternBuilder> elementBuilders = patterns.stream().map(p -> p.toBuilder())
					.collect(Collectors.toList());
			return new PatternSetDescriptorBuilderImplementation(population().identifier(), elementBuilders);
		}

		@Override
		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof PatternSetDescriptorImplementation)) {
				return false;
			}
			PatternSetDescriptorImplementation otherDescriptor = (PatternSetDescriptorImplementation) other;
			return (this.population.equals(otherDescriptor.population)
					&& this.patterns.equals(otherDescriptor.patterns));
		}

	}

	private static class PatternSetDescriptorBuilderImplementation implements PatternSetDescriptorBuilder {

		private final String populationId;

		private final ArrayList<PatternBuilder> elementBuilders;

		@JsonCreator
		private PatternSetDescriptorBuilderImplementation(@JsonProperty("populationId") String populationIdentifier,
				@JsonProperty("elementBuilders") List<PatternBuilder> elementBuilders) {
			this.populationId = populationIdentifier;
			this.elementBuilders = new ArrayList<>(elementBuilders);
		}

		@JsonProperty("populationId")
		public String populationId() {
			return populationId;
		}

		@JsonProperty("elementBuilders")
		public List<PatternBuilder> elementBuilders() {
			return elementBuilders;
		}

		@Override
		public PatternSetDescriptor build(Workspace dataWorkspace) {
			checkArgument(dataWorkspace.contains(populationId), "Workspace does not contain '" + populationId + "'");
			Population artifact = (Population) dataWorkspace.get(populationId);
			Set<Pattern> elements = elementBuilders.stream().map(b -> b.build(dataWorkspace))
					.collect(Collectors.toSet());
			return createPatternSetDescriptor(artifact, elements);
		}

		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof PatternSetDescriptorBuilderImplementation)) {
				return false;
			}
			PatternSetDescriptorBuilderImplementation otherDescriptor = (PatternSetDescriptorBuilderImplementation) other;
			return (this.populationId.equals(otherDescriptor.populationId)
					&& this.elementBuilders.equals(otherDescriptor.elementBuilders));
		}

	}

	private static class PatternSetImplementation extends DefaultPattern implements PatternSet {

		private PatternSetImplementation(Population dataArtifact, PatternSetDescriptor descriptor) {
			super(dataArtifact, descriptor, ImmutableList.of());
		}

		public PatternSetDescriptor descriptor() {
			return (PatternSetDescriptor) super.descriptor();
		}

		@Override
		protected String getAdditionsForStringRepresentation() {
			StringBuilder resultBuilder = new StringBuilder();
			resultBuilder.append(super.getAdditionsForStringRepresentation());
			resultBuilder.append("elements: [\n");
			for (Pattern pattern : descriptor().getPatterns()) {
				resultBuilder.append(pattern.toString());
			}
			resultBuilder.append("]\n");
			return resultBuilder.toString();
		}

		@Override
		public PatternSetBuilder toBuilder() {
			return new PatternSetBuilderImplementation(population().identifier(), descriptor().toBuilder());
		}

	}

	private static class PatternSetBuilderImplementation implements PatternSetBuilder {

		private PatternSetDescriptorBuilder descriptorBuilder;

		private String populationId;

		@JsonCreator
		public PatternSetBuilderImplementation(@JsonProperty("populationId") String populationId,
				@JsonProperty("descriptorBuilder") PatternSetDescriptorBuilder descriptorBuilder) {
			this.populationId = populationId;
			this.descriptorBuilder = descriptorBuilder;
		}

		@JsonProperty("populationId")
		public String populationId() {
			return populationId;
		}

		@Override
		public PatternBuilder addMeasurementProcedure(MeasurementProcedure<PatternDescriptor> procedure) {
			throw new UnsupportedOperationException();
		}

		@Override
		public PatternBuilder removeMeasurementProcedure(MeasurementProcedure<PatternDescriptor> procedure) {
			throw new UnsupportedOperationException();
		}

		@Override
		public PatternSet build(Workspace workspace) {
			return new PatternSetImplementation((Population) workspace.get(populationId),
					descriptorBuilder.build(workspace));
		}

		@Override
		public PatternSetDescriptorBuilder getDescriptorBuilder() {
			return descriptorBuilder;
		}

		@Override
		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof PatternSetBuilderImplementation)) {
				return false;
			}
			PatternSetBuilderImplementation otherPatternSet = (PatternSetBuilderImplementation) other;
			return (this.populationId.equals(otherPatternSet.populationId)
					&& this.descriptorBuilder.equals(otherPatternSet.descriptorBuilder));
		}
	}

}
