/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.sequence;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;

/**
 * @author Sandy
 *
 */
public class DefaultEpisodeDescriptor implements EpisodeDescriptor {

	@Override
	public EpisodeDescriptorBuilder toBuilder() {
		return null;
	}
	

	private final PropositionalLogic propositionalLogic;
	private List<NodeDescriptor<Proposition>> rootDescriptors;

	private DefaultEpisodeDescriptor(PropositionalLogic propositionalLogic, List<NodeDescriptor<Proposition>> rootDescriptors) {
		this.propositionalLogic = propositionalLogic;
		this.rootDescriptors = rootDescriptors;
	}

	@Override
	public List<NodeDescriptor<Proposition>> rootDescriptors() {
		return rootDescriptors;
	}

	public static EpisodeDescriptorBuilder defaultSequenceDescriptorBuilder() {
		return new DefaultSequenceDescriptorBuilder(new ArrayList<NodeDescriptor<Proposition>>());
	}
	
	private static class DefaultSequenceDescriptorBuilder implements EpisodeDescriptorBuilder {

		private List<NodeDescriptor<Proposition>> rootDescriptors;

		public DefaultSequenceDescriptorBuilder(List<NodeDescriptor<Proposition>> rootDescriptors) {
			this.rootDescriptors = rootDescriptors;
		}

		@Override
		public EpisodeDescriptor build(Workspace context) {
			return new DefaultEpisodeDescriptor(context
					.getAllPropositionalLogics().get(0), rootDescriptors);
		}

		@Override
		public List<NodeDescriptor<Proposition>> rootDescriptors() {
			return rootDescriptors;
		}
		
	}
}
