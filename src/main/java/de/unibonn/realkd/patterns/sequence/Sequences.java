/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.sequence;

import static com.google.common.collect.Lists.newArrayList;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.DefaultPattern;
import de.unibonn.realkd.patterns.Measurement;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternBuilder;
import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 * @author Sandy
 *
 */
public class Sequences {
	
	public static Sequence create(SequenceDescriptor description, List<MeasurementProcedure<PatternDescriptor>> measurementProcedures) {
		return new SequenceImplementation(description,
				measurementProcedures.stream().map(p -> p.perform(description)).collect(Collectors.toList()),
				measurementProcedures);
	}
	
	private static class SequenceImplementation extends DefaultPattern implements Sequence {

		private List<MeasurementProcedure<PatternDescriptor>> proceduresBackup;

		public SequenceImplementation(SequenceDescriptor description, List<Measurement> measurements,
				List<MeasurementProcedure<PatternDescriptor>> procedures) {
			super(null, description, measurements);
			this.proceduresBackup = ImmutableList.copyOf(procedures);

		}
		
		public SequenceDescriptor descriptor() {
			return (SequenceDescriptor) super.descriptor();
		}
		
		@Override
		public SequenceBuilderImplementation toBuilder() {
			return new SequenceBuilderImplementation(descriptor().toBuilder(), proceduresBackup);
		}
		
	}
	
	public static SequenceBuilder sequenceBuilder() {
		return new SequenceBuilderImplementation(DefaultSequenceDescriptor.defaultSequenceDescriptorBuilder(), newArrayList());
	}
	
	private static class SequenceBuilderImplementation implements SequenceBuilder {

		private final SequenceDescriptorBuilder descriptorBuilder;

		private final ArrayList<MeasurementProcedure<PatternDescriptor>> procedures;

		public SequenceBuilderImplementation(SequenceDescriptorBuilder descriptorBuilder,
				List<MeasurementProcedure<PatternDescriptor>> procedures) {
			this.procedures = new ArrayList<>(procedures);
			this.descriptorBuilder = descriptorBuilder;
		}

		@Override
		public synchronized PatternBuilder addMeasurementProcedure(MeasurementProcedure<PatternDescriptor> procedure) {
			procedures.add(procedure);
			return this;
		}

		@Override
		public synchronized PatternBuilder removeMeasurementProcedure(MeasurementProcedure<PatternDescriptor> procedure) {
			procedures.remove(procedure);
			return this;
		}

		@Override
		public synchronized Sequence build(Workspace workspace) {
			SequenceDescriptor descriptor = descriptorBuilder.build(workspace);
			return Sequences.create(descriptor, procedures);
		}

		@Override
		public synchronized SequenceDescriptorBuilder getDescriptorBuilder() {
			return descriptorBuilder;
		}
		
	}
	
}
