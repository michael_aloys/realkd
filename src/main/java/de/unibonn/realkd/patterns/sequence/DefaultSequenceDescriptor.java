/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.sequence;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.stream.Collectors;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.sequences.SequenceDatabase;
import de.unibonn.realkd.patterns.util.DefaultPropositionList;
import de.unibonn.realkd.patterns.util.PropositionList;
import de.unibonn.realkd.patterns.util.PropositionListBuilder;

/**
 * @author Sandy
 *
 */
public class DefaultSequenceDescriptor implements SequenceDescriptor {
	
	public static SequenceDescriptor create(SequenceDatabase sequenceDatabase){ 
		return new DefaultSequenceDescriptor(sequenceDatabase, newArrayList());
	}
	
	public static SequenceDescriptor create(SequenceDatabase sequenceDatabase, List<PropositionList> orderedSetsDescriptors){ 
		return new DefaultSequenceDescriptor(sequenceDatabase, orderedSetsDescriptors);
	}
	
	private List<PropositionList> orderedSetsDescriptors;
	private SequenceDatabase sequenceDatabase;

	private DefaultSequenceDescriptor(SequenceDatabase sequenceDatabase, List<PropositionList> orderedSetsDescriptors) {
		this.sequenceDatabase = sequenceDatabase;
		this.orderedSetsDescriptors = orderedSetsDescriptors;
	}
	
	@Override
	public SequenceDatabase sequenceDatabase() {
		return this.sequenceDatabase;
	}
	

	@Override
	public List<PropositionList> orderedSets() {
		return orderedSetsDescriptors;
	}

	@Override
	public SequenceDescriptorBuilder toBuilder() {
		return new DefaultSequenceDescriptorBuilder(
				this.orderedSetsDescriptors.stream().map(d -> d.toBuilder()).collect(Collectors.toList()));
	}
	
	public static SequenceDescriptorBuilder defaultSequenceDescriptorBuilder() {
		return new DefaultSequenceDescriptorBuilder(newArrayList(DefaultPropositionList.defaultPropositionListBuilder()));
	}
	
	private static class DefaultSequenceDescriptorBuilder implements SequenceDescriptorBuilder {

		private List<PropositionListBuilder> orderedSetBuilders;

		public DefaultSequenceDescriptorBuilder(List<PropositionListBuilder> orderedSetBuilders) {
			this.orderedSetBuilders = orderedSetBuilders;
		}

		@Override
		public SequenceDescriptor build(Workspace context) {
			return new DefaultSequenceDescriptor((SequenceDatabase) context.getAllPropositionalLogics().get(0), orderedSetBuilders.stream()
					.map(builder -> builder.build(context)).collect(Collectors.toList()));
		}

		@Override
		public List<PropositionListBuilder> orderedSetsBuilders() {
			return orderedSetBuilders;
		}
		
	}
	
}
