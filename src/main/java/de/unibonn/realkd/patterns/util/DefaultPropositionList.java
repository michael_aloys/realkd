/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.util;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.Proposition;

/**
 * @author Sandy
 *
 */
public class DefaultPropositionList implements PropositionList {
	
	public static PropositionList emptyPropositionList() {
		return new DefaultPropositionList(newArrayList());
	}
	
	public static PropositionList newPropositionList(List<Proposition> propositions) {
		return new DefaultPropositionList(propositions);
	}

	private List<Proposition> propositions;

	public DefaultPropositionList(List<Proposition> propositions) {
		this.propositions = ImmutableList.copyOf(propositions);
	}
	
	@Override
	public List<Proposition> propositions() {
		return this.propositions;
	}

	@Override
	public PropositionListBuilder toBuilder() {
		return new DefaultPropositionListBuilder(
				this.propositions.stream().map(p -> p.getId()).collect(Collectors.toList()));
	}
	
	@Override
	public boolean isEmpty() {
		return this.propositions().isEmpty();
	}

	@Override
	public PropositionList getSpecialization(Proposition augmentation) {
		List<Proposition> propositions = Lists.newArrayList(this.propositions());
		propositions.add(augmentation); 
		return new DefaultPropositionList(ImmutableList.copyOf(propositions));
	}

	@Override
	public PropositionList getGeneralization(Proposition reductionElement) {
		if (!this.propositions().contains(reductionElement)) {
			throw new IllegalArgumentException("reduction element not part of description");
		}
		List<Proposition> propositions = Lists.newArrayList(this.propositions());
		propositions.remove(reductionElement);
		return new DefaultPropositionList(ImmutableList.copyOf(propositions));
	}
	
	public static PropositionListBuilder defaultPropositionListBuilder() {
		return new DefaultPropositionListBuilder(newArrayList());
	}
	
	private static class DefaultPropositionListBuilder implements PropositionListBuilder {

		private List<Integer> elementList;

		public DefaultPropositionListBuilder(List<Integer> elementList) {
			this.elementList = elementList;
		}
		
		@Override
		public PropositionList build(Workspace dataWorkspace) {
			List<? extends Proposition> propositions = dataWorkspace
					.getAllPropositionalLogics().get(0).getPropositions();
			return new DefaultPropositionList(elementList.stream()
					.map(i -> propositions.get(i)).collect(Collectors.toList()));
		}

		@Override
		public List<Integer> elementList() {
			return this.elementList;	
		}
		
	}

}
