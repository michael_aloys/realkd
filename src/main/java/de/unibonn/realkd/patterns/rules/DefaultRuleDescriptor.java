/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.rules;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptorBuilder;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;

/**
 * <p>
 * Composes two logical descriptors as a rule where the antecedent implies the
 * consequent in the rule descriptor.
 * </p>
 * 
 * @author Sandy Moens
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 * 
 */
public class DefaultRuleDescriptor implements RuleDescriptor {

	public static RuleDescriptor create(PropositionalLogic propositionalLogic,
			LogicalDescriptor antecedent, LogicalDescriptor consequent) {
		return new DefaultRuleDescriptor(propositionalLogic, antecedent,
				consequent);
	}

	private final PropositionalLogic propositionalLogic;

	private LogicalDescriptor antecedent;

	private LogicalDescriptor consequent;

	private DefaultRuleDescriptor(PropositionalLogic propositionalLogic,
			LogicalDescriptor antecedent, LogicalDescriptor consequent) {
		this.propositionalLogic = propositionalLogic;
		this.antecedent = antecedent;
		this.consequent = consequent;
	}

	@Override
	public PropositionalLogic getPropositionalLogic() {
		return propositionalLogic;
	}

	@Override
	public LogicalDescriptor getAntecedent() {
		return this.antecedent;
	}

	@Override
	public LogicalDescriptor getConsequent() {
		return this.consequent;
	}

//	@Override
//	public PropositionalLogic getDataArtifact() {
//		return this.propositionalLogic;
//	}
	
	public static RuleDescriptorBuilder defaultRuleDescriptorBuilder() {
		return new DefaultRuleDescriptorBuilder(LogicalDescriptors.logicalDescriptorBuilder(), LogicalDescriptors.logicalDescriptorBuilder());
	}

	private static class DefaultRuleDescriptorBuilder implements
			RuleDescriptorBuilder {

		private final LogicalDescriptorBuilder antecedentBuilder;

		private final LogicalDescriptorBuilder consequentBuilder;

		public DefaultRuleDescriptorBuilder(
				LogicalDescriptorBuilder antecedentBuilder,
				LogicalDescriptorBuilder consequentBuilder) {
			this.antecedentBuilder = antecedentBuilder;
			this.consequentBuilder = consequentBuilder;
		}

		@Override
		public DefaultRuleDescriptor build(Workspace context) {
			return new DefaultRuleDescriptor(context
					.getAllPropositionalLogics().get(0),
					antecedentBuilder.build(context),
					consequentBuilder.build(context));
		}

		@Override
		public LogicalDescriptorBuilder getAntecendentBuilder() {
			return antecedentBuilder;
		}

		@Override
		public LogicalDescriptorBuilder getConsequentBuilder() {
			return consequentBuilder;
		}

	}

	public DefaultRuleDescriptorBuilder toBuilder() {
		return new DefaultRuleDescriptorBuilder(antecedent.toBuilder(),
				consequent.toBuilder());
	}

}
