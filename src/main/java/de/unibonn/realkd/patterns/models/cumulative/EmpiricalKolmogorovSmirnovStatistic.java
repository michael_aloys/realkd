/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.models.cumulative;

import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.models.Model;
import de.unibonn.realkd.patterns.models.ModelDistanceFunction;
import de.unibonn.realkd.patterns.models.UnivariateContinuousModel;

/**
 * Measures the Kolmogorov-Smirnov statistic for an ordinal attribute by
 * iterating over the emperical data points in ascending order of their values.
 * 
 * @author Mario Boley
 * 
 * @since 0.2.2
 * 
 * @version 0.2.2
 *
 */
public enum EmpiricalKolmogorovSmirnovStatistic implements ModelDistanceFunction {

	EMPIRICAL_KOLMOGOROV_SMIRNOV_STAT;

//	private EmpiricalKolmogorovSmirnovStatistic() {
//		;
//	}

	@Override
	public QualityMeasureId getCorrespondingInterestingnessMeasure() {
		return QualityMeasureId.KOLMOGOROV_SMIRNOV_STATISTIC;
	}

	@Override
	public String toString() {
		return getCorrespondingInterestingnessMeasure().getName();
	}

//	public boolean equals(Object other) {
//		return (other instanceof EmpiricalKolmogorovSmirnovStatistic);
//	}

	@Override
	public boolean isApplicable(Model globalModel, Model localModel) {
		return (globalModel instanceof EmpiricalCumulativeDistribution
				&& localModel instanceof EmpiricalCumulativeDistribution
				&& globalModel instanceof UnivariateContinuousModel && localModel instanceof UnivariateContinuousModel);
	}

	@Override
	public double distance(Model g, Model l) {
		OrdinalAttribute<?> target = (OrdinalAttribute<?>) l.attributes().get(0);
		double maxDiff = 0.0;
		int subCumCount = 0;
		int globalNonMissingCount = target.sortedNonMissingRowIndices().size();
		int localNonMissingCount = (int) l.rows().stream().filter(obj -> !target.valueMissing(obj)).count();
		if (localNonMissingCount == 0) {
			return Double.NaN;
		}
		for (int i = 0; i < globalNonMissingCount; i++) {
			int obj = target.sortedNonMissingRowIndices().get(i);
			if (l.rows().contains(obj)) {
				subCumCount++;
			}
			double diff = Math.abs((double) i / globalNonMissingCount - (double) subCumCount / localNonMissingCount);
			maxDiff = Math.max(maxDiff, diff);
		}
		return maxDiff;
	}

}
