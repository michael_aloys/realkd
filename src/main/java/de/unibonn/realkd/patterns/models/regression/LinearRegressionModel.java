/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.models.regression;

import static de.unibonn.realkd.patterns.MeasurementImplementation.measurement;

import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Measurement;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.models.DefaultModel;
import de.unibonn.realkd.patterns.models.ModelDistanceFunction;
import de.unibonn.realkd.patterns.models.ModelFactory;

/**
 * <p>
 * Regression model of one metric variable (regressand) given as a linear
 * function from one other metric variable (covariate). The function is defined
 * by two parameters: slope and intercept.
 * </p>
 * 
 * @author Elvin Evendijevs
 * 
 * @since 0.0.1
 * 
 * @version 0.3.0
 *
 */
public class LinearRegressionModel extends DefaultModel {

	private final double slope;

	private final double intercept;

	private final ImmutableList<Measurement> descriptiveMeasurements;

	private final ModelFactory factory;

	public Double getSlope() {
		return this.slope;
	}

	public Double getIntercept() {
		return this.intercept;
	}

	public static final ModelDistanceFunction COSINEDISTANCE = AngularDistanceOfRegressionSlopes.INSTANCE;

	// public LinearRegressionModel(DataTable dataTable, List<Attribute<?>>
	// attributes, double slope, double intercept) {
	// this(dataTable, attributes, dataTable.population().objectIds(), slope,
	// intercept);
	// }

	public LinearRegressionModel(DataTable dataTable, List<? extends Attribute<?>> attributes, Set<Integer> rows, double slope,
			double intercept, ModelFactory factory) {
		super(dataTable, attributes, rows);
		this.slope = slope;
		this.intercept = intercept;
		this.descriptiveMeasurements = ImmutableList.of(measurement(QualityMeasureId.SLOPE, slope),
				measurement(QualityMeasureId.INTERCEPT, intercept));
		this.factory = factory;
	}

	public Double predict(Double x) {
		return this.slope * x + intercept;
	}

	@Override
	public ModelFactory toFactory() {
		return factory;
		// return TheilSenLinearRegressionModelFactory.INSTANCE;
	}

	@Override
	public List<Measurement> descriptiveMeasurements() {
		return descriptiveMeasurements;
	}

	@Override
	public String toString() {
		return "LinearRegressionModel(" + attributes() + ")";
	}
}
