/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.models.gaussian;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.List;
import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Measurement;
import de.unibonn.realkd.patterns.MeasurementImplementation;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.models.ModelFactory;
import de.unibonn.realkd.patterns.models.ProbabilisticModel;
import de.unibonn.realkd.patterns.models.UnivariateContinuousModel;

public class GaussianModel extends ProbabilisticModel implements UnivariateContinuousModel {

	public static GaussianModel gaussian(DataTable dataTable, Attribute<?> attribute) {
		return new GaussianModel(dataTable, attribute);
	}

	public static GaussianModel gaussian(DataTable dataTable, Attribute<?> attribute, Set<Integer> rows) {
		return new GaussianModel(dataTable, attribute, rows);
	}

	private double mean;
	private double variance;
	private final double std;
	private double min;
	private double max;

	private final Function<Double, Double> densityFunction;

	private static class GaussianDensityFunction implements Function<Double, Double> {

		private final double mean;

		private final double normalizationFactor;

		private final double variance;

		public GaussianDensityFunction(double mean, double std) {
			this.mean = mean;
			this.normalizationFactor = (std * 2 * Math.PI);
			this.variance = std * std;
		}

		@Override
		public Double apply(Double x) {
			double shift = x - mean;
			return Math.exp(-1.0 * shift * shift / (2 * variance)) / normalizationFactor;
		}

	}

	private GaussianModel(DataTable dataTable, Attribute<?> attribute) {
		super(dataTable, ImmutableList.of(attribute));
		mean = ((MetricAttribute) attribute).mean();
		variance = ((MetricAttribute) attribute).variance();
		std = Math.sqrt(variance);
		min = ((MetricAttribute) attribute).min();
		max = ((MetricAttribute) attribute).max();
		densityFunction = new GaussianDensityFunction(mean, Math.sqrt(variance));
	}

	private GaussianModel(DataTable dataTable, Attribute<?> attribute, Set<Integer> rows) {
		super(dataTable, ImmutableList.of(attribute), rows);
		if (!(attribute instanceof MetricAttribute)) {
			throw new IllegalArgumentException("can only be instantiated with numeric attribute");
		}
		mean = getMeanOnRows((MetricAttribute) attribute);
		getMinMaxVarianceOnRows((MetricAttribute) attribute);
		std = Math.sqrt(variance);
		densityFunction = new GaussianDensityFunction(mean, Math.sqrt(variance));
	}

	private double getMeanOnRows(MetricAttribute attribute) {
		return attribute.meanOnRows(rows());
	}

	private void getMinMaxVarianceOnRows(MetricAttribute attribute) {
		double result = 0.0;
		if (rows().size() == 0) {
			variance = 0;
			min = 0;
			max = 0;
			return;
		}
		min = Double.MAX_VALUE;
		max = Double.MIN_VALUE;
		for (int rowIx : rows()) {
			if (attribute.valueMissing(rowIx)) {
				continue;
			}
			double value = attribute.value(rowIx);
			min = min(min, value);
			max = max(max, value);
			result += (mean - value) * (mean - value);
		}
		variance = result / rows().size();
	}

	public double getMean() {
		return mean;
	}

	public double std() {
		return std;
	}

	public double getVariance() {
		return variance;
	}

	public double getMin() {
		return min;
	}

	public double getMax() {
		return max;
	}

	public Function<Double, Double> getDensityFunction() {
		return densityFunction;
	}

	public List<Measurement> descriptiveMeasurements() {
		return ImmutableList.of(MeasurementImplementation.measurement(QualityMeasureId.MEAN, getMean()));
	}

	@Override
	public ModelFactory toFactory() {
		return GaussianModelFactory.INSTANCE;
	}
	
	@Override
	public String toString() {
		return "Gaussian(" + attributes() + ")";
	}

}
