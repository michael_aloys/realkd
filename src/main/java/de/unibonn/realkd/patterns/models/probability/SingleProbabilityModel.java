/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.models.probability;

import java.util.List;

import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.models.DefaultModel;
import de.unibonn.realkd.patterns.models.ModelFactory;

/**
 * <p>
 * Models the occurrence probability of a conjunction of events defined on the
 * represented attributes (each of which given as
 * {@link AttributeBasedProposition}) on a given population of the data.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 *
 */
public class SingleProbabilityModel extends DefaultModel {

	private final List<AttributeBasedProposition<?>> events;

	private final double jointProbability;

	/**
	 * @param dataTable
	 * @param attributes
	 */
	public SingleProbabilityModel(DataTable dataTable, List<Attribute<?>> attributes,
			List<AttributeBasedProposition<?>> events, double jointProbability) {
		super(dataTable, attributes);
		this.events = events;
		this.jointProbability = jointProbability;
	}

	/**
	 * @return the events, the conjunction of which is modeled by this
	 */
	public List<AttributeBasedProposition<?>> getEvents() {
		return events;
	}

	/**
	 * @return the joint probability for the conjunction of all individual
	 *         events
	 */
	public double getJointProbability() {
		return jointProbability;
	}

	@Override
	public ModelFactory toFactory() {
		throw new UnsupportedOperationException();
	}

}
