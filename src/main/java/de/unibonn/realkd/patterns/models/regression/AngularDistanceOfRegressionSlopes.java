/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.models.regression;

import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.models.Model;
import de.unibonn.realkd.patterns.models.ModelDistanceFunction;

public enum AngularDistanceOfRegressionSlopes implements ModelDistanceFunction {

	INSTANCE;

	@Override
	public double distance(Model globalModel, Model localModel) {
		if (localModel.rows().size() < 2 || globalModel.rows().size() < 2) {
			return 0.0;
		}
		Double gSlope = ((LinearRegressionModel) globalModel).getSlope();
		Double lSlope = ((LinearRegressionModel) localModel).getSlope();
		if (gSlope == null || lSlope == null) {
			return 0d;
		}

		// the vector is (1, gSlope * 1)
		double globalVectorNorm = Math.sqrt(1 + Math.pow(gSlope, 2));
		// the vector is (1, lSlope * 1)
		double localVectorNorm = Math.sqrt(1 + Math.pow(lSlope, 2));

		// return the cosine between the two vectors
		double cosine = (1 + gSlope * lSlope) / (globalVectorNorm * localVectorNorm);
//		return 1 - Math.abs(cosine);
		return Math.acos(cosine)/Math.PI;
	}

	// private double euclideanDistance(DefaultModel globalModel, DefaultModel
	// localModel) {
	// Double gSlope = ((TheilSenLinearRegressionModel) globalModel).getSlope();
	// Double gIntercept = ((TheilSenLinearRegressionModel)
	// globalModel).getIntercept();
	// Double lSlope = ((TheilSenLinearRegressionModel) localModel).getSlope();
	// Double lIntercept = ((TheilSenLinearRegressionModel)
	// localModel).getIntercept();
	//
	// /*
	// * when there is no two different covariants at least in given subgroup
	// * then slope can not be estimated
	// */
	// if (lSlope == null || lIntercept == null) {
	// return 0d;
	// }
	//
	// return Math.sqrt(Math.pow(gSlope - lSlope, 2) + Math.pow(gIntercept -
	// lIntercept, 2));
	// }

	@Override
	public QualityMeasureId getCorrespondingInterestingnessMeasure() {
		return QualityMeasureId.ANGULAR_DISTANCE_OF_SLOPES;
	}

	@Override
	public boolean isApplicable(Model globalModel, Model localModel) {
		return (globalModel instanceof LinearRegressionModel && localModel instanceof LinearRegressionModel);
	}

	public String toString() {
		return "Angular distance of slopes";
	}

}
