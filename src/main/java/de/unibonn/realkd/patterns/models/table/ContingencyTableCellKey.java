/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.models.table;

import java.util.ArrayList;
import java.util.List;

/*
 * the output of Cartesian product of set of string.
 * 
 * TODO: the sole purpose of this class is by now that it has a fast 
 * (but probably incorrect) implementation of hashCode. What to do about this? 
 * 
 * -Mario
 * 
 */
public class ContingencyTableCellKey {

	private List<String> key;

	public ContingencyTableCellKey(List<String> key) {
		/*
		 * we have to sort so because we want to treat (a_v1, b_v2) same as
		 * (b_v2, a_v1)
		 */
		this.key = new ArrayList<>(key);
		// Collections.sort(this.key, new Comparator<Pair<String, String>>() {
		//
		// @Override
		// public int compare(Pair<String, String> o1, Pair<String, String> o2)
		// {
		// int _1Comp = o1._1().compareTo(o2._1());
		// if (_1Comp != 0) {
		// return _1Comp;
		// } else {
		// return o1._2().compareTo(o2._2());
		// }
		// }
		// });
	}

	@Override
	public int hashCode() {
		// String hashStr = "";
		int result = 0;
		for (String keyElement : key) {
			result = 17 * result + keyElement.hashCode();
			// hashStr += keyElement;
		}
		return result;
		// return hashStr.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		// if (this==other) {
		// return true;
		// }
		// if (!(other instanceof ContingencyTableCellKey)) {
		// return false;
		// }
		// return this.key.equals(((ContingencyTableCellKey)other).key);
		return this.hashCode() == other.hashCode();
	}

}
