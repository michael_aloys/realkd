/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.models.weibull;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.models.Model;
import de.unibonn.realkd.patterns.models.ModelDistanceFunction;

public class HellingerDistanceOfIdenticallyShapedWeibullDistributions implements ModelDistanceFunction {

	private static final HellingerDistanceOfIdenticallyShapedWeibullDistributions INSTANCE = new HellingerDistanceOfIdenticallyShapedWeibullDistributions();

	public static HellingerDistanceOfIdenticallyShapedWeibullDistributions hellingerDistanceOfIdenticallyShapedWeibullDistributions() {
		return INSTANCE;
	}

	private HellingerDistanceOfIdenticallyShapedWeibullDistributions() {
		;
	}
	
	@Override
	public QualityMeasureId getCorrespondingInterestingnessMeasure() {
		return QualityMeasureId.HELLINGER_DISTANCE;
	}

	@Override
	public boolean isApplicable(Model g, Model l) {
		if (!(g instanceof WeibullDistribution) || !(l instanceof WeibullDistribution)) {
			return false;
		}
		/**
		 * TODO should we replace equality check here by small tolerance?
		 */
		return ((WeibullDistribution) g).getShape() == ((WeibullDistribution) l).getShape();
	}

	@Override
	public double distance(Model g, Model l) {
		WeibullDistribution p = (WeibullDistribution) g;
		WeibullDistribution q = (WeibullDistribution) l;
		checkArgument(p.getShape() == q.getShape(),
				"Can only compute distance for Weibull distributions with identical shape.");
		double k = p.getShape();
		double hellingerDistance = sqrt(1.0
				- 2 * pow(p.getScale() * q.getScale(), k / 2.0) / (pow(p.getScale(), k) + pow(q.getScale(), k)));

		return hellingerDistance;
	}

	@Override
	public String toString() {
		return getCorrespondingInterestingnessMeasure().getName();
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof HellingerDistanceOfIdenticallyShapedWeibullDistributions);
	}

}