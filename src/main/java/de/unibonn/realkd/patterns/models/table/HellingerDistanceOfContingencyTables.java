/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.models.table;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

import java.util.stream.Stream;

import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.models.Model;
import de.unibonn.realkd.patterns.models.ModelDistanceFunction;

/**
 * Computes the Hellinger distance between Contingency tables of matching
 * dimensionality.
 * 
 * @author Mario Boley
 *
 * @since 0.2.2
 *
 * @version 0.2.2
 * 
 */
public enum HellingerDistanceOfContingencyTables implements ModelDistanceFunction {

	HELLINGER_DISTANCE_OF_TABLES;

	@Override
	public double distance(Model a, Model b) {
		ContingencyTable t1 = ((ContingencyTableModel) a).getProbabilities();
		ContingencyTable t2 = ((ContingencyTableModel) b).getProbabilities();
		Stream<ContingencyTableCellKey> keys = t1.getKeys().stream();

		Stream<Double> squaredDiffOfRoots = keys
				.map(k -> pow(sqrt(t1.getNormalizedValue(k)) - sqrt(t2.getNormalizedValue(k)), 2));
		Double sum = squaredDiffOfRoots.reduce(0.0, (x, y) -> x + y);
		return sqrt(sum) / sqrt(2);
	}

	@Override
	public QualityMeasureId getCorrespondingInterestingnessMeasure() {
		return QualityMeasureId.HELLINGER_DISTANCE;
	}

	@Override
	public boolean isApplicable(Model a, Model b) {
		return (a instanceof ContingencyTableModel && b instanceof ContingencyTableModel && ((ContingencyTableModel) a)
				.getProbabilities().getKeys().equals(((ContingencyTableModel) b).getProbabilities().getKeys()));
	}

}
