/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.models.table;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import de.unibonn.realkd.data.table.attribute.CategoricAttribute;

public class CategoricalKeyComputer implements ContingencyTable.ValueToBinMapper {

	private CategoricAttribute<?> attribute;

	public CategoricalKeyComputer(CategoricAttribute<?> attribute) {
		this.attribute = attribute;
	}

	@Override
	public String bin(String value) {
		return value;
//		return attribute.getName() + "_" + value;
	}

	@Override
	public List<String> allBins() {
		List<String> keys = newArrayList();
//		for (Object value : newHashSet(attribute.getCategories())) {
		for (Object value : attribute.categories()) {
			keys.add(value.toString());
//			keys.add(attribute.getName() + "_" + value);
		}
		return keys;
	}
}
