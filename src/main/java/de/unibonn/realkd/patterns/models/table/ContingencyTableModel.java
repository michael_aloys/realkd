/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.models.table;

import static com.google.common.collect.Lists.newArrayListWithCapacity;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.google.common.base.Preconditions;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.DefaultCategoricAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.models.ModelFactory;
import de.unibonn.realkd.patterns.models.ProbabilisticModel;

public class ContingencyTableModel extends ProbabilisticModel {

	private static final Logger LOGGER = Logger.getLogger(ContingencyTableModel.class.getName());

	private ContingencyTable contingencyTable;

	private final List<ContingencyTable.ValueToBinMapper> valueMappers;

	public ContingencyTableModel(DataTable dataTable, List<? extends Attribute<?>> attributes) {
		super(dataTable, attributes);
		LOGGER.fine("constructing global contingency table for attributes: " + attributes);
		valueMappers = createValueMappers();
		contingencyTable = computeTable2();
	}

	public ContingencyTableModel(DataTable dataTable, List<? extends Attribute<?>> attributes, Set<Integer> rows) {
		super(dataTable, attributes, rows);
		LOGGER.fine("constructing local contingency table on " + rows.size() + " rows for attributes: " + attributes);
		valueMappers = createValueMappers();
		contingencyTable = computeTableForRows2();
	}

	public List<String> binsOfAttribute(int i) {
		Preconditions.checkElementIndex(i, valueMappers.size());
		return valueMappers.get(i).allBins();
	}
	
	public double cellProbability(List<String> bins) {
		ContingencyTableCellKey cellKey = new ContingencyTableCellKey(bins);
		return contingencyTable.getNormalizedValue(cellKey);
	}
	
	private ContingencyTable computeTable2() {
		return computeTableForRows2(getDataTable().population().objectIds());
	}

	private ContingencyTable computeTableForRows2() {
		return computeTableForRows2(rows());
	}

	private boolean rowValueMissingForAtLeastOneAttribute(Integer rowId) {
		for (Attribute<?> attribute : attributes) {
			if (attribute.valueMissing(rowId)) {
				return true;
			}
		}
		return false;
	}

	private ContingencyTable computeTableForRows2(Set<Integer> rows) {
		ContingencyTable table = new ContingencyTable();

		for (Integer row : rows) {
			/*
			 * TODO: actually, marginal probabilities of non missing values should
			 * still increase; is there a soluton for this? -Mario
			 */
			if (rowValueMissingForAtLeastOneAttribute(row)) {
				continue;
			}
			List<String> key = computeKey(row);
			ContingencyTableCellKey cellKey = new ContingencyTableCellKey(key);
			table.incrementValue(cellKey);
		}

		return table;
	}

	/**
	 * 
	 * @param row
	 *            with present values for all attributes
	 * @return a list of keys (one for each target attribute) whose elements
	 *         identify jointly the cell in the contingency table that
	 *         corresponds the target attribute values in row
	 */
	private List<String> computeKey(Integer row) {
		List<String> key = newArrayListWithCapacity(attributes.size());
		Iterator<ContingencyTable.ValueToBinMapper> itKC = valueMappers.iterator();

		for (Attribute<?> attribute : attributes) {
			key.add(itKC.next().bin(attribute.value(row).toString()));
		}

		return key;
	}

	private List<ContingencyTable.ValueToBinMapper> createValueMappers() {
		List<ContingencyTable.ValueToBinMapper> result = newArrayListWithCapacity(attributes.size());
		for (Attribute<?> attribute : attributes) {
			ContingencyTable.ValueToBinMapper keyComputer = getKeyComputer(attribute);
			if (keyComputer != null) {
				result.add(keyComputer);
			}
		}
		return result;
	}

	private ContingencyTable.ValueToBinMapper getKeyComputer(Attribute<?> attribute) {
		if (attribute instanceof DefaultCategoricAttribute) {
			return new CategoricalKeyComputer((DefaultCategoricAttribute) attribute);
		}

		if (attribute instanceof MetricAttribute) {
			return new NumericalKeyComputer((MetricAttribute) attribute);
		}

		// TODO: Add key computation for ordinal attribute
		throw new UnsupportedOperationException(
				"key computation not supported for attributes that are neither categoric nor numeric");

	}

	public ContingencyTable getProbabilities() {
		return contingencyTable;
	}

	@Override
	public ModelFactory toFactory() {
		return ContingencyTableModelFactory.INSTANCE;
	}
	
	@Override
	public String toString() {
		return "ContingencyTable("+attributes()+")";
	}

}