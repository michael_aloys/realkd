/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.models.weibull;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.Math.exp;
import static java.lang.Math.pow;

import java.util.List;
import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.Measurement;
import de.unibonn.realkd.patterns.MeasurementImplementation;
import de.unibonn.realkd.patterns.models.DefaultModel;
import de.unibonn.realkd.patterns.models.ModelFactory;
import de.unibonn.realkd.patterns.models.UnivariateContinuousModel;

/**
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public class WeibullDistribution extends DefaultModel implements UnivariateContinuousModel {

	private final double shape;

	private final double scale;

	private final Function<Double, Double> densityFunction;

	private static class WeibullDensityFunction implements Function<Double, Double> {

		private final double k;

		private final double lambda;

		public WeibullDensityFunction(double shape, double scale) {
			this.k = shape;
			this.lambda = scale;
		}

		@Override
		public Double apply(Double x) {
			if (x <= 0.0) {
				return 0.0;
			} else {
				return (k / lambda) * pow(x / lambda, k - 1.0) * exp(-1.0 * pow(x / lambda, k));
			}
		}

	}

	public WeibullDistribution(DataTable dataTable, Attribute<?> attribute, Set<Integer> rows, double shape,
			double scale) {
		super(dataTable, ImmutableList.of(attribute), rows);
		checkArgument(attribute instanceof MetricAttribute);
		checkArgument(shape > 0, "shape paramater was not positive (" + shape + ")");
		checkArgument(scale > 0, "scale paramater was not positive (" + scale + ")");
		this.shape = shape;
		this.scale = scale;
		this.densityFunction = new WeibullDensityFunction(shape, scale);
	}

	public WeibullDistribution(DataTable dataTable, Attribute<?> attribute, double shape, double scale) {
		this(dataTable, attribute, dataTable.population().objectIds(), shape, scale);
	}

	public double getShape() {
		return shape;
	}

	public double getScale() {
		return scale;
	}

	public Function<Double, Double> getDensityFunction() {
		return densityFunction;
	}

	@Override
	public List<Measurement> descriptiveMeasurements() {
		return ImmutableList.of(MeasurementImplementation.measurement(QualityMeasureId.WEIBULL_SCALE, getScale()));
	}

	@Override
	public ModelFactory toFactory() {
		return new FixedShapeWeibullModelFactory(getShape());
	}

	@Override
	public String toString() {
		return "Weibull(" + attributes() + ")";
	}

}
