/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.models.cumulative;

import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.models.DefaultModel;
import de.unibonn.realkd.patterns.models.ModelFactory;
import de.unibonn.realkd.patterns.models.UnivariateContinuousModel;

/**
 * This is currently a stub which later should hold the computation of the
 * correlation aware binning.
 * 
 * @author Huang Vu Nguyen
 * @author Mario Boley
 * 
 * @since 0.1.1
 * @version 0.1.1
 *
 */
public enum EmpiricalCumulativeDistributionFactory implements ModelFactory {

	INSTANCE;

	public static class UnivariateCumulativeDensity extends DefaultModel implements UnivariateContinuousModel {

		private final Function<Double, Double> densityFunction;

		public UnivariateCumulativeDensity(DataTable dataTable, List<? extends Attribute<?>> attributes) {
			super(dataTable, attributes);
			MetricAttribute attr = ((MetricAttribute) attributes().get(0));
			densityFunction = x -> (double) ((MetricAttribute) attributes().get(0)).orderNumber(x)
					/ (double) attr.sortedNonMissingRowIndices().size();
		}

		public UnivariateCumulativeDensity(DataTable dataTable, List<? extends Attribute<?>> attributes, Set<Integer> rows) {
			super(dataTable, attributes, rows);
			MetricAttribute attr = ((MetricAttribute) attributes().get(0));
			Set<Integer> nonMissingIndices = rows.stream().filter(i -> !attr.valueMissing(i))
					.collect(Collectors.toSet());
			densityFunction = x -> (double) ((MetricAttribute) attributes().get(0)).orderNumberOnRows(x,
					nonMissingIndices) / (double) nonMissingIndices.size();

		}

		@Override
		public Function<Double, Double> getDensityFunction() {
			return densityFunction;
		}

		@Override
		public ModelFactory toFactory() {
			return EmpiricalCumulativeDistributionFactory.INSTANCE;
		}

	}

	public static final String STRING_NAME = "Cumulative distribution (correlation aware binning)";

	private EmpiricalCumulativeDistributionFactory() {
		;
	}

	@Override
	public Class<? extends DefaultModel> getModelClass() {
		return EmpiricalCumulativeDistribution.class;
	}

	@Override
	public DefaultModel getModel(DataTable dataTable, List<? extends Attribute<?>> attributes) {
		if (attributes.size() == 1) {
			return new UnivariateCumulativeDensity(dataTable, attributes);
		}

		return new EmpiricalCumulativeDistribution(dataTable, attributes);
	}

	@Override
	public DefaultModel getModel(DataTable dataTable, List<? extends Attribute<?>> attributes, Set<Integer> rows) {
		if (attributes.size() == 1) {
			return new UnivariateCumulativeDensity(dataTable, attributes, rows);
		}

		return new EmpiricalCumulativeDistribution(dataTable, attributes, rows);
	}

	@Override
	public boolean isApplicable(List<? extends Attribute<?>> attributes) {
		if (attributes.size() == 0) {
			return false;
		}
		for (Attribute<?> attribute : attributes) {
			if (!(attribute instanceof MetricAttribute)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return STRING_NAME;
	}

}
