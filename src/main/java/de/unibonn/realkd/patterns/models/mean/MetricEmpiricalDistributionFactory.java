/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.models.mean;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.models.DefaultModel;
import de.unibonn.realkd.patterns.models.ModelFactory;

public enum MetricEmpiricalDistributionFactory implements ModelFactory {

	INSTANCE;

	public static final String STRING_NAME = "Empirical distribution";

	private MetricEmpiricalDistributionFactory() {
		;
	}

	@Override
	public Class<? extends DefaultModel> getModelClass() {
		return MetricEmpiricalDistribution.class;
	}

	@Override
	public DefaultModel getModel(DataTable dataTable, List<? extends Attribute<?>> attributes) {
		List<MetricAttribute> metricAttributes = attributes.stream().peek(a -> {
			if (!(a instanceof MetricAttribute))
				throw new IllegalArgumentException("Can only create empirical metric distribution for metric attributes");
		}).map(a -> (MetricAttribute) a).collect(Collectors.toList());
		return MetricEmpiricalDistribution.metricEmpiricalDistribution(dataTable, metricAttributes);
	}

	@Override
	public DefaultModel getModel(DataTable dataTable, List<? extends Attribute<?>> attributes, Set<Integer> rows) {
		List<MetricAttribute> metricAttributes = attributes.stream().peek(a -> {
			if (!(a instanceof MetricAttribute))
				throw new IllegalArgumentException("Can only create empirical metric distribution for metric attributes");
		}).map(a -> (MetricAttribute) a).collect(Collectors.toList());
		return MetricEmpiricalDistribution.metricEmpiricalDistribution(dataTable, metricAttributes, rows);
	}

	@Override
	public boolean isApplicable(List<? extends Attribute<?>> attributes) {
		if (attributes.size() == 0) {
			return false;
		}
		for (Attribute<?> attribute : attributes) {
			if (!(attribute instanceof MetricAttribute)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return STRING_NAME;
	}

}
