/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.models.regression;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.models.DefaultModel;
import de.unibonn.realkd.patterns.models.ModelFactory;

public enum TheilSenLinearRegressionModelFactory implements ModelFactory {

	INSTANCE;

	private static final String STRING_NAME = "Theil Sen linear regression model";

	private TheilSenLinearRegressionModelFactory() {
		;
	}

	@Override
	public Class<? extends DefaultModel> getModelClass() {
		return LinearRegressionModel.class;
	}

	@Override
	public DefaultModel getModel(DataTable dataTable, List<? extends Attribute<?>> attributes) {
		return createModel(dataTable, attributes, dataTable.population().objectIds());
		// return new TheilSenLinearRegressionModel(dataTable, attributes);
	}

	@Override
	public DefaultModel getModel(DataTable dataTable, List<? extends Attribute<?>> attributes, Set<Integer> rows) {
		return createModel(dataTable, attributes, rows);
		// return new TheilSenLinearRegressionModel(dataTable, attributes,
		// rows);
	}

	@Override
	public boolean isApplicable(List<? extends Attribute<?>> attributes) {
		if (attributes.size() != 2) {
			return false;
		}
		for (Attribute<?> attribute : attributes) {
			if (!(attribute instanceof MetricAttribute)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return STRING_NAME;
	}

	private LinearRegressionModel createModel(DataTable dataTable, List<? extends Attribute<?>> attributes, Set<Integer> rows) {
		// ensureNumericalityOfAttributes();
		// ensureOnlyTwoAttributes();

		List<Double> covariateValues = new ArrayList<>();
		List<Double> regressandValues = new ArrayList<>();
		for (Integer row : rows) {
			if (attributes.get(0).valueMissing(row) || attributes.get(1).valueMissing(row)) {
				continue;
			}
			covariateValues.add(((MetricAttribute) attributes.get(0)).value(row));
			regressandValues.add(((MetricAttribute) attributes.get(1)).value(row));
		}

		double slope = estimateSlope(covariateValues, regressandValues);
		double intercept = estimateIntercept(slope, covariateValues, regressandValues);
		return new LinearRegressionModel(dataTable, attributes, rows, slope, intercept, this);
	}

	private Double estimateSlope(List<Double> covariateValues, List<Double> regressandValues) {
		List<Double> slopes = new ArrayList<>();
		for (int i = 0; i < covariateValues.size() - 1; i++) {
			for (int j = i + 1; j < covariateValues.size(); j++) {
				double covDiff = covariateValues.get(j) - covariateValues.get(i);
				if (covDiff != 0d) {
					slopes.add((regressandValues.get(j) - regressandValues.get(i)) / covDiff);
				}
			}
		}
		if (slopes.size() > 0) {
			return median(slopes);
		}
		return 0.0;
		// throw new IllegalArgumentException("Data does not contain two points
		// with different x-vales");
	}

	private Double estimateIntercept(double slope, List<Double> covariateValues, List<Double> regressandValues) {
		List<Double> intercepts = new ArrayList<>();
		for (int i = 0; i < covariateValues.size(); i++) {
			intercepts.add(regressandValues.get(i) - slope * covariateValues.get(i));
		}
		if (intercepts.size() > 0) {
			return median(intercepts);
		}
		return 0.0;
		// throw new IllegalArgumentException("Data does not contain two points
		// with different x-vales");
	}

	// private void ensureNumericalityOfAttributes() {
	// if (!(getAttributes().get(0) instanceof MetricAttribute)
	// || !(getAttributes().get(1) instanceof MetricAttribute)) {
	// throw new IllegalArgumentException("Both of targets attributes must be
	// numeric!");
	// }
	// }
	//
	// private void ensureOnlyTwoAttributes() {
	// if (2 != getAttributes().size()) {
	// throw new IllegalArgumentException("There must be two target attributes
	// only.");
	// }
	// }

	// TODO: call guava function instead
	private Double median(List<Double> values) {
		Double median;
		Collections.sort(values);
		if (values.size() % 2 == 0) {
			int ind = values.size() / 2 - 1;
			median = (values.get(ind) + values.get(ind + 1)) / 2.0;
		} else {
			median = values.get(values.size() / 2);
		}
		return median;
	}

}
