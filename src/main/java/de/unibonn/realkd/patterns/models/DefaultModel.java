/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.models;

import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Measurement;

/**
 * Base class for models that model some part of an underlying datatable.
 * 
 * @author Elvin Evendiyev
 * 
 * @since 0.1.0
 * 
 * @version 0.3.2
 * 
 */
public abstract class DefaultModel implements Model {

	private final DataTable dataTable;

	protected final List<? extends Attribute<?>> attributes;

	protected final Set<Integer> rows;

	public DefaultModel(DataTable dataTable, List<? extends Attribute<?>> attributes) {
		this(dataTable, attributes, dataTable.population().objectIds());
	}

	public DefaultModel(DataTable dataTable, List<? extends Attribute<?>> attributes, Set<Integer> rows) {
		if (attributes == null) {
			throw new IllegalArgumentException("attributes must not be null");
		}
		if (attributes.size() == 0) {
			throw new IllegalArgumentException("list of attributes must not be empty");
		}
		this.dataTable = dataTable;
		this.attributes = attributes;
		this.rows = rows;
	}

	public DataTable getDataTable() {
		return this.dataTable;
	}

	public List<? extends Attribute<?>> attributes() {
		return attributes;
	}

	public Set<Integer> rows() {
		return this.rows;
	}

	public List<Measurement> descriptiveMeasurements() {
		return ImmutableList.of();
	}

}
