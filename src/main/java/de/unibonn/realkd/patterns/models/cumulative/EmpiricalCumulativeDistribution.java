/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.models.cumulative;

import java.util.List;
import java.util.Set;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.models.DefaultModel;
import de.unibonn.realkd.patterns.models.ModelFactory;

/**
 * This class is a stub. It is supposed to hold information of a multivariate
 * binning model in the future that is the basis for non-parametric divergence
 * measures such as CJS.
 * 
 * @author Mario Boley
 *
 * @since 0.1.1
 * 
 * @version 0.1.1
 * 
 */
@Deprecated
public class EmpiricalCumulativeDistribution extends DefaultModel {

	public EmpiricalCumulativeDistribution(DataTable dataTable,
			List<? extends Attribute<?>> attributes) {
		super(dataTable, attributes);
	}

	public EmpiricalCumulativeDistribution(DataTable dataTable,
			List<? extends Attribute<?>> attributes, Set<Integer> rows) {
		super(dataTable, attributes, rows);
	}

	@Override
	public ModelFactory toFactory() {
		return EmpiricalCumulativeDistributionFactory.INSTANCE;
	}

}
