/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.models.gaussian;

import static java.lang.Math.exp;
import static java.lang.Math.sqrt;

import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.models.Model;
import de.unibonn.realkd.patterns.models.ModelDistanceFunction;

/**
 * Provides static factory methods and constants for Gaussian modelling.
 * 
 * @author Mario Boley
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 *
 */
public class GaussianModelling {

	static enum HellingerDistanceOfGaussians implements ModelDistanceFunction {
	
		INSTANCE;
	
		@Override
		public double distance(Model globalModel, Model localModel) {
			GaussianModel p = (GaussianModel) globalModel;
			GaussianModel q = (GaussianModel) localModel;
			double shift = q.getMean() - p.getMean();
			double squaredDistance = 1 - sqrt((2 * p.std() * q.std()) / (p.getVariance() + q.getVariance()))
					* exp(-1 * shift * shift / (4 * (p.getVariance() + q.getVariance())));
			return sqrt(squaredDistance);
		}
	
		@Override
		public QualityMeasureId getCorrespondingInterestingnessMeasure() {
			return QualityMeasureId.HELLINGER_DISTANCE;
		}
	
		@Override
		public boolean isApplicable(Model globalModel, Model localModel) {
			return GaussianModel.class.isAssignableFrom(localModel.getClass())
					&& GaussianModel.class.isAssignableFrom(localModel.getClass());
		}
	
		@Override
		public String toString() {
			return "Hellinger distance";
		}
	
	}

	static enum KLdivergenceOfGaussians implements ModelDistanceFunction {
	
		INSTANCE;
	
		@Override
		public double distance(Model globalModel, Model localModel) {
			GaussianModel globalGaussian = (GaussianModel) globalModel;
			GaussianModel localGaussian = (GaussianModel) localModel;
			if (localGaussian.getVariance() == 0.0) {
				return 0.0;
			}
			double logOfSigmaRatio = Math.log(Math.sqrt(localGaussian.getVariance()) / Math.sqrt(globalGaussian.getVariance()));
			double meanDiff = globalGaussian.getMean() - localGaussian.getMean();
			return logOfSigmaRatio + (globalGaussian.getVariance() + meanDiff * meanDiff) / (2 * localGaussian.getVariance())
					- 0.5;
		}
	
		@Override
		public QualityMeasureId getCorrespondingInterestingnessMeasure() {
			return QualityMeasureId.KL_DIVERGENCE;
		}
	
		@Override
		public boolean isApplicable(Model globalModel, Model localModel) {
			return GaussianModel.class.isAssignableFrom(localModel.getClass())
					&& GaussianModel.class.isAssignableFrom(localModel.getClass());
		}
	
		@Override
		public String toString() {
			return "KL Divergence";
		}
	
	}

	public static final ModelDistanceFunction KL_DIVERGENCE_OF_GAUSSIANS = GaussianModelling.KLdivergenceOfGaussians.INSTANCE;
	public static final ModelDistanceFunction HELLINGER_DISTANCE_OF_GAUSSIANS = GaussianModelling.HellingerDistanceOfGaussians.INSTANCE;

	private GaussianModelling() {
		;
	}

}
