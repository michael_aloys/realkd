/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns.models.mean;

import static de.unibonn.realkd.patterns.MeasurementImplementation.measurement;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Measurement;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.models.DefaultModel;
import de.unibonn.realkd.patterns.models.Model;
import de.unibonn.realkd.patterns.models.ModelDistanceFunction;
import de.unibonn.realkd.patterns.models.ModelFactory;
import de.unibonn.realkd.util.Arrays;

public class MetricEmpiricalDistribution extends DefaultModel {

	private enum MeanDeviationDistance implements ModelDistanceFunction {

		INSTANCE;

		@Override
		public double distance(Model globalModel, Model localModel) {
			if (!(globalModel instanceof MetricEmpiricalDistribution
					&& localModel instanceof MetricEmpiricalDistribution)) {
				throw new IllegalArgumentException("models must be mean deviation models");
			}

			double distance = 0;
			List<Double> globalMeans = ((MetricEmpiricalDistribution) globalModel).means();
			List<Double> localMeans = ((MetricEmpiricalDistribution) localModel).means();
			for (int i = 0; i < globalMeans.size(); i++) {
				distance += Math.abs(globalMeans.get(i) - localMeans.get(i));
			}
			return distance;
		}

		@Override
		public QualityMeasureId getCorrespondingInterestingnessMeasure() {
			return QualityMeasureId.MANHATTAN_MEAN_DEVIATION;
		}

		/**
		 * Test if both models represent mean vectors of same dimensionality.
		 */
		@Override
		public boolean isApplicable(Model globalModel, Model localModel) {
			return (globalModel instanceof MetricEmpiricalDistribution
					&& localModel instanceof MetricEmpiricalDistribution && ((MetricEmpiricalDistribution) globalModel)
							.means().size() == ((MetricEmpiricalDistribution) localModel).means().size());
		}

		public String toString() {
			return "Manhattan mean distance";
		}

	}

	private enum MedianDeviationDistance implements ModelDistanceFunction {

		INSTANCE;

		@Override
		public double distance(Model globalModel, Model localModel) {
			if (!(globalModel instanceof MetricEmpiricalDistribution
					&& localModel instanceof MetricEmpiricalDistribution && globalModel.attributes().size() == 1
					&& globalModel.attributes().equals(localModel.attributes()))) {
				throw new IllegalArgumentException("models must be mean deviation models with one attribute");
			}

			double distance = Math.abs(((MetricEmpiricalDistribution) localModel).medians().get(0)
					- ((MetricEmpiricalDistribution) globalModel).medians().get(0));
			return distance;
		}

		@Override
		public QualityMeasureId getCorrespondingInterestingnessMeasure() {
			return QualityMeasureId.MEDIAN_DEVIATION;
		}

		/**
		 * Test if both models represent mean vectors of same dimensionality.
		 */
		@Override
		public boolean isApplicable(Model globalModel, Model localModel) {
			return (globalModel instanceof MetricEmpiricalDistribution
					&& localModel instanceof MetricEmpiricalDistribution
					&& ((MetricEmpiricalDistribution) globalModel).means().size() == 1
					&& ((MetricEmpiricalDistribution) localModel).means().size() == 1);
		}

		public String toString() {
			return "Median deviation";
		}

	}

	private enum PositiveDeviation implements ModelDistanceFunction {

		INSTANCE;

		@Override
		public double distance(Model globalModel, Model localModel) {
			if (!(globalModel instanceof MetricEmpiricalDistribution
					&& localModel instanceof MetricEmpiricalDistribution && globalModel.attributes().size() == 1
					&& globalModel.attributes().equals(localModel.attributes()))) {
				throw new IllegalArgumentException("models must be mean deviation models with one attribute");
			}

			double distance = Math.max(0, ((MetricEmpiricalDistribution) localModel).means().get(0)
					- ((MetricEmpiricalDistribution) globalModel).means().get(0));
			return distance;
		}

		@Override
		public QualityMeasureId getCorrespondingInterestingnessMeasure() {
			return QualityMeasureId.POSITIVE_MEAN_DIFFERENCE;
		}

		/**
		 * Tests if both models represent one-dimensional mean values.
		 */
		@Override
		public boolean isApplicable(Model globalModel, Model localModel) {
			return (globalModel instanceof MetricEmpiricalDistribution
					&& localModel instanceof MetricEmpiricalDistribution
					&& ((MetricEmpiricalDistribution) globalModel).means().size() == 1
					&& ((MetricEmpiricalDistribution) localModel).means().size() == 1);
		}

		public String toString() {
			return "Positive mean difference";
		}

	}

	private enum NegativeDeviation implements ModelDistanceFunction {

		INSTANCE;

		@Override
		public double distance(Model globalModel, Model localModel) {
			if (!(globalModel instanceof MetricEmpiricalDistribution
					&& localModel instanceof MetricEmpiricalDistribution && globalModel.attributes().size() == 1
					&& globalModel.attributes().equals(localModel.attributes()))) {
				throw new IllegalArgumentException("models must be mean deviation models with one attribute");
			}

			return Math.max(0, ((MetricEmpiricalDistribution) globalModel).means().get(0))
					- ((MetricEmpiricalDistribution) localModel).means().get(0);
		}

		@Override
		public QualityMeasureId getCorrespondingInterestingnessMeasure() {
			return QualityMeasureId.NEGATIVE_MEAN_DIFFERENCE;
		}

		/**
		 * Tests if both models represent one-dimensional mean values.
		 */
		@Override
		public boolean isApplicable(Model globalModel, Model localModel) {
			return (globalModel instanceof MetricEmpiricalDistribution
					&& localModel instanceof MetricEmpiricalDistribution
					&& ((MetricEmpiricalDistribution) globalModel).means().size() == 1
					&& ((MetricEmpiricalDistribution) localModel).means().size() == 1);
		}

		public String toString() {
			return "Negative mean difference";
		}

	}

	public static final ModelDistanceFunction NEGATIVE_MEAN_DEVIATION = NegativeDeviation.INSTANCE;

	public static final ModelDistanceFunction MANHATTEN_MEAN_DEVIATION = MeanDeviationDistance.INSTANCE;

	public static final ModelDistanceFunction POSITIVE_MEAN_DEVIATION = PositiveDeviation.INSTANCE;
	
	public static final ModelDistanceFunction MEDIAN_DEVIATION = MedianDeviationDistance.INSTANCE;

	public static MetricEmpiricalDistribution metricEmpiricalDistribution(DataTable dataTable,
			List<MetricAttribute> attributes) {
		List<Double> means = attributes.stream().map(a -> a.mean()).collect(Collectors.toList());
		List<Double> medians = attributes.stream().map(a -> a.median()).collect(Collectors.toList());
		double[][] covMatrix = attributes.size() == 1
				? new double[][] { new double[] { ((MetricAttribute) attributes.get(0)).variance() } }
				: computeCovarianceMatrix(attributes, means, dataTable.population().objectIds());
		List<Double> aamds = attributes.stream().map(a -> a.averageAbsoluteMedianDeviation()).collect(toList());
		return new MetricEmpiricalDistribution(dataTable, attributes, means, covMatrix, medians, aamds);
	}

	public static MetricEmpiricalDistribution metricEmpiricalDistribution(DataTable dataTable,
			List<MetricAttribute> attributes, Set<Integer> rows) {
		List<Double> means = attributes.stream().map(a -> a.meanOnRows(rows)).collect(toList());
		List<Double> medians = attributes.stream().map(a -> a.medianOnRows(rows)).collect(toList());
		List<Double> aamds = attributes.stream().map(a -> a.averageAbsoluteMedianDeviationOnRows(rows))
				.collect(toList());
		return new MetricEmpiricalDistribution(dataTable, attributes, rows, means,
				computeCovarianceMatrix(attributes, means, rows), medians, aamds);
	}

	private final List<Double> means;

	private final List<Double> medians;

	private final double[][] covarianceMatrix;

	private final List<Double> avgAbsMedDevs;

	private MetricEmpiricalDistribution(DataTable dataTable, List<MetricAttribute> attributes, List<Double> means,
			double[][] covMatrix, List<Double> medians, List<Double> avgAbsMedDevs) {
		super(dataTable, attributes);
		this.means = means;
		this.covarianceMatrix = covMatrix;
		this.medians = medians;
		this.avgAbsMedDevs = avgAbsMedDevs;
	}

	private MetricEmpiricalDistribution(DataTable dataTable, List<MetricAttribute> attributes, Set<Integer> rows,
			List<Double> means, double[][] covMatrix, List<Double> medians, List<Double> avgAbsMedDevs) {
		super(dataTable, attributes, rows);
		this.means = means;
		this.covarianceMatrix = covMatrix;
		this.medians = medians;
		this.avgAbsMedDevs = avgAbsMedDevs;
	}

	private static double[][] computeCovarianceMatrix(List<MetricAttribute> attributes, List<Double> means,
			Set<Integer> rows) {
		double[][] result = new double[attributes.size()][attributes.size()];
		rows.stream().forEach(z -> {
			Function<Integer, Function<Integer, Function<Double, Double>>> addCovContributionOfRow = i -> j -> (v -> v
					+ covContributionOfEntry(z, i, j, attributes, means));
			Arrays.apply(result, addCovContributionOfRow);
		});
		Arrays.apply(result, i -> j -> v -> (v / (rows.size() - 1)));
		return result;
	}

	private static double covContributionOfEntry(int z, int i, int j, List<MetricAttribute> attributes,
			List<Double> means) {
		Optional<Double> iOpt = ((MetricAttribute) attributes.get(i)).getValueOption(z);
		Optional<Double> jOpt = ((MetricAttribute) attributes.get(j)).getValueOption(z);
		Optional<Optional<Double>> contrOpt = iOpt.map(x -> jOpt.map(y -> (x - means.get(i)) * (y - means.get(j))));
		return contrOpt.orElse(Optional.of(0.0)).orElse(0.0);
	}

	@SuppressWarnings("unchecked") // safe due to constructor
	public List<MetricAttribute> attributes() {
		return (List<MetricAttribute>) attributes;
	}

	public List<Double> means() {
		return means;
	}

	public List<Double> medians() {
		return medians;
	}

	public double[][] covarianceMatrix() {
		return covarianceMatrix;
	}

	public List<Double> averageAbsMedianDeviations() {
		return avgAbsMedDevs;
	}

	@Override
	public ModelFactory toFactory() {
		return MetricEmpiricalDistributionFactory.INSTANCE;
	}

	@Override
	public List<Measurement> descriptiveMeasurements() {
		if (attributes.size() == 1) {
			return ImmutableList.of(measurement(QualityMeasureId.MEAN, means().get(0)),
					measurement(QualityMeasureId.STD, Math.sqrt(covarianceMatrix[0][0])),
					measurement(QualityMeasureId.MEDIAN, medians.get(0)),
					measurement(QualityMeasureId.AAMD, avgAbsMedDevs.get(0)));
		}
		return ImmutableList.of();
	}

	@Override
	public String toString() {
		return "MetricEmpiricalDistribution(" + attributes() + ")";
	}

}
