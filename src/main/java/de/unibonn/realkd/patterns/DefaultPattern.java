/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.unibonn.realkd.data.Population;

public class DefaultPattern implements Pattern {

	private final List<Measure> measuresInOrder;

	private final Map<Measure, Double> measureToValues;

	private final Population population;

	private final PatternDescriptor descriptor;

	private final List<Measurement> measurements;

	public DefaultPattern(Population population,
			PatternDescriptor descriptor, List<Measurement> measurements) {
		this.population = population;
		this.descriptor = descriptor;

		this.measurements = measurements;
		this.measuresInOrder = new ArrayList<>();
		this.measureToValues = new HashMap<>();
		measurements.forEach(this::addMeasurement);
	}

	/**
	 * Recursively adds first all auxiliary measurements and then measurement
	 * itself to flattened list of measurements.
	 */
	private void addMeasurement(Measurement measurement) {
		measurement.auxiliaryMeasurements().forEach(this::addMeasurement);
		if (!measureToValues.containsKey(measurement.measure())) {
			this.measuresInOrder.add(measurement.measure());
			this.measureToValues.put(measurement.measure(),
					measurement.value());
		}
	}

	@Override
	public List<Measurement> measurements() {
		return measurements;
	}

	@Override
	public List<Measure> measures() {
		return measuresInOrder;
	}

	@Override
	public double value(Measure measure) {
		return measureToValues.get(measure);
	}

	@Override
	public boolean hasMeasure(Measure measure) {
		return measureToValues.containsKey(measure);
	}

	@Override
	public final Population population() {
		return this.population;
	}

	@Override
	public PatternDescriptor descriptor() {
		return descriptor;
	}

	@Override
	public final String toString() {
		StringBuilder resultBuilder = new StringBuilder();
		resultBuilder.append(this.getClass().getSimpleName()
				+ "(");
//		resultBuilder.append("\n");
		resultBuilder.append(descriptor.toString());
		resultBuilder.append(", ");
		resultBuilder.append(getAdditionsForStringRepresentation());

//		if (descriptor instanceof TableSubspaceDescriptor) {
//			resultBuilder.append("attributes: [\n");
//			Iterator<Attribute<?>> attributeIterator = ((TableSubspaceDescriptor) descriptor)
//					.getReferencedAttributes().iterator();
//			while (attributeIterator.hasNext()) {
//				Attribute<?> attribute = attributeIterator.next();
//				resultBuilder.append("\t" + attribute.name());
//				resultBuilder
//						.append(attributeIterator.hasNext() ? ",\n" : "\n");
//			}
//			resultBuilder.append("],\n");
//		}
		resultBuilder.append("[");
		Iterator<Measure> measureIterator = measures().iterator();
		while (measureIterator.hasNext()) {
			Measure measure = measureIterator.next();
			resultBuilder.append(measure.getName() + ": "
					+ value(measure));
			resultBuilder.append(measureIterator.hasNext() ? "," : "");
		}
		resultBuilder.append("]");
		resultBuilder.append(")");
		return resultBuilder.toString();
	}

	/**
	 * Hook that can be overriden by sub-classes in order to add information to
	 * the string representation of pattern as computed by {@link #toString} .
	 */
	protected String getAdditionsForStringRepresentation() {
		return "";
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof DefaultPattern)) {
			return false;
		}
		DefaultPattern that = (DefaultPattern) o;
		return this.measureToValues.equals(that.measureToValues)
				&& this.descriptor.equals(that.descriptor);
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 37 * result + measureToValues.hashCode();
		result = 37 * result + descriptor.hashCode();
		return result;
	}

}
