/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns;

import java.util.Collection;

import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.annotate.JsonTypeInfo.As;
import org.codehaus.jackson.annotate.JsonTypeInfo.Id;

import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.workspace.Workspace;

/**
 * <p>
 * Mutable flat representation of a pattern that can be converted to a deep
 * pattern domain object through a data workspace.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
@JsonTypeInfo(use=Id.CLASS, include=As.WRAPPER_ARRAY)
public interface PatternBuilder extends RuntimeBuilder<Pattern, Workspace> {

	@Override
	public abstract Pattern build(Workspace context);

	// /**
	// * Provides list of measurement procedures in order that they are used by
	// * builder for construction of association. Note: result cannot be used to
	// * mutate list. Use {@link #addMeasurementProcedure(MeasurementProcedure)}
	// * or {@link #removeMeasurementProcedure(MeasurementProcedure)} instead.
	// *
	// * @return list of quality measurement procedures that will be used by
	// * builder
	// */
	// public abstract List<MeasurementProcedure> getMeasurementProcedures();

	public abstract PatternDescriptorBuilder getDescriptorBuilder();
	
	public abstract PatternBuilder addMeasurementProcedure(
			MeasurementProcedure<PatternDescriptor> procedure);

	public abstract PatternBuilder removeMeasurementProcedure(
			MeasurementProcedure<PatternDescriptor> procedure);
	
	public default Collection<String> dependencyIds() {
		return getDescriptorBuilder().dependencyIds();
	}

}
