/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.patterns;

/**
 * Enumeration of measurement identifiers for which measurements can be bound to
 * patterns.
 * 
 * @see Pattern
 * 
 * @author Björn Jacobs
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.3.0
 *
 */
public enum QualityMeasureId implements Measure {

	FREQUENCY("frequency", "Relative occurance frequency of the pattern in the complete data."), 
	LIFT("lift", "Normalized difference between actual frequency and expected frequency of pattern."), 
	NEGATIVE_LIFT("negative lift", "Positive lift or zero in case actual lift is negative."), 
	ABSOLUTE_LIFT("absolute lift", "Absolute lift value."), 
	PRODUCT_OF_INDIVIDUAL_FREQUENCIES("expected frequency", "Expected frequency when assuming independenc of single propositions."), 
	DEVIATION_OF_FREQUENCY("deviation of frequency", "Difference between actual frequency and expected frequency of pattern."), 
	MANHATTAN_MEAN_DEVIATION("Manhattan mean deviation", "Manhatten distance between mean vector of global population and mean vector of subgroup."), 
	POSITIVE_MEAN_DIFFERENCE("positive mean difference", "Difference between mean target attribute value in subgroup and mean target attribute value in global population or zero of difference is negative."), 
	NEGATIVE_MEAN_DIFFERENCE("negative mean difference", ""),
	MEDIAN_DEVIATION("median deviation","Absolute difference between median target attribute value in subgroup and median target attribute value in global population."),
	TOTAL_VARIATION_DISTANCE("total variation distance", "Total variation distance between global and local distribution of target attributes."), 
	HELLINGER_DISTANCE("Hellinger distance", "Hellinger distance between global and local model of target attribute."),
	KL_DIVERGENCE("KL divergence","Kullback Leibler divergence from global to local model."),
	ANGULAR_DISTANCE_OF_SLOPES("cosine distance", "Cosine of the angle between global and local regression models."),
	OUTLIER_SCORE("outlier score", ""),
	MEAN("mean","Fitted mean value."),
	MEDIAN("median","Fitted median value."),
	GLOBAL_MEAN("global mean", "Mean target attribute value in global population."), 
	LOCAL_MEAN("local mean", "Mean target attribute value in subgroup."),
	GLOBAL_MEDIAN("global median", "Median target attribute value in global population."), 
	LOCAL_MEDIAN("local median", "Median target attribute value in subgroup."),
	SLOPE("slope", "Slope of fitted regression line."),
	INTERCEPT("intercept","Intercept (offset) of fitted regression line."),
	LOCAL_SLOPE("local slope", "Regression slope in subgroup."),
	REF_SLOPE("ref. slope", "Regression slope in reference population."),
	LOCAL_INTERCEPT("local intercept", "Regression intercept in subgroup."),
	REF_INTERCEPT("re. intercept", "Regression intercept in reference population."),
	STD("std. dev.","Standard deviation of fitted model."),
	AAMD("avg. med. dev.","Average of absolute deviations from the median of fitted model."),
	GLOBAL_STD("global std. dev.", "Squareroot of 1-norm of attribute covariance matrix in global population."),
	LOCAL_STD("local std. dev.", "Squareroot of 1-norm of attribute covariance matrix in subgroup."),
	GLOBAL_AAMD("global avg. med. dev.","Average of absolute deviations from median in global population."),
	LOCAL_AAMD("local avg. med. dev.","Average of absolute deviations from median in subgroup."),
	STD_GAIN("std. dev. gain", "The relative difference between the standard deviation of the reference model and the local model or zero if difference is negative."),
	AAMD_GAIN("avg. med. dev. gain", "The relative difference beeteen the average absolute median deviation between the global population and the subgroupgroup or zero if difference is negative."),
	CJS("cumulative Jensen-Shannon divergence", "Divergence between global and local distribution of target attributes (in terms of cumulative distribution functions)."),
	KOLMOGOROV_SMIRNOV_STATISTIC("Kolmogorov-Smirnov statistic", "Maximum difference between cumulative target distributions of global and subgroup data."),
	AREA("area", "The true size of the pattern in the data as a combination of the support and the size of the pattern."), 
	SUPPORT("support", "Absolute occurance frequency of the pattern in the complete data."), 
	CONFIDENCE("confidence", "The conditianal probability of the consequent occurring given that the antecedent occurs."), 
	WEIBULL_SCALE("scale","The scale parameter of the fitted distribution."),
	GLOBAL_WEIBULL_SCALE("global scale","The scale parameter of the Weibull distribution fitted to the global population"),
	LOCAL_WEIBULL_SCALE("local scale","The scale parameter of the Weibull distribution fitted to the local population"), 
	LOCAL_RMSE("local rmse","The root mean squared error of the local model"),
	REFERENCE_RMSE("ref. rmse","The root mean squared error of the reference model."),
	RMSE_GAIN("rmse gain", "The difference between reference rmse and local rmse or zero if this difference is negative."), 
	REF_ENTROPY("ref. entropy","Shannon entropy of reference model."),
	LOCAL_ENTROPY("local entropy","Shannon entropy of local model."),
	LOCAL_MODE_PROBABILITY("local mode probability", "Maximum probability cell of local model."),
	ENTROPY_GAIN("entropy gain","The relative difference between entropy of the reference model and the local model or zero if this difference is negative."),
	ABSOLUTE_PEARSON_GAIN("Pearson correlation gain", "The difference between the absolute Pearson correlation coefficients in subgroup and in reference data."), 
	GLOBAL_PEARSON("ref. Pearson correlation",""),
	LOCAL_PEARSON("local Pearson correlation","");
	;

	private final String name;
	
	private final String description;

	private QualityMeasureId(String name, String description) {
		this.name = name;
		this.description = description;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

}
