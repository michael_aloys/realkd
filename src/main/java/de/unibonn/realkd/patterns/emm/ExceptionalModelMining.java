/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.emm;

import static com.google.common.collect.Lists.newArrayList;
import static de.unibonn.realkd.patterns.models.weibull.HellingerDistanceOfIdenticallyShapedWeibullDistributions.hellingerDistanceOfIdenticallyShapedWeibullDistributions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

//import com.fasterxml.jackson.annotation.JsonCreator;
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.DefaultPattern;
import de.unibonn.realkd.patterns.Measure;
import de.unibonn.realkd.patterns.Measurement;
import de.unibonn.realkd.patterns.MeasurementImplementation;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.models.Model;
import de.unibonn.realkd.patterns.models.ModelDistanceFunction;
import de.unibonn.realkd.patterns.models.ProbabilisticModel;
import de.unibonn.realkd.patterns.models.cumulative.CumulativeJensenShannonDivergence;
import de.unibonn.realkd.patterns.models.cumulative.EmpiricalKolmogorovSmirnovStatistic;
import de.unibonn.realkd.patterns.models.gaussian.GaussianModelling;
import de.unibonn.realkd.patterns.models.mean.MetricEmpiricalDistribution;
import de.unibonn.realkd.patterns.models.regression.LinearRegressionModel;
import de.unibonn.realkd.patterns.models.table.HellingerDistanceOfContingencyTables;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.patterns.subgroups.SubgroupBuilder;
import de.unibonn.realkd.patterns.subgroups.Subgroups;

/**
 * <p>
 * Provides factory method for exceptional model patterns and measurement
 * procedures applicable to EMM pattern descriptors.
 * </p>
 * 
 * @author Mario Boley
 * @author Sandy Moens
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2
 *
 */
public class ExceptionalModelMining {

	public static ExceptionalModelPattern emmPattern(Subgroup descriptor,
			ModelDistanceMeasurementProcedure distanceMeasurementProcedure,
			List<MeasurementProcedure<PatternDescriptor>> additionalMeasurementProcedures) {
		List<Measurement> measurements = new ArrayList<>();
		measurements.add(FREQUENCY_FOR_EMMPATTERNS_PROCEDURE.perform(descriptor));
		measurements.add(distanceMeasurementProcedure.perform(descriptor));
		measurements.addAll(Subgroups.descriptiveModelMeasurements(descriptor));
		measurements.addAll(
				additionalMeasurementProcedures.stream().map(p -> p.perform(descriptor)).collect(Collectors.toList()));

		return new ExceptionalModelPatternImplementation(descriptor, measurements, distanceMeasurementProcedure,
				ImmutableList.copyOf(additionalMeasurementProcedures));
	}

	public static final MeasurementProcedure<PatternDescriptor> FREQUENCY_FOR_EMMPATTERNS_PROCEDURE = new MeasurementProcedure<PatternDescriptor>() {

		@Override
		public Measurement perform(PatternDescriptor descriptor) {
			Subgroup description = (Subgroup) descriptor;
			int localSize = description.extensionDescriptor().indices().size();
			int globalSize = description.extensionDescriptor().getPropositionalLogic().population().size();
			double frequency = localSize / (double) globalSize;

			return MeasurementImplementation.measurement(QualityMeasureId.FREQUENCY, frequency);
		}

		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			return (descriptor instanceof Subgroup);
		}

		@Override
		public Measure getMeasure() {
			return QualityMeasureId.FREQUENCY;
		}
	};

	public static final ModelDistanceMeasurementProcedure TVD_MEASUREMENT_PROCEDURE = new ModelDistanceFunctionBasedMeasurementProcedure(
			ProbabilisticModel.TOTALVARIATION);

	public static final ModelDistanceMeasurementProcedure KLD_OF_GAUSSIANS_MEASUREMENT_PROCEDURE = new ModelDistanceFunctionBasedMeasurementProcedure(
			GaussianModelling.KL_DIVERGENCE_OF_GAUSSIANS);

	public static final ModelDistanceMeasurementProcedure HELLINGER_OF_TABLES = new ModelDistanceFunctionBasedMeasurementProcedure(
			HellingerDistanceOfContingencyTables.HELLINGER_DISTANCE_OF_TABLES);

	public static final ModelDistanceMeasurementProcedure HELLINGER_OF_GAUSSIANS_MEASUREMENT_PROCEDURE = new ModelDistanceFunctionBasedMeasurementProcedure(
			GaussianModelling.HELLINGER_DISTANCE_OF_GAUSSIANS);

	public static final ModelDistanceMeasurementProcedure HELLINGER_DIST_FOR_IDENTICALLY_SHAPED_WEIBULL_DISTRIBUTIONS = new ModelDistanceFunctionBasedMeasurementProcedure(
			hellingerDistanceOfIdenticallyShapedWeibullDistributions());

	public static final ModelDistanceMeasurementProcedure MANHATTEN_MEAN_DEV_MEASUREMENT_PROCEDURE = new ModelDistanceFunctionBasedMeasurementProcedure(
			MetricEmpiricalDistribution.MANHATTEN_MEAN_DEVIATION);
	
	public static final ModelDistanceMeasurementProcedure MEDIAN_DEV_MEASUREMENT_PROCEDURE = new ModelDistanceFunctionBasedMeasurementProcedure(
			MetricEmpiricalDistribution.MEDIAN_DEVIATION);

	public static final ModelDistanceMeasurementProcedure POS_MEAN_DEV_PROCEDURE = new ModelDistanceFunctionBasedMeasurementProcedure(
			MetricEmpiricalDistribution.POSITIVE_MEAN_DEVIATION);

	public static final ModelDistanceMeasurementProcedure NEG_MEAN_DEV_PROCEDURE = new ModelDistanceFunctionBasedMeasurementProcedure(
			MetricEmpiricalDistribution.NEGATIVE_MEAN_DEVIATION);

	public static final ModelDistanceMeasurementProcedure COS_DIST_PROCEDURE = new ModelDistanceFunctionBasedMeasurementProcedure(
			LinearRegressionModel.COSINEDISTANCE);

	public static final ModelDistanceMeasurementProcedure CJS_DIV_PROCEDURE = new ModelDistanceFunctionBasedMeasurementProcedure(
			CumulativeJensenShannonDivergence.INSTANCE);

	public static final ModelDistanceMeasurementProcedure EMP_KOLMOGOROV_SMIRNOV = new ModelDistanceFunctionBasedMeasurementProcedure(
			EmpiricalKolmogorovSmirnovStatistic.EMPIRICAL_KOLMOGOROV_SMIRNOV_STAT);

	/**
	 * Complete collection of distance based measurement procedures defined in
	 * this class.
	 */
	public static final List<ModelDistanceMeasurementProcedure> MODEL_DISTANCE_BASED_MEASUREMENT_PROCEDURES = ImmutableList
			.of(TVD_MEASUREMENT_PROCEDURE, HELLINGER_OF_TABLES, HELLINGER_OF_GAUSSIANS_MEASUREMENT_PROCEDURE,
					HELLINGER_DIST_FOR_IDENTICALLY_SHAPED_WEIBULL_DISTRIBUTIONS,
					MANHATTEN_MEAN_DEV_MEASUREMENT_PROCEDURE, POS_MEAN_DEV_PROCEDURE, NEG_MEAN_DEV_PROCEDURE,
					COS_DIST_PROCEDURE, CJS_DIV_PROCEDURE, EMP_KOLMOGOROV_SMIRNOV);

	private static class ModelDistanceFunctionBasedMeasurementProcedure implements ModelDistanceMeasurementProcedure {

		private final ModelDistanceFunction distanceFunction;

		@JsonCreator
		public ModelDistanceFunctionBasedMeasurementProcedure(
				@JsonProperty("distanceFunction") ModelDistanceFunction distanceFunction) {
			this.distanceFunction = distanceFunction;
		}

		@SuppressWarnings("unused") // for Jackson
		public ModelDistanceFunction getDistanceFunction() {
			return distanceFunction;
		}

		@Override
		@JsonIgnore
		public final Measure getMeasure() {
			return distanceFunction.getCorrespondingInterestingnessMeasure();
		}

		@Override
		public String toString() {
			return distanceFunction.toString();
		}

		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof ModelDistanceFunctionBasedMeasurementProcedure)) {
				return false;
			}
			return (distanceFunction.equals(((ModelDistanceFunctionBasedMeasurementProcedure) other).distanceFunction));
		}

		@Override
		public boolean isApplicable(Model globalModel, Model localModel) {
			return distanceFunction.isApplicable(globalModel, localModel);
		}

		@Override
		public Measurement perform(Model g, Model l) {
			return MeasurementImplementation.measurement(getMeasure(), distanceFunction.distance(g, l));
		}

	}

	private static class ExceptionalModelPatternImplementation extends DefaultPattern
			implements ExceptionalModelPattern {

		private final Subgroup descriptor;

		private final ModelDistanceMeasurementProcedure deviationMeasurementProcedure;

		private final List<MeasurementProcedure<PatternDescriptor>> additionalMeasurementProcedureBackup;

		private ExceptionalModelPatternImplementation(Subgroup descriptor, List<Measurement> measurements,
				ModelDistanceMeasurementProcedure deviationMeasurementProcedure,
				List<MeasurementProcedure<PatternDescriptor>> additionalMeasurementProcedures) {

			super(descriptor.extensionDescriptor().getPropositionalLogic().population(),
					descriptor, measurements);

			this.descriptor = descriptor;
			this.deviationMeasurementProcedure = deviationMeasurementProcedure;
			this.additionalMeasurementProcedureBackup = additionalMeasurementProcedures;
		}

		public Subgroup descriptor() {
			return this.descriptor;
		}

		public Measure getDeviationMeasure() {
			return deviationMeasurementProcedure.getMeasure();
		}

		public ExceptionalModelPatternBuilder toBuilder() {
			return new ExceptionalModelPatternBuilderImplementation(descriptor.toBuilder(),
					this.deviationMeasurementProcedure, this.additionalMeasurementProcedureBackup);
		}
	}

	public static ExceptionalModelPatternBuilder exceptionalModelPatternBuilder() {
		return new ExceptionalModelPatternBuilderImplementation(Subgroups.subgroupDescriptorBuilder(), null,
				newArrayList());
	}

	private static class ExceptionalModelPatternBuilderImplementation implements ExceptionalModelPatternBuilder {

		private final SubgroupBuilder descriptorBuilder;

		private final ArrayList<MeasurementProcedure<PatternDescriptor>> additionalMeasurementProcedures;

		private ModelDistanceMeasurementProcedure distanceMeasurementProcedure;

		@JsonCreator
		private ExceptionalModelPatternBuilderImplementation(
				@JsonProperty("descriptorBuilder") SubgroupBuilder descriptorBuilder,
				@JsonProperty("distanceMeasurementProcedure") ModelDistanceMeasurementProcedure distanceMeasurementProcedure,
				@JsonProperty("additionalMeasurementProcedures") List<MeasurementProcedure<PatternDescriptor>> additionalMeasurementProcedures) {
			this.descriptorBuilder = descriptorBuilder;
			this.distanceMeasurementProcedure = distanceMeasurementProcedure;
			this.additionalMeasurementProcedures = new ArrayList<>(additionalMeasurementProcedures);
		}

		@SuppressWarnings("unused") // for Jackson
		public ModelDistanceMeasurementProcedure getDistanceMeasurementProcedure() {
			return distanceMeasurementProcedure;
		}

		@SuppressWarnings("unused") // for Jackson
		public void distanceMeasureProcedure(ModelDistanceMeasurementProcedure distanceMeasurementProcedure) {
			this.distanceMeasurementProcedure = distanceMeasurementProcedure;
		}

		@SuppressWarnings("unused") // for Jackson
		public List<MeasurementProcedure<PatternDescriptor>> getAdditionalMeasurementProcedures() {
			return additionalMeasurementProcedures;
		}

		@Override
		public synchronized ExceptionalModelPatternBuilder addMeasurementProcedure(
				MeasurementProcedure<PatternDescriptor> procedure) {
			additionalMeasurementProcedures.add(procedure);
			return this;
		}

		@Override
		public synchronized ExceptionalModelPatternBuilder removeMeasurementProcedure(
				MeasurementProcedure<PatternDescriptor> procedure) {
			additionalMeasurementProcedures.remove(procedure);
			return this;
		}

		@Override
		public synchronized ExceptionalModelPattern build(Workspace workspace) {
			return emmPattern(descriptorBuilder.build(workspace), distanceMeasurementProcedure,
					additionalMeasurementProcedures);
		}

		@Override
		public synchronized SubgroupBuilder getDescriptorBuilder() {
			return descriptorBuilder;
		}

		@Override
		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof ExceptionalModelPatternBuilderImplementation)) {
				return false;
			}
			ExceptionalModelPatternBuilderImplementation otherBuilder = (ExceptionalModelPatternBuilderImplementation) other;
			return (this.descriptorBuilder.equals(otherBuilder.descriptorBuilder)
					&& this.distanceMeasurementProcedure.equals(otherBuilder.distanceMeasurementProcedure)
					&& this.additionalMeasurementProcedures.equals(otherBuilder.additionalMeasurementProcedures));
		}

		// @Override
		// public List<MeasurementProcedure> getMeasurementProcedures() {
		// ArrayList<MeasurementProcedure> result = new ArrayList<>();
		// result.add(FREQUENCY_FOR_EMMPATTERNS_PROCEDURE);
		// result.add(distanceMeasurementProcedure);
		// result.addAll(additionalMeasurementProcedures);
		// return result;
		// }

	}

}
