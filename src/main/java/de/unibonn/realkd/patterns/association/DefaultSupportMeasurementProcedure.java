/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.association;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.List;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnore;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.sequences.SequenceDatabase;
import de.unibonn.realkd.data.sequences.SequenceEvent;
import de.unibonn.realkd.data.sequences.SequenceTransaction;
import de.unibonn.realkd.patterns.Measure;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.Measurement;
import de.unibonn.realkd.patterns.MeasurementImplementation;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.sequence.SequenceDescriptor;
import de.unibonn.realkd.patterns.util.PropositionList;

/**
 * Procedure for computing the support of a pattern. This is defined as: the
 * absolute occurrence frequency of the pattern in the complete data.
 * 
 * @author Sandy Moens
 * 
 * @since 0.1.2
 * 
 * @version 0.3.0
 *
 */
public enum DefaultSupportMeasurementProcedure implements MeasurementProcedure<PatternDescriptor> {

	INSTANCE;
	
	private DefaultSupportMeasurementProcedure() {
		;
	}

	@Override
	public boolean isApplicable(PatternDescriptor descriptor) {
		return LogicalDescriptor.class.isAssignableFrom(descriptor.getClass()) 
				||	SequenceDescriptor.class.isAssignableFrom(descriptor.getClass());
	}

	@JsonIgnore
	@Override
	public Measure getMeasure() {
		return QualityMeasureId.SUPPORT;
	}

	@Override
	public Measurement perform(PatternDescriptor descriptor) {

		if(LogicalDescriptor.class.isAssignableFrom(descriptor.getClass())) {
			LogicalDescriptor logicalDescriptor = (LogicalDescriptor) descriptor;
	
			double support = (double) logicalDescriptor.indices().size();
	
			return MeasurementImplementation.measurement(QualityMeasureId.SUPPORT, support);
		}
		SequenceDescriptor sequenceDescriptor = (SequenceDescriptor) descriptor;

		SequenceDatabase sequenceDatabase = sequenceDescriptor.sequenceDatabase();
		
		double support = 0;
		for(SequenceTransaction sequence: sequenceDatabase.sequences()) {
			int ix = 0;
			List<SequenceEvent> events = newArrayList(sequence.events());
			Set<Proposition> propositionsToFind = newHashSet();
			for(PropositionList orderedSet: sequenceDescriptor.orderedSets()) {
				propositionsToFind = newHashSet(orderedSet.propositions());
				while(!propositionsToFind.isEmpty() && ix < events.size()) {
					propositionsToFind.removeAll(events.get(ix).propositions());
					ix++;
				}
			}
			if(propositionsToFind.isEmpty() && ix < events.size()) {
				support++;
			}
		}

		return MeasurementImplementation.measurement(QualityMeasureId.SUPPORT, support);
	}

}
