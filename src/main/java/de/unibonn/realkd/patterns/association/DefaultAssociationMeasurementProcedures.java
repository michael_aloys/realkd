/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.association;

import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.patterns.Measure;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.Measurement;
import de.unibonn.realkd.patterns.MeasurementImplementation;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

/**
 * @author Mario Boley
 * @author Sandy Moens
 * 
 * @since 0.1.2
 * @version 0.1.2.1
 *
 */
public enum DefaultAssociationMeasurementProcedures implements MeasurementProcedure<PatternDescriptor> {

	/**
	 * Procedure for computing the area of a pattern. This is defined as: the
	 * true size of the pattern in the data as a combination of the support and
	 * the size of the pattern. This procedure computes support as an auxiliary
	 * measurement.
	 * 
	 */
	AREA(new MeasurementProcedure<PatternDescriptor>() {
		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			return descriptor instanceof LogicalDescriptor;
		}

		@Override
		public Measure getMeasure() {
			return QualityMeasureId.AREA;
		}

		@Override
		public Measurement perform(PatternDescriptor descriptor) {

			LogicalDescriptor logicalDescriptor = (LogicalDescriptor) descriptor;

			double support = (double) logicalDescriptor.indices().size();

			double area = support * logicalDescriptor.size();

			List<Measurement> auxiliaryMeasurements = ImmutableList
					.of(MeasurementImplementation.measurement(QualityMeasureId.SUPPORT, support));

			return MeasurementImplementation.measurement(QualityMeasureId.AREA, area, auxiliaryMeasurements);
		}
	}),

	/**
	 * Computes a lift measurement with auxiliary measurements for frequency,
	 * product of individual frequencies, and difference of frequency.
	 * 
	 */
	LIFT(new MeasurementProcedure<PatternDescriptor>() {

		private double computeProductOfIndividualFrequencies(LogicalDescriptor description) {
			double product = 1.0;
			PropositionalLogic logic = description.getPropositionalLogic();
			int propLogicSize = logic.population().size();

			for (Proposition literal : description.getElements()) {
				product *= logic.getSupportSet(literal.getId()).size() / (double) propLogicSize;
			}

			return product;
		}

		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			return (descriptor instanceof LogicalDescriptor);
		}

		@Override
		public Measure getMeasure() {
			return QualityMeasureId.LIFT;
		}

		@Override
		public Measurement perform(PatternDescriptor descriptor) {

			LogicalDescriptor logicalDescriptor = (LogicalDescriptor) descriptor;

			double productOfFrequencies = computeProductOfIndividualFrequencies(logicalDescriptor);

			double denominator = Math.pow(2, logicalDescriptor.size() - 2.);

			double frequency = (double) logicalDescriptor.indices().size()
					/ logicalDescriptor.getPropositionalLogic().population().size();

			double deviationOfFrequency = frequency - productOfFrequencies;

			double lift = deviationOfFrequency / denominator;

			List<Measurement> auxiliaryMeasurements = ImmutableList.of(
					MeasurementImplementation.measurement(QualityMeasureId.FREQUENCY, frequency),
					MeasurementImplementation.measurement(QualityMeasureId.PRODUCT_OF_INDIVIDUAL_FREQUENCIES,
							productOfFrequencies),
					MeasurementImplementation.measurement(QualityMeasureId.DEVIATION_OF_FREQUENCY,
							deviationOfFrequency));

			return MeasurementImplementation.measurement(QualityMeasureId.LIFT, lift, auxiliaryMeasurements);
		}
	});

	private final MeasurementProcedure<PatternDescriptor> implementation;

	private DefaultAssociationMeasurementProcedures(MeasurementProcedure<PatternDescriptor> implementation) {
		this.implementation = implementation;
	}

	@Override
	public boolean isApplicable(PatternDescriptor descriptor) {
		return implementation.isApplicable(descriptor);
	}

	@Override
	public Measure getMeasure() {
		return implementation.getMeasure();
	}

	@Override
	public Measurement perform(PatternDescriptor descriptor) {
		return implementation.perform(descriptor);
	}

}
