/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.association;

import static com.google.common.collect.Lists.newArrayList;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.DefaultPattern;
import de.unibonn.realkd.patterns.Measurement;
import de.unibonn.realkd.patterns.MeasurementImplementation;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptorBuilder;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;

/**
 * <p>
 * Utility class providing access to singleton association measurement
 * procedures and static factory method for association construction.
 * </p>
 * 
 * @author Mario Boley
 * @author Sandy Moens
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2
 *
 */
public class Associations {

	/**
	 * <p>
	 * Creates an association pattern from a given logical descriptor with a
	 * form of lift measurement and any number of additional measurements based
	 * on an input list of measurement procedures.
	 * </p>
	 * <p>
	 * The lift measurement is adapted in the following sense: In case lift
	 * measurement is positive, creates an association with lift measurement; in
	 * case lift measurement is positive, creates an association with negative
	 * lift measurement (which consequently will be positive).
	 * </p>
	 * 
	 * @param descriptor
	 *            the logical descriptor of the association pattern
	 * @param additionalProcedures
	 *            other measurement procedures that the results of which are
	 *            added to the pattern
	 * @return an association pattern with measurement for positive or negative
	 *         lift
	 */
	public static Association association(LogicalDescriptor descriptor,
			List<MeasurementProcedure<PatternDescriptor>> additionalProcedures) {
		List<Measurement> measurements = new ArrayList<>();

		Measurement liftMeasurement = Associations.LIFT_MEASUREMENT_PROCEDURE.perform(descriptor);
		/*
		 * Turn lift measurement in negative lift measurement in case lift is
		 * negative
		 */
		if (liftMeasurement.value() < 0) {
			liftMeasurement = MeasurementImplementation.measurement(QualityMeasureId.NEGATIVE_LIFT, -1 * liftMeasurement.value(), liftMeasurement.auxiliaryMeasurements());
		}
		measurements.add(liftMeasurement);
		additionalProcedures.forEach(p -> {
			measurements.add(p.perform(descriptor));
		});
		return new AssociationImplementation(descriptor, measurements, additionalProcedures);
	}

	/**
	 * <p>
	 * Creates an association pattern from a given logical descriptor with a
	 * form of lift measurement.
	 * </p>
	 * 
	 * @param descriptor
	 *            the logical descriptor of the association pattern
	 * @return an association pattern with measurement for positive or negative
	 *         lift
	 * 
	 * @see #association(LogicalDescriptor, List)
	 *      
	 */
	public static Association association(LogicalDescriptor descriptor) {
		return association(descriptor, ImmutableList.of());

	}

	private static class AssociationImplementation extends DefaultPattern implements Association {

		private final List<MeasurementProcedure<PatternDescriptor>> procedures;

		private AssociationImplementation(LogicalDescriptor description, List<Measurement> measurements,
				List<MeasurementProcedure<PatternDescriptor>> additionalProcedures) {
			super(description.getPropositionalLogic().population(), description, measurements);
			this.procedures = additionalProcedures;
		}

		@Override
		public double getLift() {
			if (hasMeasure(QualityMeasureId.LIFT)) {
				return value(QualityMeasureId.LIFT);
			} else {
				return -1 * value(QualityMeasureId.NEGATIVE_LIFT);
			}
		}

		@Override
		public double getExpectedFrequency() {
			return value(QualityMeasureId.PRODUCT_OF_INDIVIDUAL_FREQUENCIES);
		}

		@Override
		public LogicalDescriptor descriptor() {
			return (LogicalDescriptor) super.descriptor();
		}

		@Override
		public AssociationBuilder toBuilder() {
			return new AssociationBuilderImplementation(descriptor().toBuilder(), ImmutableList.copyOf(procedures));
		}
	}
	
	public static AssociationBuilder associationBuilder() {
		return new AssociationBuilderImplementation(LogicalDescriptors.logicalDescriptorBuilder(), newArrayList());
	}

	private static final class AssociationBuilderImplementation implements AssociationBuilder {

		private final LogicalDescriptorBuilder descriptorBuilder;

		private final ArrayList<MeasurementProcedure<PatternDescriptor>> additionalMeasurementProcedures;

		@JsonCreator
		public AssociationBuilderImplementation(
				@JsonProperty("descriptorBuilder") LogicalDescriptorBuilder descriptorBuilder,
				@JsonProperty("additionalMeasurementProcedures") List<MeasurementProcedure<PatternDescriptor>> additionalProcedures) {
			this.descriptorBuilder = descriptorBuilder;
			this.additionalMeasurementProcedures = new ArrayList<>(additionalProcedures);
		}

		@Override
		public synchronized Association build(Workspace context) {
			LogicalDescriptor logicalDescriptor = descriptorBuilder.build(context);
			return Associations.association(logicalDescriptor, additionalMeasurementProcedures);
		}

		@Override
		public synchronized LogicalDescriptorBuilder getDescriptorBuilder() {
			return descriptorBuilder;
		}

		@SuppressWarnings("unused") // used by Jackson
		public synchronized List<MeasurementProcedure<PatternDescriptor>> getAdditionalMeasurementProcedures() {
			return additionalMeasurementProcedures;
		}

		@Override
		public synchronized AssociationBuilder addMeasurementProcedure(
				MeasurementProcedure<PatternDescriptor> procedure) {
			additionalMeasurementProcedures.add(procedure);
			return this;

		}

		@Override
		public synchronized AssociationBuilder removeMeasurementProcedure(
				MeasurementProcedure<PatternDescriptor> procedure) {
			additionalMeasurementProcedures.remove(procedure);
			return this;
		}

		// @Override
		// public List<MeasurementProcedure> getMeasurementProcedures() {
		// ArrayList<MeasurementProcedure> result = new ArrayList<>();
		// result.add(LIFT_MEASUREMENT_PROCEDURE);
		// result.addAll(additionalMeasurementProcedures);
		// return result;
		// }

		@Override
		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof AssociationBuilderImplementation)) {
				return false;
			}
			AssociationBuilderImplementation otherAssBuilder = (AssociationBuilderImplementation) other;
			return (this.getDescriptorBuilder().equals(descriptorBuilder)
					&& this.additionalMeasurementProcedures.equals(otherAssBuilder.additionalMeasurementProcedures));
		}

	}

	public static final MeasurementProcedure<PatternDescriptor> AREA_MEASUREMENT_PROCEDURE = DefaultAssociationMeasurementProcedures.AREA;

	public static final MeasurementProcedure<PatternDescriptor> LIFT_MEASUREMENT_PROCEDURE = DefaultAssociationMeasurementProcedures.LIFT;

}
