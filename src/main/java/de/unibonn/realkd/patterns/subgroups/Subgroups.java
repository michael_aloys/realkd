/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.subgroups;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Lists.newArrayList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Measurement;
import de.unibonn.realkd.patterns.MeasurementImplementation;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptorBuilder;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;
import de.unibonn.realkd.patterns.models.DefaultModel;
import de.unibonn.realkd.patterns.models.Model;
import de.unibonn.realkd.patterns.models.ModelFactory;

/**
 * <p>
 * Provides static factory methods for the construction of subgroup descriptors.
 * Subgroups are a composition of a descriptor of a sub-population of some
 * global population, a local model of some target attributes, and another
 * (usually global) reference model.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.3.0
 * 
 * @version 0.3.2
 *
 */
public class Subgroups {

	private Subgroups() {
		; // not to be instantiated
	}

	public static Subgroup subgroup(LogicalDescriptor extensionDescriptor, DataTable targetTable,
			List<Attribute<?>> targets, Model referenceModel, Model localModel) {

		return new SubgroupDescriptorImplementation(extensionDescriptor, targetTable, targets, referenceModel,
				localModel);
	}

	public static Subgroup subgroup(LogicalDescriptor extensionDescriptor, DataTable targetTable,
			List<Attribute<?>> targets, ModelFactory modelFactory) {

		DefaultModel referenceModel = modelFactory.getModel(targetTable, targets);
		DefaultModel localModel = modelFactory.getModel(targetTable, targets, extensionDescriptor.indices());

		return new SubgroupDescriptorImplementation(extensionDescriptor, targetTable, targets, referenceModel,
				localModel);
	}

	private static class SubgroupDescriptorImplementation implements Subgroup {

		private final Model referenceModel;
		private final Model localModel;
		private final DataTable targetTable;
		private final List<Attribute<?>> targets;
		private final LogicalDescriptor extensionDescriptor;

		private SubgroupDescriptorImplementation(LogicalDescriptor extensionDescriptor, DataTable targetTable,
				List<Attribute<?>> targets, Model referenceModel, Model localModel) {
			checkArgument(targets.equals(referenceModel.attributes()),
					"Target list must equal attribute list of reference model.");
			checkArgument(localModel.attributes().equals(referenceModel.attributes()),
					"Attribute list of local model must equal attribute list of reference model.");
			this.referenceModel = referenceModel;
			this.localModel = localModel;
			this.targets = targets;
			this.extensionDescriptor = extensionDescriptor;
			this.targetTable = targetTable;
		}

		@Override
		public Model referenceModel() {
			return referenceModel;
		}

		@Override
		public Model localModel() {
			return localModel;
		}

		@Override
		public List<Attribute<?>> targetAttributes() {
			return targets;
		}

		@Override
		public SortedSet<Integer> indices() {
			return extensionDescriptor.indices();
		}

		@Override
		public LogicalDescriptor extensionDescriptor() {
			return extensionDescriptor;
		}

		@Override
		public List<Attribute<?>> getReferencedAttributes() {
			Set<Attribute<?>> temp = new HashSet<>();
			temp.addAll(extensionDescriptor.getReferencedAttributes());
			temp.addAll(targets);
			return new ArrayList<>(temp);
		}

		@Override
		public Population population() {
			return extensionDescriptor.population();
		}

		@Override
		public DataTable getTargetTable() {
			return targetTable;
		}

		/**
		 * Checks equality of extension descriptor and target list.
		 * 
		 * WARNING: this does not currently consider the model (classes).
		 * 
		 */
		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (!(o instanceof Subgroup)) {
				return false;
			}
			Subgroup that = (Subgroup) o;
			return this.extensionDescriptor.equals(that.extensionDescriptor())
					&& this.targets.equals(that.targetAttributes());
		}

		@Override
		public int hashCode() {
			int result = 17;
			result = 37 * result + extensionDescriptor.hashCode();
			result = 37 * result + targets.hashCode();
			result = 37 * result + referenceModel.toFactory().hashCode();
			return result;
		}

		@Override
		public String toString() {
			return "Subgroup("+localModel.toString() + ", " + extensionDescriptor.toString() + ")";
		}

		@Override
		public SubgroupBuilder toBuilder() {
			List<Integer> attributeIndices = targetAttributes().stream()
					.map(a -> targetTable.getAttributes().indexOf(a)).collect(Collectors.toList());
			return new SubgroupDescriptorBuilderImplementation(extensionDescriptor().toBuilder(), attributeIndices,
					referenceModel.toFactory(), targetTable.identifier());
		}

	}

	public static SubgroupBuilder subgroupDescriptorBuilder() {
		return new SubgroupDescriptorBuilderImplementation(LogicalDescriptors.logicalDescriptorBuilder(),
				newArrayList(), null, null);
	}

	private static class SubgroupDescriptorBuilderImplementation implements SubgroupBuilder {

		private final LogicalDescriptorBuilder extensionDescriptorBuilder;

		private final String tableId;

		private final List<Integer> attributeIndices;

		private ModelFactory modelFactory;

		@JsonCreator
		private SubgroupDescriptorBuilderImplementation(
				@JsonProperty("extensionBuilder") LogicalDescriptorBuilder extensionDescriptorBuilder,
				@JsonProperty("attributeIndices") List<Integer> attributeIndices,
				@JsonProperty("modelFactory") ModelFactory modelFactory, @JsonProperty("tableId") String tableId) {
			this.extensionDescriptorBuilder = extensionDescriptorBuilder;
			this.attributeIndices = new ArrayList<>(attributeIndices);
			this.modelFactory = modelFactory;
			this.tableId = tableId;
		}

		@Override
		public Subgroup build(Workspace dataWorkspace) {
			// checkArgument(dataWorkspace.getAllDatatables().size() == 1,
			// "Workspace must contain exactly one data table");
			// DataTable targetTable = dataWorkspace.getAllDatatables().get(0);
			DataTable targetTable = dataWorkspace.get(tableId, DataTable.class).get();
			LogicalDescriptor extensionDescriptor = extensionDescriptorBuilder.build(dataWorkspace);
			List<Attribute<?>> targetAttributes = attributeIndices.stream().map(i -> targetTable.getAttribute(i))
					.collect(Collectors.toList());
			return Subgroups.subgroup(extensionDescriptor, targetTable, targetAttributes, modelFactory);
		}

		@Override
		public List<Integer> getAttributeIndices() {
			return attributeIndices;
		}

		@Override
		public LogicalDescriptorBuilder getExtensionBuilder() {
			return this.extensionDescriptorBuilder;
		}

		@Override
		public ModelFactory getModelFactory() {
			return this.modelFactory;
		}

		public void modelFactory(ModelFactory modelFactory) {
			this.modelFactory = modelFactory;
		}

		@JsonProperty("tableId")
		public String tableId() {
			return tableId;
		}

		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof SubgroupDescriptorBuilderImplementation)) {
				return false;
			}
			SubgroupDescriptorBuilderImplementation otherBuilder = (SubgroupDescriptorBuilderImplementation) other;
			return (this.extensionDescriptorBuilder.equals(otherBuilder.extensionDescriptorBuilder)
					&& this.attributeIndices.equals(otherBuilder.attributeIndices)
					&& this.modelFactory.equals(otherBuilder.modelFactory));
		}

		@Override
		public Collection<String> dependencyIds() {
			return Sets.union(ImmutableSet.copyOf(extensionDescriptorBuilder.dependencyIds()),
					ImmutableSet.of(tableId));

		}
	}

	private static final Map<QualityMeasureId, QualityMeasureId> REF_DESCRIPTIVE_MEASURE_TRANSLATION = new HashMap<>();

	static {
		REF_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.WEIBULL_SCALE, QualityMeasureId.GLOBAL_WEIBULL_SCALE);
		REF_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.MEAN, QualityMeasureId.GLOBAL_MEAN);
		REF_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.MEDIAN, QualityMeasureId.GLOBAL_MEDIAN);
		REF_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.STD, QualityMeasureId.GLOBAL_STD);
		REF_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.AAMD, QualityMeasureId.GLOBAL_AAMD);
		REF_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.SLOPE, QualityMeasureId.REF_SLOPE);
		REF_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.INTERCEPT, QualityMeasureId.REF_INTERCEPT);
	}

	private static final Map<QualityMeasureId, QualityMeasureId> LOCAL_DESCRIPTIVE_MEASURE_TRANSLATION = new HashMap<>();

	static {
		LOCAL_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.WEIBULL_SCALE, QualityMeasureId.LOCAL_WEIBULL_SCALE);
		LOCAL_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.MEAN, QualityMeasureId.LOCAL_MEAN);
		LOCAL_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.MEDIAN, QualityMeasureId.LOCAL_MEDIAN);
		LOCAL_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.STD, QualityMeasureId.LOCAL_STD);
		LOCAL_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.AAMD, QualityMeasureId.LOCAL_AAMD);
		LOCAL_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.SLOPE, QualityMeasureId.LOCAL_SLOPE);
		LOCAL_DESCRIPTIVE_MEASURE_TRANSLATION.put(QualityMeasureId.INTERCEPT, QualityMeasureId.LOCAL_INTERCEPT);
	}

	/**
	 * Maps descriptive measurements of local and reference model of a subgroup
	 * descriptor to measurements for patterns. For example, a measurement of
	 * the 'mean' of the local model will be mapped to a measurement of the
	 * 'local mean' for subgroup pattern.
	 */
	public static List<Measurement> descriptiveModelMeasurements(Subgroup descriptor) {
		Model g = descriptor.referenceModel();
		Model l = descriptor.localModel();
		List<Measurement> measurements = new ArrayList<>();
		Stream<Measurement> translatableGlobalMeasurements = g.descriptiveMeasurements().stream()
				.filter(m -> REF_DESCRIPTIVE_MEASURE_TRANSLATION.containsKey(m.measure()));
		translatableGlobalMeasurements.map(m -> MeasurementImplementation
				.measurement(REF_DESCRIPTIVE_MEASURE_TRANSLATION.get(m.measure()), m.value()))
				.forEach(measurements::add);
		Stream<Measurement> translatableLocalMeasurements = l.descriptiveMeasurements().stream()
				.filter(m -> LOCAL_DESCRIPTIVE_MEASURE_TRANSLATION.containsKey(m.measure()));
		translatableLocalMeasurements.map(m -> MeasurementImplementation
				.measurement(LOCAL_DESCRIPTIVE_MEASURE_TRANSLATION.get(m.measure()), m.value()))
				.forEach(measurements::add);
		return measurements;
	}

}
