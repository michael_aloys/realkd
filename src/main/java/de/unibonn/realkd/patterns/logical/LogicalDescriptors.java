/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.logical;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Lists.newArrayList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

//import com.fasterxml.jackson.annotation.JsonCreator;
//import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;

/**
 * @author Mario Boley
 * @author Sandy Moens
 *
 */
public class LogicalDescriptors {

	public static LogicalDescriptor create(PropositionalLogic propositionalLogic, Collection<Proposition> elements) {
		return LogicalDescriptorDefaultImplementation.create(propositionalLogic, elements);
	}

	/**
	 * <p>
	 * Implementation that buffers support set on creation; this is likely to be
	 * removed in future versions.
	 * </p>
	 */
	public static class LogicalDescriptorDefaultImplementation implements LogicalDescriptor {

		private static class CanonicalOrder implements Comparator<Proposition> {

			@Override
			public int compare(Proposition o1, Proposition o2) {
				return Integer.compare(o1.getId(), o2.getId());
			}

		}

		private static Comparator<Proposition> PROPOSITION_ORDER = new CanonicalOrder();

		private static List<Proposition> getInCanonicalOrder(Collection<Proposition> elements) {
			List<Proposition> orderedElements = new ArrayList<>(elements);
			Collections.sort(orderedElements, PROPOSITION_ORDER);
			return orderedElements;
		}

		/**
		 * computes the support set of the pattern by forming the intersection
		 * of the support sets of all propositions. Note that in the special
		 * case of an empty description, the support set has to be the complete
		 * set of objects
		 */
		private static TreeSet<Integer> calculateSupport(PropositionalLogic propositionalLogic,
				List<Proposition> elements) {
			TreeSet<Integer> result = new TreeSet<>();
			if (elements.isEmpty()) {
				result.addAll(propositionalLogic.population().objectIds());
			} else {
				// supportSet.addAll(getElements().get(0).getSupportSet());
				result.addAll(propositionalLogic.getSupportSet(elements.get(0).getId()));
			}
			for (int i = 1; i < elements.size(); i++) {
				// supportSet.retainAll(getElements().get(i).getSupportSet());
				result.retainAll(propositionalLogic.getSupportSet(elements.get(i).getId()));
			}
			return result;
		}

		static LogicalDescriptor create(PropositionalLogic propositionalLogic, Collection<Proposition> elements) {
			List<Proposition> orderedElements = getInCanonicalOrder(elements);
			TreeSet<Integer> supportSet = calculateSupport(propositionalLogic, orderedElements);
			return new LogicalDescriptorDefaultImplementation(propositionalLogic, orderedElements, supportSet);
		}

		private final List<Proposition> elements;

		private final PropositionalLogic propositionalLogic;

		/*
		 * for fancy descriptor support
		 */
		// private final ImmutableList<DescriptionElement> descriptorElements;

		private LogicalDescriptorDefaultImplementation(PropositionalLogic propositionalLogic,
				List<Proposition> orderedElements, SortedSet<Integer> supportSet) {
			this.propositionalLogic = propositionalLogic;
			this.elements = ImmutableList.copyOf(orderedElements);
			this.supportSet = supportSet;

			/*
			 * for fancy descriptor support
			 */
			// List<DescriptionElement> descriptorElementAccumulation = new
			// ArrayList<>();
			// descriptorElementAccumulation.add(new DescriptionElement(
			// "The propositions", ImmutableSet.of()));
			// this.elements.forEach(p -> {
			// descriptorElementAccumulation.add(new DescriptionElement(p,
			// ImmutableSet.of(DescriptionElementTag.DATA_REFERENCE,
			// DescriptionElementTag.ESSENTIAL)));
			// });
			// this.descriptorElements = ImmutableList
			// .copyOf(descriptorElementAccumulation);
		}

		/*
		 * for fancy descriptor support
		 */
		// @Override
		// public List<DescriptionElement> getDescriptionElements() {
		// return descriptorElements;
		// }

		@Override
		public PropositionalLogic getPropositionalLogic() {
			return propositionalLogic;
		}

		/**
		 * 
		 * @return number of contained propositions
		 */
		@Override
		public int size() {
			return this.elements.size();
		}

		@Override
		public boolean isEmpty() {
			return this.elements.isEmpty();
		}

		@Override
		public Proposition getElement(int i) {
			return this.elements.get(i);
		}

		@Override
		public List<String> getElementsAsStringList() {
			List<String> descriptionList = new ArrayList<>();
			for (Proposition proposition : getElements()) {
				descriptionList.add(proposition.toString());
			}

			return descriptionList;
		}

		@Override
		public LogicalDescriptor getSpecialization(Proposition augmentation) {
			List<Proposition> newElements = new ArrayList<>(getElements());
			newElements.add(augmentation);
			Collections.sort(newElements, PROPOSITION_ORDER);

			SortedSet<Integer> newSupportSet = new TreeSet<>();
			newSupportSet.addAll(indices());
			newSupportSet.retainAll(propositionalLogic.getSupportSet(augmentation.getId()));
			return new LogicalDescriptorDefaultImplementation(getPropositionalLogic(), newElements, newSupportSet);
		}

		@Override
		public LogicalDescriptor getGeneralization(Proposition reductionElement) {
			if (!getElements().contains(reductionElement)) {
				throw new IllegalArgumentException("reduction element not part of description");
			}
			List<Proposition> newDescription = new ArrayList<>(getElements());
			newDescription.remove(reductionElement);
			return create(getPropositionalLogic(), newDescription);

		}

		@Override
		public boolean refersToAttribute(Attribute<?> attribute) {
			for (Proposition proposition : getElements()) {
				if (proposition instanceof AttributeBasedProposition
						&& ((AttributeBasedProposition<?>) proposition).getAttribute() == attribute) {
					return true;
				}
			}

			return false;
		}

		@Override
		public List<Attribute<?>> getReferencedAttributes() {
			List<Attribute<?>> result = new ArrayList<>();
			for (Proposition proposition : getElements()) {
				if (proposition instanceof AttributeBasedProposition<?>) {
					result.add(((AttributeBasedProposition<?>) proposition).getAttribute());
				}
			}
			return result;
		}

		@Override
		public List<Proposition> getElements() {
			return this.elements;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (!(o instanceof LogicalDescriptor))
				return false;

			LogicalDescriptor other = (LogicalDescriptor) o;

			if (other.getElements().size() != this.getElements().size()) {
				return false;
			}

			for (int i = 0; i < this.getElements().size(); i++) {
				if (this.getElements().get(i) != other.getElements().get(i)) {
					return false;
				}
			}

			return true;
		}

		@Override
		public int hashCode() {
			int result = 17;
			for (Proposition proposition : this.elements) {
				result = 37 * result + proposition.hashCode();
			}
			return result;
		}

		private final SortedSet<Integer> supportSet;

		@Override
		public String toString() {
			return elements.toString();
		}

		@Override
		public SortedSet<Integer> indices() {
			return supportSet;
		}

		@Override
		public Population population() {
			return propositionalLogic.population();
		}

		@Override
		public LogicalDescriptorBuilder toBuilder() {
			return new LogicalDescriptorBuilderDefaultImplementation(getPropositionalLogic().identifier(),
					elements.stream().map(e -> e.getId()).collect(Collectors.toCollection(ArrayList::new)));
		}

	}
	
	public static LogicalDescriptorBuilder logicalDescriptorBuilder() {
		return new LogicalDescriptorBuilderDefaultImplementation("_", newArrayList());
	}

	public static class LogicalDescriptorBuilderDefaultImplementation implements LogicalDescriptorBuilder {

		private String propositionalLogicIdentifier;

		private final ArrayList<Integer> elementIndices;

		@JsonCreator
		public LogicalDescriptorBuilderDefaultImplementation(
				@JsonProperty("propositionalLogicIdentifier") String propositionalLogicIdentifier,
				@JsonProperty("elementIndexList") List<Integer> elementIndices) {
			this.propositionalLogicIdentifier = propositionalLogicIdentifier;
			this.elementIndices = Lists.newArrayList(elementIndices);
		}

		@Override
		public LogicalDescriptor build(Workspace context) {
			checkArgument(context.contains(propositionalLogicIdentifier, PropositionalLogic.class),
					"Workspace does not contain artifact '" + propositionalLogicIdentifier
							+ "' of type PropositionalLogic");
			PropositionalLogic propositionalLogic = (PropositionalLogic) context.get(propositionalLogicIdentifier);
			List<Proposition> propositions = elementIndices.stream()
					.map(i -> propositionalLogic.getPropositions().get(i)).collect(Collectors.toList());
			return LogicalDescriptorDefaultImplementation.create(propositionalLogic, propositions);
		}

		@Override
		public List<Integer> getElementIndexList() {
			return elementIndices;
		}

		@Override
		public String getPropositionalLogicIdentifier() {
			return propositionalLogicIdentifier;
		}
		
		@Override
		public void propositionalLogicIdentifier(String identifier) {
			this.propositionalLogicIdentifier = identifier;
		}

		@Override
		public boolean equals(Object other) {
			if (other == this) {
				return true;
			}
			if (!(other instanceof LogicalDescriptorBuilder)) {
				return false;
			}
			LogicalDescriptorBuilder otherDescriptor = (LogicalDescriptorBuilder) other;
			return (this.getElementIndexList().equals(otherDescriptor.getElementIndexList()) && this
					.getPropositionalLogicIdentifier().equals(otherDescriptor.getPropositionalLogicIdentifier()));
		}

	}

}
