/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.pmm;

import static de.unibonn.realkd.patterns.MeasurementImplementation.measurement;
import static java.lang.Math.max;
import static java.lang.Math.sqrt;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.DefaultPattern;
import de.unibonn.realkd.patterns.Measure;
import de.unibonn.realkd.patterns.Measurement;
import de.unibonn.realkd.patterns.MeasurementImplementation;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.PatternDescriptorBuilder;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.emm.ExceptionalModelMining;
import de.unibonn.realkd.patterns.models.mean.MetricEmpiricalDistribution;
import de.unibonn.realkd.patterns.models.regression.LinearRegressionModel;
import de.unibonn.realkd.patterns.models.table.ContingencyTable;
import de.unibonn.realkd.patterns.models.table.ContingencyTableModel;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.patterns.subgroups.SubgroupBuilder;
import de.unibonn.realkd.patterns.subgroups.Subgroups;
import de.unibonn.realkd.util.Arrays;

/**
 * <p>
 * Provides static factory methods for the construction of simple model mining
 * subgroups as well as measurement procedures for pure model mining.
 * </p>
 * <p>
 * Pure model subgroups capture a subset of a population for which a selection
 * of target attributes can be represented "purely" by a certain model class.
 * The meaning of "purely" can vary between low prediction or fitting error, or
 * simply mean low target attribute variance.
 * </p>
 * <p>
 * In contrast to exceptional model mining, pure model mining is agnostic to the
 * global data distribution. However, depending on the specific configuration,
 * pure model mining is highly non-agnostic to the local data distribution,
 * which it explicitly tests for good fits of distributions that may have a
 * strong bias.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 * 
 * @see ExceptionalModelMining
 *
 */
public class PureModelMining {

	/**
	 * Creates a new pure model subgroup pattern based on a provided descriptor,
	 * a purity measurement procedure, and a list of optional measurement
	 * procedures. In addition to the specified procedures the resulting
	 * subgroup pattern will also contain a frequency measurement.
	 * 
	 * @param descriptor
	 *            the descriptor of the new subgroup
	 * @param purityMeasurementProcedure
	 *            the procedure to be used to measure model purity of the
	 *            subgroup
	 * @param optionalMeasurementProcedures
	 *            more optional procedures to be applied for the new subgroup
	 * @return new pure model subgroup pattern
	 */
	public static PureModelSubgroup pureSubgroup(final Subgroup descriptor,
			final MeasurementProcedure<PatternDescriptor> purityMeasurementProcedure,
			final List<MeasurementProcedure<PatternDescriptor>> optionalMeasurementProcedures) {

		List<MeasurementProcedure<PatternDescriptor>> list = Lists.newArrayList();
		list.add(PmmFrequencyMeasurementProcedure.INSTANCE);
		list.add(purityMeasurementProcedure);
		list.addAll(optionalMeasurementProcedures);

		List<Measurement> measurements = list.stream().map(p -> p.perform(descriptor)).collect(Collectors.toList());

		List<Measurement> allMeasurements = new ArrayList<>(measurements);
		allMeasurements.addAll(Subgroups.descriptiveModelMeasurements(descriptor));

		List<MeasurementProcedure<PatternDescriptor>> optionalProceduresSnapshot = ImmutableList
				.copyOf(optionalMeasurementProcedures);

		return new PureModelSubgroupImplementation(descriptor.population(), descriptor, allMeasurements,
				purityMeasurementProcedure.getMeasure(),
				pattern -> new PureModelSubgroupBuilderImplementation(pattern.descriptor().toBuilder(),
						purityMeasurementProcedure, optionalProceduresSnapshot));
	}

	private PureModelMining() {
		;
	}

	private static class PureModelSubgroupBuilderImplementation implements PureModelSubgroupBuilder {

		private SubgroupBuilder descriptorBuilder;

		private MeasurementProcedure<PatternDescriptor> purityMeasurementProcedure;

		private ArrayList<MeasurementProcedure<PatternDescriptor>> additionalMeasurementProcedures = new ArrayList<>();

		@JsonCreator
		private PureModelSubgroupBuilderImplementation(
				@JsonProperty("descriptorBuilder") SubgroupBuilder descriptorBuilder,
				@JsonProperty("purityMeasurementProcedure") MeasurementProcedure<PatternDescriptor> purityMeasurementProcedure,
				@JsonProperty("additionalMeasurementProcedures") List<MeasurementProcedure<PatternDescriptor>> additionalMeasurementProcedures) {
			this.descriptorBuilder = descriptorBuilder;
			this.purityMeasurementProcedure = purityMeasurementProcedure;
			this.additionalMeasurementProcedures = Lists.newArrayList(additionalMeasurementProcedures);
		}

		@Override
		public PatternDescriptorBuilder getDescriptorBuilder() {
			return descriptorBuilder;
		}

		@Override
		public PureModelSubgroupBuilder addMeasurementProcedure(MeasurementProcedure<PatternDescriptor> procedure) {
			additionalMeasurementProcedures.add(procedure);
			return this;
		}

		@Override
		public PureModelSubgroupBuilder removeMeasurementProcedure(MeasurementProcedure<PatternDescriptor> procedure) {
			additionalMeasurementProcedures.remove(procedure);
			return this;
		}

		@JsonProperty("purityMeasurementProcedure")
		public MeasurementProcedure<PatternDescriptor> purityMeasureProcedure() {
			return purityMeasurementProcedure;
		}

		@JsonProperty("additionalMeasurementProcedures")
		public List<MeasurementProcedure<PatternDescriptor>> additionalMeasurementProcedures() {
			return additionalMeasurementProcedures;
		}

		@Override
		public PureModelSubgroup build(Workspace context) {
			Subgroup descriptor = descriptorBuilder.build(context);
			return pureSubgroup(descriptor, purityMeasurementProcedure, additionalMeasurementProcedures);
		}

		@Override
		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof PureModelSubgroupBuilderImplementation)) {
				return false;
			}
			PureModelSubgroupBuilderImplementation otherBuilder = (PureModelSubgroupBuilderImplementation) other;
			return (Objects.equals(this.descriptorBuilder, otherBuilder.descriptorBuilder)
					&& Objects.equals(this.purityMeasurementProcedure, otherBuilder.purityMeasurementProcedure)
					&& Objects.equals(this.additionalMeasurementProcedures,
							otherBuilder.additionalMeasurementProcedures));
		}

	}

	private static class PureModelSubgroupImplementation extends DefaultPattern implements PureModelSubgroup {

		private final Function<PureModelSubgroup, PureModelSubgroupBuilder> toBuilderConverter;

		private final Measure purityMeasure;

		public PureModelSubgroupImplementation(Population population, Subgroup descriptor,
				List<Measurement> measurements, Measure purityMeasure,
				Function<PureModelSubgroup, PureModelSubgroupBuilder> toBuilderConverter) {
			super(population, descriptor, measurements);
			this.purityMeasure = purityMeasure;
			this.toBuilderConverter = toBuilderConverter;
		}

		@Override
		public Subgroup descriptor() {
			return (Subgroup) super.descriptor();
		}

		@Override
		public Measure purityGainMeasure() {
			return purityMeasure;
		}

		@Override
		public PureModelSubgroupBuilder toBuilder() {
			return toBuilderConverter.apply(this);
		}

	}

	private static enum PmmFrequencyMeasurementProcedure implements MeasurementProcedure<PatternDescriptor> {

		INSTANCE;

		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			return (descriptor instanceof Subgroup);
		}

		@Override
		@JsonIgnore
		public Measure getMeasure() {
			return QualityMeasureId.FREQUENCY;
		}

		@Override
		public Measurement perform(PatternDescriptor descriptor) {
			Subgroup pmmDescriptor = (Subgroup) descriptor;
			int support = pmmDescriptor.extensionDescriptor().indices().size();
			double value = (double) support / pmmDescriptor.extensionDescriptor().population().size();
			Measurement supportMeasurement = MeasurementImplementation.measurement(QualityMeasureId.SUPPORT, support);
			return MeasurementImplementation.measurement(getMeasure(), value, ImmutableList.of(supportMeasurement));
		}

	}

	public static final MeasurementProcedure<PatternDescriptor> STD_GAIN_MEASUREMENT_PROCEDURE = StandardDeviationMeasurementProcedure.INSTANCE;

	public static final MeasurementProcedure<PatternDescriptor> ABSOLUTE_PEARSON_GAIN_MEASUREMENT_PROCEDURE = PearsonCorrelationGainProcedure.INSTANCE;

	public static final MeasurementProcedure<PatternDescriptor> ENTROPY_GAIN_MEASUREMENT_PROCEDURE = EntropyGainMeasurementProcedure.INSTANCE;

	public static final MeasurementProcedure<PatternDescriptor> RMSE_GAIN_MEASUREMENT_PROCEDURE = RmseGainMeasurementProcedure.INSTANCE;

	private static enum PearsonCorrelationGainProcedure implements MeasurementProcedure<PatternDescriptor> {

		INSTANCE;

		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			if (!(descriptor instanceof Subgroup)) {
				return false;
			}
			Subgroup subgroup = (Subgroup) descriptor;
			return (subgroup.localModel().attributes().size() == 2
					&& subgroup.localModel() instanceof MetricEmpiricalDistribution)
					&& (subgroup.referenceModel() instanceof MetricEmpiricalDistribution);
		}

		@Override
		@JsonIgnore
		public Measure getMeasure() {
			return QualityMeasureId.ABSOLUTE_PEARSON_GAIN;
		}

		@Override
		public Measurement perform(PatternDescriptor descriptor) {
			Subgroup pmmDescriptor = (Subgroup) descriptor;
			double localCov = ((MetricEmpiricalDistribution) pmmDescriptor.localModel()).covarianceMatrix()[0][1];
			double refCov = ((MetricEmpiricalDistribution) pmmDescriptor.referenceModel()).covarianceMatrix()[0][1];
			double refStd1 = sqrt(
					((MetricEmpiricalDistribution) pmmDescriptor.referenceModel()).covarianceMatrix()[0][0]);
			double refStd2 = sqrt(
					((MetricEmpiricalDistribution) pmmDescriptor.referenceModel()).covarianceMatrix()[1][1]);
			double localStd1 = sqrt(
					((MetricEmpiricalDistribution) pmmDescriptor.localModel()).covarianceMatrix()[0][0]);
			double localStd2 = sqrt(
					((MetricEmpiricalDistribution) pmmDescriptor.localModel()).covarianceMatrix()[1][1]);
			double localCorrelation = localCov / (localStd1 * localStd2);
			double refCorrelation = refCov / (refStd1 * refStd2);
			double result = max(Math.abs(localCorrelation) - Math.abs(refCorrelation), 0);
			return measurement(getMeasure(), result,
					ImmutableList.of(measurement(QualityMeasureId.GLOBAL_PEARSON, refCorrelation),
							measurement(QualityMeasureId.LOCAL_PEARSON, localCorrelation)));
		}

		@Override
		public String toString() {
			return "Pearson correlation gain";
		}

	}

	private static enum StandardDeviationMeasurementProcedure implements MeasurementProcedure<PatternDescriptor> {

		INSTANCE;

		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			if (!(descriptor instanceof Subgroup)) {
				return false;
			}
			Subgroup subgroup = (Subgroup) descriptor;
			return (subgroup.localModel() instanceof MetricEmpiricalDistribution)
					&& (subgroup.referenceModel() instanceof MetricEmpiricalDistribution);
		}

		@Override
		@JsonIgnore
		public Measure getMeasure() {
			return QualityMeasureId.STD_GAIN;
		}

		@Override
		public Measurement perform(PatternDescriptor descriptor) {
			Subgroup pmmDescriptor = (Subgroup) descriptor;
			double[][] localCovMatrix = ((MetricEmpiricalDistribution) pmmDescriptor.localModel()).covarianceMatrix();
			double[][] refCovMatrix = ((MetricEmpiricalDistribution) pmmDescriptor.referenceModel()).covarianceMatrix();
			double localOneNorm = columnAbsSumStream(localCovMatrix).max().getAsDouble();
			double refOneNorm = columnAbsSumStream(refCovMatrix).max().getAsDouble();
			double refStd = sqrt(refOneNorm);
			double localStd = sqrt(localOneNorm);
			double result = max(refStd - localStd, 0)/refStd;
			return measurement(getMeasure(), result, ImmutableList.of(measurement(QualityMeasureId.GLOBAL_STD, refStd),
					measurement(QualityMeasureId.LOCAL_STD, localStd)));
		}

		private DoubleStream columnAbsSumStream(double[][] matrix) {
			return Arrays.columnAggregateStream(matrix, 0.0, (x, y) -> x + Math.abs(y));
		}

		@Override
		public String toString() {
			return "Std. deviation gain";
		}

	}

	private static enum EntropyGainMeasurementProcedure implements MeasurementProcedure<PatternDescriptor> {

		INSTANCE;

		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			if (!(descriptor instanceof Subgroup)) {
				return false;
			}
			Subgroup subgroup = (Subgroup) descriptor;
			return (subgroup.localModel() instanceof ContingencyTableModel)
					&& (subgroup.referenceModel() instanceof ContingencyTableModel);
		}

		@Override
		@JsonIgnore
		public Measure getMeasure() {
			return QualityMeasureId.ENTROPY_GAIN;
		}

		@Override
		public Measurement perform(PatternDescriptor descriptor) {
			Subgroup pmmDescriptor = (Subgroup) descriptor;
			ContingencyTableModel localModel = (ContingencyTableModel) pmmDescriptor.localModel();
			ContingencyTableModel refModel = (ContingencyTableModel) pmmDescriptor.referenceModel();
			double refEntropy = entropy(refModel.getProbabilities());
			double localEntropy = entropy(localModel.getProbabilities());
			return measurement(QualityMeasureId.ENTROPY_GAIN, max(refEntropy - localEntropy,0)/refEntropy, ImmutableList.of(
					measurement(QualityMeasureId.REF_ENTROPY, refEntropy),
					measurement(QualityMeasureId.LOCAL_ENTROPY, localEntropy),
					measurement(QualityMeasureId.LOCAL_MODE_PROBABILITY, maxProb(localModel.getProbabilities()))));
		}

		private double entropy(ContingencyTable table) {
			DoubleStream probabilities = table.getKeys().stream().mapToDouble(key -> table.getNormalizedValue(key));
			double entropy = probabilities.map(p -> (p > 0.0) ? -p * Math.log(p) / Math.log(2) : 0.0).sum();
			return entropy;
		}

		private double maxProb(ContingencyTable table) {
			DoubleStream probabilities = table.getKeys().stream().mapToDouble(key -> table.getNormalizedValue(key));
			return probabilities.max().orElseGet(() -> Double.NaN);
		}

		@Override
		public String toString() {
			return "Entropy gain";
		}

	}

	private static enum RmseGainMeasurementProcedure implements MeasurementProcedure<PatternDescriptor> {

		INSTANCE;

		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			if (!(descriptor instanceof Subgroup)) {
				return false;
			}
			Subgroup subgroup = (Subgroup) descriptor;
			return ((subgroup.localModel() instanceof LinearRegressionModel)
					&& (subgroup.referenceModel() instanceof LinearRegressionModel));
		}

		@Override
		public Measure getMeasure() {
			return QualityMeasureId.RMSE_GAIN;
		}

		@Override
		public Measurement perform(PatternDescriptor descriptor) {
			Subgroup subgroup = (Subgroup) descriptor;
			double localRmse = rmse((LinearRegressionModel) subgroup.localModel());
			double refRmse = rmse((LinearRegressionModel) subgroup.referenceModel());
			double gain = max(refRmse - localRmse, 0);
			return measurement(QualityMeasureId.RMSE_GAIN, gain,
					ImmutableList.of(measurement(QualityMeasureId.REFERENCE_RMSE, refRmse),
							measurement(QualityMeasureId.LOCAL_RMSE, localRmse)));
		}

		private double rmse(LinearRegressionModel model) {
			MetricAttribute x = (MetricAttribute) model.attributes().get(0);
			MetricAttribute y = (MetricAttribute) model.attributes().get(1);
			ToDoubleFunction<Integer> errorTerm = i -> Math.pow(model.predict(x.value(i)) - y.value(i), 2);
			Stream<Integer> nonMissing = model.rows().stream().filter(i -> !x.valueMissing(i) && !y.valueMissing(i));
			double meanSqaredError = nonMissing.collect(Collectors.averagingDouble(errorTerm));
			double rmse = Math.sqrt(meanSqaredError);
			return rmse;
		}

		public String toString() {
			return getMeasure().getName();
		}

	}

}
