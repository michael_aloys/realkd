/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.table;

import static com.google.common.base.Preconditions.checkElementIndex;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.realkd.common.workspace.Entity;
import de.unibonn.realkd.common.workspace.HasSerialForm;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attributegroups.AttributeGroup;

/**
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0
 * 
 */
public final class DefaultDataTable implements DataTable, HasSerialForm {

	static class DefaultDataTableSerialFom implements SerialForm {

		public final String populationId;

		public final String name;

		public final String description;

		public final List<AttributeGroup> groups;

		public final List<Attribute<?>> attributes;

		public final String id;

		@JsonCreator
		public DefaultDataTableSerialFom(@JsonProperty("populationId") String populationId,
				@JsonProperty("id") String id, @JsonProperty("name") String name,
				@JsonProperty("description") String description,
				@JsonProperty("attributes") List<Attribute<?>> attributes,
				@JsonProperty("groups") List<AttributeGroup> groups) {
			this.populationId = populationId;
			this.id = id;
			this.name = name;
			this.description = description;
			this.attributes = attributes;
			this.groups = groups;
		}

		@Override
		public Entity get(Workspace workspace) {
			Optional<Population> pop = workspace.get(populationId, Population.class);
			return DataTables.table(id, name, description, pop.get(), attributes, groups);
		}

		@Override
		public String identifier() {
			return id;
		}

		@Override
		public Collection<String> dependencyIds() {
			return ImmutableSet.of(populationId);
		}

		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof DefaultDataTableSerialFom)) {
				return false;
			}
			DefaultDataTableSerialFom otherSerialForm = (DefaultDataTableSerialFom) other;
			return populationId.equals(otherSerialForm.populationId) && id.equals(otherSerialForm.id)
					&& name.equals(otherSerialForm.name) && description.equals(otherSerialForm.description)
					&& attributes.equals(otherSerialForm.attributes) && groups.equals(otherSerialForm.groups);
		}

		public int hashCode() {
			return Objects.hash(id, populationId, name, description, attributes, groups);
		}

	}

	private final String name;

	private final String description;

	private final AttributeGroupStore attributeGroupsStore;

	private final List<Attribute<?>> attributes;

	private final Population population;

	private final String id;

	private final String stringRepr;

	private final ImmutableList<Entity> dependencies;

	DefaultDataTable(String id, String name, String description, Population population, List<Attribute<?>> attributes,
			List<AttributeGroup> attributeGroups) {
		this.id = id;
		this.population = population;
		this.name = name;
		this.description = description;
		this.attributes = ImmutableList.copyOf(attributes);
		this.attributeGroupsStore = new AttributeGroupStore(attributeGroups);
		this.stringRepr = name;
		this.dependencies = ImmutableList.of(population);
	}

	@Override
	public String identifier() {
		return id;
	}

	@Override
	public boolean atLeastOneAttributeValueMissingFor(int objectId) {
		return atLeastOneAttributeValueMissingFor(objectId, attributes);
	}

	@Override
	public boolean atLeastOneAttributeValueMissingFor(int objectId, List<? extends Attribute<?>> testAttributes) {
		for (Attribute<?> attribute : testAttributes) {
			if (attribute.valueMissing(objectId)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public String description() {
		return description;
	}

	@Override
	public List<String> getAttributeNames() {
		List<String> res = new ArrayList<>();
		for (Attribute<?> attribute : getAttributes()) {
			res.add(attribute.name());
		}
		return res;
	}

	@Override
	public List<Attribute<?>> getAttributes() {
		return attributes;
	}

	@Override
	public Attribute<?> getAttribute(int attributeIndex) {
		checkElementIndex(attributeIndex, attributes.size());
		return attributes.get(attributeIndex);
	}

	@Override
	public List<AttributeGroup> getAttributeGroups() {
		return attributeGroupsStore.groups();
	}

	@Override
	public Collection<AttributeGroup> getAttributeGroupsOf(Attribute<?> attribute) {
		return this.attributeGroupsStore.getAttributeGroupsOf(attribute);
	}

	@Override
	public boolean isPartOfMacroAttributeWithAtLeastOneOf(Attribute<?> attribute,
			Collection<Attribute<?>> otherAttributes) {
		return this.attributeGroupsStore.isPartOfMacroAttributeWithAtLeastOneOf(attribute, otherAttributes);
	}

	@Override
	public boolean isPartOfMacroAttributeWith(Attribute<?> attribute, Attribute<?> otherAttribute) {
		return this.attributeGroupsStore.isPartOfMacroAttributeWith(attribute, otherAttribute);
	}

	@Override
	public String toString() {
		return stringRepr;
	}

	@Override
	public Population population() {
		return population;
	}

	@Override
	public SerialForm serialForm() {
		return new DefaultDataTableSerialFom(population.identifier(), id, name, description, attributes,
				attributeGroupsStore.groups());
	}

	@Override
	public List<Entity> dependencies() {
		return dependencies;
	}

}
