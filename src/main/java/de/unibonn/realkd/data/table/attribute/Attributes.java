/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.table.attribute;

import static java.util.Comparator.naturalOrder;

import java.util.Comparator;
import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.JsonSerialization;

/**
 * Provides static factory methods for attributes.
 * 
 * @author Mario Boley
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 *
 */
public class Attributes {

	private Attributes() {
		; // not to be instantiated
	}

	public static CategoricAttribute<String> categoricalAttribute(String name, String description,
			List<String> values) {
		return new DefaultCategoricAttribute(name, description, values);
	}

	public static <T> OrdinalAttribute<T> ordinalAttribute(String name, String description, List<T> values,
			Comparator<T> comparator, Class<T> type) {
		return new DefaultOrdinalAttribute<T>(name, description, values, comparator, type);
	}

	public static <T extends Comparable<T>> OrdinalAttribute<T> ordinalAttribute(String name, String description,
			List<T> values, Class<T> type) {
		return new DefaultOrdinalAttribute<T>(name, description, values, naturalOrder(), type);
	}

	public static <T> OrderedCategoricAttribute<T> orderedCategoricAttribute(String name, String description,
			List<T> values, Comparator<T> comparator, Class<T> type) {
		return new OrderedCategoricAttribute<T>(name, description, values, comparator, type);
	}

	public static <T extends Comparable<T>> OrderedCategoricAttribute<T> orderedCategoricAttribute(String name,
			String description, List<T> values, Class<T> type) {
		return new OrderedCategoricAttribute<T>(name, description, values, naturalOrder(), type);
	}

	public static MetricAttribute metricDoubleAttribute(String name, String description, List<Double> values) {
		return new DefaultMetricAttribute(name, description, values);
	}

	public static void main(String[] args) {
//		Population population = Populations.population("testPop", 3);
		CategoricAttribute<String> categoricAttribute = categoricalAttribute("Categoric attribute", "For testing", ImmutableList.of("male", "male", "female"));
		String json = JsonSerialization.toPrettyJson(categoricAttribute, Object.class);
		System.out.println(json);
	}

}
