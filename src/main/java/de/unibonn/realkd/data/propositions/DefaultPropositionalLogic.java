/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.propositions;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.realkd.data.Population;

/**
 * @author Mario Boley
 * 
 * @since 0.2.0
 * 
 * @version 0.3.0
 *
 */
public class DefaultPropositionalLogic implements PropositionalLogic {

	// private static final Logger LOGGER = Logger
	// .getLogger(DefaultPropositionalLogic.class.getName());

	private final ImmutableList<Proposition> propositions;

	private final Set<Integer>[] truthSets;

	private final String name;
	
	private final String id;

	private final String description;

	private final Population population;

	@SuppressWarnings("unchecked")
	public DefaultPropositionalLogic(String id, String name, String description, Population population,
			List<Proposition> propositions) {
		this.id=id;
		this.name = name;
		this.description = description;
		this.population = population;
		this.propositions = ImmutableList.copyOf(propositions);
		this.truthSets = ((Set<Integer>[]) new Set[population.size()]);
	}

	public DefaultPropositionalLogic(String name, String description, Population population,
			List<Proposition> propositions) {
		this(name, name, description, population, propositions);
	}

	@Override
	public List<Proposition> getPropositions() {
		return propositions;
	}

	@Override
	public String toString() {
		return name();
	}

	@Override
	public String identifier() {
		return id;
	}
	
	@Override
	public String name() {
		return name;
	}

	@Override
	public String description() {
		return description;
	}

	@Override
	public Set<Integer> getSupportSet(int basePropositionIndex) {
		return propositions.get(basePropositionIndex).getSupportSet();
	}

	@Override
	public Set<Integer> getTruthSet(int objectId) {
		if (truthSets[objectId] == null) {
			List<Integer> list = propositions.stream().filter(x -> x.holdsFor(objectId)).map(x -> x.getId())
					.collect(Collectors.toList());
			truthSets[objectId] = ImmutableSet.copyOf(list);
		}
		return truthSets[objectId];
	}

	@Override
	public Population population() {
		return population;
	}

}
