/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.propositions;

import java.util.ArrayList;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import de.unibonn.realkd.data.constraints.Constraint;
import de.unibonn.realkd.data.table.attribute.Attribute;

public class DefaultAttributeBasedProposition<T> implements AttributeBasedProposition<T> {

	private final Attribute<? extends T> attribute;

	private final Constraint<T> constraint;

	private final int indexInLogic;

	private ImmutableSet<Integer> supportSet;

	private final String name;

	public DefaultAttributeBasedProposition(Attribute<? extends T> attribute, Constraint<T> constraint,
			int indexInLogic) {
		this.attribute = attribute;
		this.constraint = constraint;
		this.indexInLogic = indexInLogic;
		name = attribute.name() + constraint.suffixNotationName();
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int getId() {
		return indexInLogic;
	}

	@Override
	public Attribute<? extends T> getAttribute() {
		return attribute;
	}

	@Override
	public Constraint<T> getConstraint() {
		return constraint;
	}

	@Override
	public boolean implies(Proposition anotherProposition) {
		if (!(anotherProposition instanceof AttributeBasedProposition<?>)) {
			return false;
		}
		if (this.getAttribute() == ((AttributeBasedProposition<?>) anotherProposition).getAttribute()) {
			// following cast is safe because propositons refer to same
			// attribute
			@SuppressWarnings("unchecked")
			Constraint<T> otherConstraint = (Constraint<T>) ((AttributeBasedProposition<T>) anotherProposition).getConstraint();
			return this.constraint.implies(otherConstraint);
		}
		return false;
	}

	/**
	 * Buffers the support set after first request and uses {@link ImmutableSet}
	 * as return type.
	 * 
	 */
	@Override
	public Set<Integer> getSupportSet() {
		if (supportSet == null) {
			ArrayList<Integer> supportList = new ArrayList<>();
			for (int i = 0; i <= attribute.maxIndex(); i++) {
				if (holdsFor(i)) {
					supportList.add(i);
				}
			}
			supportSet = ImmutableSet.copyOf(supportList);
		}
		return supportSet;
	}

	@Override
	public int getSupportCount() {
		return getSupportSet().size();
	}
	@Override
	public String name() {
		return name;
	}

}
