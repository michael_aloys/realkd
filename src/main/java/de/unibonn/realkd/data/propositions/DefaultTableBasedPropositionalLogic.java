/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.propositions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.workspace.Entity;
import de.unibonn.realkd.common.workspace.HasSerialForm;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.constraints.Constraint;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;

public class DefaultTableBasedPropositionalLogic implements TableBasedPropositionalLogic, HasSerialForm {

	private static class DefaultTableBasedPropositionalLogicSerialForm implements SerialForm {

		public final String identifier;

		public final String name;

		public final String description;

		public final String tableIdentifier;

		public final List<Integer> attributeIndices;

		public final List<Constraint<?>> constraints;

		private final ImmutableList<String> dependencies;

		@JsonCreator
		public DefaultTableBasedPropositionalLogicSerialForm(@JsonProperty("identifier") String identifier,
				@JsonProperty("name") String name, @JsonProperty("description") String description,
				@JsonProperty("tableIdentifier") String tableIdentifier,
				@JsonProperty("attributeIndices") List<Integer> attributeIndices,
				@JsonProperty("constraints") List<Constraint<?>> constraints) {
			this.identifier = identifier;
			this.name = name;
			this.description = description;
			this.tableIdentifier = tableIdentifier;
			this.attributeIndices = attributeIndices;
			this.constraints = constraints;
			dependencies = ImmutableList.of(tableIdentifier);
		}

		@Override
		public String identifier() {
			return identifier;
		}

		private <T> DefaultAttributeBasedProposition<T> propositionOfIndex(DataTable table, int i) {
			@SuppressWarnings("unchecked")
			Attribute<T> attribute = (Attribute<T>) table.getAttributes().get(attributeIndices.get(i));
			@SuppressWarnings("unchecked")
			Constraint<T> constraint = (Constraint<T>) constraints.get(i);
			return new DefaultAttributeBasedProposition<>(attribute, constraint, i);
		}

		@Override
		public Entity get(Workspace workspace) {
			DataTable table = workspace.get(tableIdentifier, DataTable.class).get();
			List<AttributeBasedProposition<?>> props = IntStream.range(0, constraints.size())
					.mapToObj(i -> propositionOfIndex(table, i)).collect(Collectors.toList());
			return new DefaultTableBasedPropositionalLogic(table, props, identifier, name, description);
		}

		public Collection<String> dependencyIds() {
			return dependencies;
		}

	}

	private final DataTable dataTable;

	private final DefaultPropositionalLogic propositionalLogic;

	public DefaultTableBasedPropositionalLogic(DataTable dataTable, List<AttributeBasedProposition<?>> propositions) {
		this(dataTable, propositions, "statements_about_" + dataTable.identifier(),
				"Statements about " + dataTable.name(), "");
	}

	public DefaultTableBasedPropositionalLogic(DataTable dataTable, List<AttributeBasedProposition<?>> propositions,
			String id, String name, String description) {
		this.dataTable = dataTable;
		List<Proposition> plainProps = propositions.stream().map(p -> (Proposition) p).collect(Collectors.toList());
		this.propositionalLogic = new DefaultPropositionalLogic(id, name, description, dataTable.population(),
				plainProps);
	}

	@Override
	public List<Proposition> getPropositions() {
		return propositionalLogic.getPropositions();
	}

	@Override
	public List<AttributeBasedProposition<?>> getAttributeBasedPropositionsAbout(Attribute<?> attribute) {
		List<AttributeBasedProposition<?>> result = new ArrayList<>();
		for (Proposition prop : this.getPropositions()) {
			if (((AttributeBasedProposition<?>) prop).getAttribute() == attribute) {
				result.add((AttributeBasedProposition<?>) prop);
			}
		}
		return result;
	}

	@Override
	public DataTable getDatatable() {
		return dataTable;
	}

	@Override
	public String toString() {
		return propositionalLogic.toString();
	}

	@Override
	public String identifier() {
		return propositionalLogic.identifier();
	}

	@Override
	public String name() {
		return propositionalLogic.name();
	}

	@Override
	public String description() {
		return propositionalLogic.description();
	}

	@Override
	public Set<Integer> getSupportSet(int basePropositionIndex) {
		return this.propositionalLogic.getSupportSet(basePropositionIndex);
	}

	@Override
	public Set<Integer> getTruthSet(int objectId) {
		return this.propositionalLogic.getTruthSet(objectId);
	}

	@Override
	public Population population() {
		return dataTable.population();
	}

	@Override
	public SerialForm serialForm() {
		List<Integer> attributeIndices = propositionalLogic.getPropositions().stream()
				.map(p -> ((AttributeBasedProposition<?>) p).getAttribute())
				.map(a -> dataTable.getAttributes().indexOf(a)).collect(Collectors.toList());
		List<Constraint<?>> constraints = propositionalLogic.getPropositions().stream()
				.map(p -> ((AttributeBasedProposition<?>) p).getConstraint()).collect(Collectors.toList());
		return new DefaultTableBasedPropositionalLogicSerialForm(this.identifier(), this.name(), this.description(),
				this.dataTable.identifier(), attributeIndices, constraints);
	}

}
