/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.sequences;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalLogic;
import de.unibonn.realkd.data.sequences.SequenceDatabase.DefaultSequenceDatabase;
import de.unibonn.realkd.data.table.attribute.Attribute;

/**
 *
 * 
 * @author Sandy Moens
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 *
 */
public class SequenceDatabaseFromTableFactory {

	private static final Logger LOGGER = Logger.getLogger(SequenceDatabaseFromTableFactory.class.getName());

	private String groupingAttributeName;
	private String timeAttributeName;

	public SequenceDatabase build(TableBasedPropositionalLogic propositionalLogic) {
		LOGGER.fine("Creating sequences");

		Map<Integer, SequenceTransaction> sequences = newHashMap();
		
		List<AttributeBasedProposition<?>> groupingPropositions = getGroupingPropositions(propositionalLogic);
		List<AttributeBasedProposition<?>> datePropositions = getTimePropositions(propositionalLogic);

		Collections.sort(datePropositions, new Comparator<AttributeBasedProposition<?>>() {
			@Override
			public int compare(AttributeBasedProposition<?> o1, AttributeBasedProposition<?> o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});
		List<Integer> groupingPropositionsIds = groupingPropositions.stream().map(p -> p.getId()).collect(Collectors.toList());
		List<Integer> timeStampIds = datePropositions.stream().map(p -> p.getId()).collect(Collectors.toList());
		
		List<Proposition> propositions = propositionalLogic.getPropositions();
		SimpleDateFormat dt = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		
		for(AttributeBasedProposition<?> idProposition: getGroupingPropositions(propositionalLogic)) {
			for(int recordId: propositionalLogic.getSupportSet(idProposition.getId())) {
				Set<Integer> ts = newHashSet(propositionalLogic.getTruthSet(recordId));
				ts.retainAll(timeStampIds);
				Set<Integer> id = newHashSet(propositionalLogic.getTruthSet(recordId));
				id.retainAll(groupingPropositionsIds);
				Set<Integer> events = newHashSet(propositionalLogic.getTruthSet(recordId));
				events.removeAll(ts);
				events.removeAll(groupingPropositionsIds);
				
				try {
					Date date = dt.parse(propositions.get(ts.iterator().next()).toString().split("=")[1]);
					List<Proposition> eventList = events.stream().map(e -> propositions.get(e)).collect(Collectors.toList());
					SequenceEvent event = new SequenceEvent(date, eventList);
					
					int groupingId = id.iterator().next();
					SequenceTransaction sequence = sequences.get(groupingId);
					if(sequence == null) {
						sequences.put(groupingId, sequence = new SequenceTransaction());
					}
					sequence.addEvent(event);
				} catch (ParseException e1) {
					e1.printStackTrace();
				} 
			}
		}
		
		
		
		LOGGER.info("Done creating sequences (" + sequences.size() + " sequences created)");

		return DefaultSequenceDatabase.newSequenceDatabase("Sequences for " + propositionalLogic.name(), "Sequences about " + propositionalLogic.description(), newArrayList(sequences.values()), propositions);
	}

	private List<AttributeBasedProposition<?>> getGroupingPropositions(TableBasedPropositionalLogic propositionalLogic) {
		for(Attribute<?> attribute: propositionalLogic.getDatatable().getAttributes()) {
			if(attribute.name().equals(groupingAttributeName)) {
				return propositionalLogic.getAttributeBasedPropositionsAbout(attribute);
			}
		}
		return ImmutableList.of();
	}
	
	private List<AttributeBasedProposition<?>> getTimePropositions(TableBasedPropositionalLogic propositionalLogic) {
		for(Attribute<?> attribute: propositionalLogic.getDatatable().getAttributes()) {
			if(attribute.name().equals(timeAttributeName)) {
				return propositionalLogic.getAttributeBasedPropositionsAbout(attribute);
			}
		}
		return ImmutableList.of();
	}
	
	public SequenceDatabaseFromTableFactory() {
		this.groupingAttributeName = "id";
		this.timeAttributeName = "date";
	}
	
	public void groupingAttributeName(String groupingAttributeName) {
		this.groupingAttributeName=groupingAttributeName;
	}
	
	public void timeAttributeName(String timeAttributeName) {
		this.timeAttributeName=timeAttributeName;
	}

}
