/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.sequences;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.workspace.Entity;
import de.unibonn.realkd.data.Populations;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;

/**
 * Sequences of binary statements about data objects 
 * 
 * @author Sandy Moens
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 *
 */
public interface SequenceDatabase extends PropositionalLogic {

	public List<SequenceTransaction> sequences();
	
	/**
	 * Provides all base propositions available in this propositional logic.
	 * 
	 */
	public abstract List<Proposition> propositions();
	
	public class DefaultSequenceDatabase implements SequenceDatabase {

		public static SequenceDatabase newSequenceDatabase(String name, String description, List<SequenceTransaction> sequences, List<Proposition> propositions) {
				return new DefaultSequenceDatabase(name, description, sequences, propositions);
		}

		private String name;
		private String description;
		private List<SequenceTransaction> sequences;
		private List<Proposition> propositions;
		
		private DefaultSequenceDatabase(String name, String description, List<SequenceTransaction> sequences, List<Proposition> propositions) {
			this.name = name;
			this.description = description;
			this.sequences = ImmutableList.copyOf(sequences);
			this.propositions = propositions;
		}
		
		public String identifier() {
			return this.name;
		}
		
		@Override
		public String name() {
			return this.name;
		}

		@Override
		public String description() {
			return this.description;
		}

		@Override
		public List<SequenceTransaction> sequences() {
			return this.sequences;		
		}

		@Override
		public List<Proposition> propositions() {
			return this.propositions;
		}

		public List<Proposition> getPropositions() {
			return this.propositions;
		}

		@Override
		public Population population() {
			return Populations.population("Sequences", this.sequences.size());
		}

		@Override
		public Set<Integer> getSupportSet(int basePropositionIndex) {
			return newHashSet();
		}

		@Override
		public Set<Integer> getTruthSet(int objectId) {
			return newHashSet();
		}

	}
	
}