/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.sequences;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import de.unibonn.realkd.data.propositions.Proposition;

/**
 * @author Sandy Moens
 *
 * @since 0.3.0
 * 
 * @version 0.3.0
 */
public class SequenceEvent implements Comparable<SequenceEvent> {
	
	private Date date;
	private Set<Proposition> propositions;

	public SequenceEvent(Date date, Collection<Proposition> propositions) {
		this.date = date;
		this.propositions = ImmutableSet.copyOf(propositions);
	}
	
	public Date date() {
		return this.date;
	}
	
	public Set<Proposition> propositions() {
		return this.propositions;
	}

	@Override
	public int compareTo(SequenceEvent o) {
		return this.date().compareTo(o.date());
	}
	
	public String toString() {
		return new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").format(date) + " " + propositions + "";
	}
	
}
