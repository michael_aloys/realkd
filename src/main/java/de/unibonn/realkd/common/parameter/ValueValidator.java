/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.common.parameter;

import de.unibonn.realkd.common.parameter.ValueValidator;

/**
 * Value validators are objects to which {@link DefaultParameter}, the default
 * implementation of {@link Parameter}, delegates the actual value validity
 * check after initial checks have been performed that are mandatory by the
 * contract of {@link Parameter}. These checks are
 * <ul>
 * <li>whether the context of the parameter is valid, all upstream parameters
 * that this parameter depends on are set to valid values,
 * <li>whether the value is not null,
 * <li>whether the value is in a valid state if the value itself is a parameter
 * container.
 * </ul>
 * 
 * @author mboley
 * 
 * @see {@link DefaultParameter}
 * 
 */
public interface ValueValidator<T> {

	public boolean valid(T value);

	/**
	 * This should actually not be needed because the null check is performed by
	 * DefaultParameter before the actual value validator is called
	 */
	public static final ValueValidator<Object> VALUE_NOT_NULL_VALIDATOR = new ValueValidator<Object>() {

		@Override
		public boolean valid(Object value) {
			return value != null;
		}
	};

	/**
	 * Trivial validator singleton that accepts all values.
	 */
	public static final ValueValidator<Object> ALWAYS_VALID_VALIDATOR = new ValueValidator<Object>() {

		@Override
		public boolean valid(Object value) {
			return true;
		}

	};
	
	public static class LargerThanThresholdValidator<U> implements ValueValidator<U> {

		private final Comparable<U> threshold;

		public LargerThanThresholdValidator(Comparable<U> threshold) {
			this.threshold=threshold;
		}
		
		@Override
		public boolean valid(U value) {
			return threshold.compareTo(value)<0;
		}
		
	}

	public static class DoubleInOpenRangeValidator implements
			ValueValidator<Double> {

		private final Double lower;

		private final Double upper;

		public DoubleInOpenRangeValidator(Double lower, Double upper) {
			if (upper < lower) {
				throw new IllegalArgumentException(
						"Lower must be lower than upper.");
			}
			this.lower = lower;
			this.upper = upper;
		}

		@Override
		public boolean valid(Double value) {
			return (lower <= value && value <= upper);
		}

	}

	public static class DoubleInClosedRangeValidator implements
			ValueValidator<Double> {

		private final Double lower;

		private final Double upper;

		public DoubleInClosedRangeValidator(Double lower, Double upper) {
			if (upper < lower) {
				throw new IllegalArgumentException(
						"Lower must be lower than upper.");
			}
			this.lower = lower;
			this.upper = upper;
		}

		@Override
		public boolean valid(Double value) {
			return (lower <= value && value <= upper);
		}

	}

}
