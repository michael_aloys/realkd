/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.common.parameter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.realkd.util.Strings;

/**
 * Default implementation of sub-collection parameter. Can be configured as
 * either sublist or subset parameter.
 * 
 * @author Mario Boley
 * @author Bo Kang
 * 
 * @since 0.1.0
 * 
 * @version 0.2.1
 * 
 */
public class DefaultSubCollectionParameter<G, T extends Collection<? extends G>>
		implements SubCollectionParameter<G, T>, DependentParameter<T> {

	private static Logger LOGGER = Logger.getLogger(DefaultSubCollectionParameter.class.getName());

	/**
	 * Implementation of StringParser that parses a single string in js format
	 * ([e1,e2,...]) to the sub-collection of objects of getCollection whose
	 * string representation corresponds to 'e1, e2,...'. If a matching element
	 * is not found for a string specifier it is ignored and a warning is
	 * logged.
	 */
	public static class JsonStringToCollectionParser<G, T extends Collection<G>> implements Function<String, T> {

		private final Supplier<Collection<G>> rangeCollectionSupplier;

		private final Function<List<G>, T> targetCollectionConverter;

		public JsonStringToCollectionParser(Supplier<Collection<G>> collectionComputer,
				Function<List<G>, T> targetCollectionConverter) {
			this.rangeCollectionSupplier = collectionComputer;
			this.targetCollectionConverter = targetCollectionConverter;
		}

		private Optional<G> findByString(String searchString) {
			Collection<G> range = rangeCollectionSupplier.get();
			for (G element : range) {
				if (element.toString().trim().equals(searchString)) {
					return Optional.of(element);
				}
			}
			LOGGER.warning("Could not find element '" + searchString + "' in collection.");
			return Optional.empty();
		}

		@Override
		public T apply(String input) {
			List<String> argumentAsStrings = Strings.jsonArrayToStringList(input);
			Stream<Optional<G>> foundElementOptions = argumentAsStrings.stream().map(s -> findByString(s));
			List<G> elementsList = foundElementOptions.filter(o -> o.isPresent()).map(o -> o.get())
					.collect(Collectors.toList());
			return targetCollectionConverter.apply(elementsList);
		}
	}

	public static class ValidateIsSubcollection<G, T extends Collection<G>> implements ValueValidator<T> {

		private CollectionComputer<T> listComputer;

		public ValidateIsSubcollection(CollectionComputer<T> listComputer) {
			this.listComputer = listComputer;
		}

		@Override
		public boolean valid(T value) {
			return listComputer.computeCollection().containsAll(value);
		}

	}

	public interface CollectionComputer<T> {

		/**
		 * 
		 * @return non-null range
		 */
		public T computeCollection();

	}

	private static final String HINT = "Choose an element from list";

	private final DefaultParameter<T> defaultParameter;

	private final CollectionComputer<T> rangeComputer;

	private DefaultSubCollectionParameter(String name, String description, Class<?> type,
			CollectionComputer<T> rangeComputer, Function<String, T> parser, ValueValidator<T> validator,
			Supplier<T> initializer, Parameter<?>... dependenParams) {
		this.rangeComputer = rangeComputer;
		this.defaultParameter = new DefaultParameter<T>(name, description, type, parser, validator, HINT, initializer,
				dependenParams);
	}

	public static <G> DefaultSubCollectionParameter<G, List<G>> getDefaultSubListParameter(String name,
			String description, CollectionComputer<List<G>> rangeComputer, Parameter<?>... dependsOnParameters) {
		return new DefaultSubCollectionParameter<G, List<G>>(name, description, List.class, rangeComputer,
				new JsonStringToCollectionParser<G, List<G>>(() -> rangeComputer.computeCollection(),
						l -> ImmutableList.copyOf(l)),
				new ValidateIsSubcollection<G, List<G>>(rangeComputer), () -> new ArrayList<>(), dependsOnParameters);

	}

	public static <G> DefaultSubCollectionParameter<G, List<G>> getDefaultSubListParameter(String name,
			String description, CollectionComputer<List<G>> rangeComputer, ValueValidator<List<G>> validator,
			Parameter<?>... dependsOnParameters) {
		return new DefaultSubCollectionParameter<G, List<G>>(name, description, List.class, rangeComputer,
				new JsonStringToCollectionParser<G, List<G>>(() -> rangeComputer.computeCollection(),
						l -> ImmutableList.copyOf(l)),
				validator, () -> new ArrayList<>(), dependsOnParameters);
	}

	/**
	 * 
	 * @param name
	 * @param description
	 * @param rangeComputer
	 * @param validator
	 * @param initializer
	 * @param dependsOnParameters
	 * @return
	 */
	public static <G> DefaultSubCollectionParameter<G, List<G>> subListParameter(String name,
			String description, CollectionComputer<List<G>> rangeComputer, ValueValidator<List<G>> validator,
			Supplier<List<G>> initializer, Parameter<?>... dependsOnParameters) {
		return new DefaultSubCollectionParameter<G, List<G>>(name, description, List.class, rangeComputer,
				new JsonStringToCollectionParser<G, List<G>>(() -> rangeComputer.computeCollection(),
						l -> ImmutableList.copyOf(l)),
				validator, initializer, dependsOnParameters);
	}

	/**
	 * Creates a parameter with valid values being subsets of some dynamic range
	 * collection. Value is initialized to the empty set.
	 * 
	 */
	public static <G> DefaultSubCollectionParameter<G, Set<G>> subSetParameter(String name, String description,
			CollectionComputer<Set<G>> rangeComputer, Parameter<?>... dependsOnParameters) {
		return subSetParameter(name, description, rangeComputer, () -> new HashSet<>(), dependsOnParameters);
	}

	/**
	 * Creates a parameter with valid values being subsets of some dynamic range
	 * collection. Value is initialized by some arbitrary initializer.
	 * 
	 */
	public static <G> DefaultSubCollectionParameter<G, Set<G>> subSetParameter(String name, String description,
			CollectionComputer<Set<G>> rangeComputer, Supplier<Set<G>> initializer,
			Parameter<?>... dependsOnParameters) {
		JsonStringToCollectionParser<G, Set<G>> toImmutableSetParser = new JsonStringToCollectionParser<G, Set<G>>(
				() -> rangeComputer.computeCollection(), l -> ImmutableSet.copyOf(l));
		ValidateIsSubcollection<G, Set<G>> validator = new ValidateIsSubcollection<G, Set<G>>(rangeComputer);
		return new DefaultSubCollectionParameter<G, Set<G>>(name, description, Set.class, rangeComputer,
				toImmutableSetParser, validator, initializer, dependsOnParameters);
	}

	@Override
	public final T getCollection() {
		// TODO: is the following cast save? Perhaps not!
		if (!isContextValid()) {
			return (T) ImmutableSet.of();
		}
		return rangeComputer.computeCollection();
	}

	@Override
	public final boolean isContextValid() {
		return this.defaultParameter.isContextValid();
	}

	@Override
	public final List<Parameter<?>> getDependsOnParameters() {
		return this.defaultParameter.getDependsOnParameters();
	}

	@Override
	public final boolean isValid() {
		return this.defaultParameter.isValid();
	}

	@Override
	public final String getValueCorrectionHint() {
		return this.defaultParameter.getValueCorrectionHint();
	}

	@Override
	public final String getName() {
		return this.defaultParameter.getName();
	}

	@Override
	public final String getDescription() {
		return this.defaultParameter.getDescription();
	}

	@Override
	public final Class<?> getType() {
		return this.defaultParameter.getType();
	}

	@Override
	public final void set(T value) {
		this.defaultParameter.set(value);
	}

	@Override
	public final void setByString(String value) {
		this.defaultParameter.setByString(value);
	}

	@Override
	public final T current() {
		return defaultParameter.current();
	}

	private Map<ParameterListener, ParameterListener> listenerReferrer = new HashMap<>();

	@Override
	public final void addListener(final ParameterListener listener) {
		// adding referrer that notifies listener with update for this parameter
		// (instead of wrapped)
		ParameterListener referrer = new ParameterListener() {
			@Override
			public void notifyValueChanged(Parameter<?> parameter) {
				listener.notifyValueChanged(DefaultSubCollectionParameter.this);
			}
		};
		this.defaultParameter.addListener(referrer);

		// storing referrer for listener in order to support deregistration
		this.listenerReferrer.put(listener, referrer);
	}

	@Override
	public void removeListener(ParameterListener listener) {
		if (!listenerReferrer.containsKey(listener)) {
			return;
		}
		this.defaultParameter.removeListener(listenerReferrer.get(listener));
	}

	@Override
	public boolean hidden() {
		return defaultParameter.hidden();
	}

}
