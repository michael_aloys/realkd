/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.common.parameter;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter.RangeComputer;

/**
 * Provides static factory methods for constructing different default parameter
 * objects.
 * 
 * NOTES to 0.2.1: in future versions other factory methods (for range
 * enumerable and so on) should be moved here. Also the return types should be
 * relaxed to interfaces.
 * 
 * @author Mario Boley
 * 
 * @since 0.2.1
 * 
 * @version 0.3.1
 * 
 */
public class Parameters {

	public static Parameter<String> stringParameter(String name, String description, String initValue,
			ValueValidator<? super String> validator, String helperText) {
		return new DefaultParameter<String>(name, description, String.class, initValue, input -> input, validator,
				helperText);
	}

	public static Parameter<Double> doubleParameter(String name, String description, double initValue,
			ValueValidator<? super Double> validator, String helperText) {
		return new DefaultParameter<Double>(name, description, Double.class, initValue, input -> Double.valueOf(input),
				validator, helperText);
	}

	public static Parameter<Integer> integerParameter(String name, String description, int initValue,
			ValueValidator<? super Integer> validator, String helperText) {
		return new DefaultParameter<Integer>(name, description, Integer.class, initValue,
				input -> Integer.valueOf(input), validator, helperText);
	}

	public static Parameter<Integer> dependentIntegerParameter(String name, String description,
			ValueValidator<? super Integer> validator, String helperText, Supplier<Integer> initializer,
			Parameter<?>... dependsOnParams) {
		return new DefaultParameter<Integer>(name, description, Integer.class, input -> Integer.valueOf(input),
				validator, helperText, initializer, dependsOnParams);
	}

	/**
	 * @param name
	 *            the name of resulting parameter
	 * @param description
	 *            the description of resulting parameter
	 * @param type
	 *            the guaranteed runtime type of values of resulting parameter
	 * @param parameter
	 *            another parameter that resulting parameter depends on
	 * @param transformers
	 *            list of transformers to determine current range or resulting
	 *            parameter based on current state of other parameter
	 * @return a range enumerable parameter of type RangeEnumerableParameter
	 *         <T> the current valid range of which depends on another given
	 *         parameters
	 * 
	 * @param <T>
	 *            the value type of the resulting parameter
	 * 
	 * @param
	 * 			<P>
	 *            the type of the other parameter that the resulting parameter
	 *            depends on
	 * 
	 */
	public static <P extends Parameter<?>, T> RangeEnumerableParameter<T> dependentRangeEnumerableParameter(String name,
			String description, Class<? super T> type, final P parameter, List<Function<P, List<T>>> transformers) {
		return Parameters.rangeEnumerableParameter(name, description, type, new RangeComputer<T>() {
			@Override
			public List<T> computeRange() {
				Stream<T> applicableProcedures = transformers.stream().flatMap(p -> p.apply(parameter).stream());
				return applicableProcedures.collect(Collectors.toList());
			}
		}, parameter);
	}

	public static <T> DefaultRangeEnumerableParameter<T> rangeEnumerableParameter(String name, String description,
			Class<?> type, RangeComputer<? extends T> rangeComputer, Parameter<?>... dependenParams) {
		return new DefaultRangeEnumerableParameter<T>(name, description, type, rangeComputer, dependenParams);
	}

	public static <T> DefaultRangeEnumerableParameter<T> rangeEnumerableParameter(String name, String description,
			Class<?> type, RangeComputer<? extends T> rangeComputer, Supplier<Boolean> hidden,
			Parameter<?>... dependenParams) {
		return new DefaultRangeEnumerableParameter<T>(name, description, type, rangeComputer, hidden, dependenParams);
	}

	public static <T> RangeEnumerableParameter<T> filteredRangeEnumerableParameter(RangeEnumerableParameter<T> original,
			List<String> validOptionList) {
		RangeComputer<T> filteredRangeComputer = () -> original.getRange().stream()
				.filter(v -> validOptionList.contains(v.toString())).collect(Collectors.toList());
		return rangeEnumerableParameter(original.getName(), original.getDescription(), original.getType(),
				filteredRangeComputer, original.getDependsOnParameters().toArray(new Parameter[0]));
	}

	/**
	 * @return range enumerable parameter with options "true" and "false"
	 *         (default "true")
	 */
	public static RangeEnumerableParameter<Boolean> booleanParameter(String name, String description) {
		return rangeEnumerableParameter(name, description, Boolean.class,
				() -> ImmutableList.of(new Boolean(true), new Boolean(false)));
	}

}
