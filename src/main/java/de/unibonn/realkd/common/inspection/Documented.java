package de.unibonn.realkd.common.inspection;

/**
 * Interface of objects that provide a user-interpretable description of
 * themselves.
 * 
 * @author Mario Boley
 *
 * @since 0.1.1
 * 
 * @version 0.1.1.1
 * 
 */
public interface Documented {

	/**
	 * @return name that is short, expressive, and user-interpretable
	 * 
	 */
	public String getName();

	/**
	 * @return extended description of object that is user-interpretable
	 */
	public String getDescription();

}
