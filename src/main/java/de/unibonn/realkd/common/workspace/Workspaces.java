/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.common.workspace;

import static java.util.stream.Collectors.toList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.function.Consumer;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import de.unibonn.realkd.common.JsonSerialization;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;

/**
 * Provides static factory methods for the creation of workspaces.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.3.0
 *
 */
public class Workspaces {

	private static Logger LOGGER = Logger.getLogger(Workspace.class.getName());

	/**
	 * 
	 * @return a new empty workspace
	 */
	public static Workspace workspace() {
		return new DefaultWorkspace();
	}

	public static Workspace fileSystemBasedWorkspace(Path folder) {
		WorkspaceWithFileBackups workspace = new WorkspaceWithFileBackups(folder);
		return workspace;
	}

	private static class DefaultWorkspace implements Workspace {

		private final Map<String, Entity> entities = new HashMap<>();

		private final Map<String, Future<Entity>> futureEntities = new HashMap<>();

		@Override
		public void add(Entity entity) {
			if (entities.containsKey(entity.identifier()) || futureEntities.containsKey(entity.identifier())) {
				throw new IllegalArgumentException(
						"workspace already contains entity with id '" + entity.identifier() + "'");
			}
			entities.put(entity.identifier(), entity);
			entity.dependencies().forEach(d -> {
				if (!this.contains(d.identifier()))
					add(d);
			});
		}
		
		@Override
		public void overwrite(Entity entity) {
			if (futureEntities.containsKey(entity.identifier())) {
				throw new IllegalArgumentException(
						"workspace is already waiting for future entity with id '" + entity.identifier() + "'");
			}
			entities.put(entity.identifier(), entity);
			entity.dependencies().forEach(d -> {
				if (!this.contains(d.identifier()))
					add(d);
			});
		}

		@Override
		public <T extends Entity> List<Entity> getEntitiesOfType(Class<T> clazz) {
			List<Entity> result = new ArrayList<Entity>();
			for (Object object : entities.values()) {
				if (clazz.isInstance(object)) {
					result.add(clazz.cast(object));
				}
			}
			return result;
		}

		@Override
		public List<DataTable> getAllDatatables() {
			List<DataTable> result = new ArrayList<>();
			for (Object dataTable : getEntitiesOfType(DataTable.class)) {
				result.add((DataTable) dataTable);
			}
			return result;
		}

		@Override
		public List<PropositionalLogic> getAllPropositionalLogics() {
			List<PropositionalLogic> result = new ArrayList<>();
			for (Object propLogic : getEntitiesOfType(PropositionalLogic.class)) {
				result.add((PropositionalLogic) propLogic);
			}
			return result;
		}

		@Override
		public boolean contains(String id) {
			return entities.containsKey(id) || futureEntities.containsKey(id);
		}

		@Override
		public boolean contains(String id, Class<? extends Entity> type) {
			return contains(id) && type.isInstance(get(id));
		}

		// TODO: return type should be changed to validation (there are three
		// different kinds of errors: no such entity, entity computation failed,
		// entity computation interrupted)
		@Override
		public Entity get(String id) {
			if (entities.containsKey(id)) {
				return entities.get(id);
			} else if (futureEntities.containsKey(id)) {
				Future<Entity> future = futureEntities.get(id);
				try {
					return future.get();
				} catch (InterruptedException | ExecutionException e) {
					throw new IllegalArgumentException(e);
				}
			}
			throw new IllegalArgumentException("No enitity with id '" + id + "'");
		}

		@SuppressWarnings("unchecked")
		@Override
		public <T extends Entity> Optional<T> get(String id, Class<T> type) {
			if (contains(id, type)) {
				return (Optional<T>) Optional.of(get(id));
			} else {
				return Optional.empty();
			}
		}

		@Override
		public Future<Entity> getAsFuture(String id) {
			if (entities.containsKey(id)) {
				Entity result = entities.get(id);
				return new FutureTask<>(() -> result);
			} else if (futureEntities.containsKey(id)) {
				return futureEntities.get(id);
			}
			throw new IllegalArgumentException("No enitity with id '" + id + "'");
		}

		@Override
		public void addFuture(String id, Future<Entity> futureEntity) {
			if (entities.containsKey(id) || futureEntities.containsKey(id)) {
				throw new IllegalArgumentException("workspace already contains entity with id '" + id + "'");
			}
			futureEntities.put(id, futureEntity);
			ExecutorService executor = Executors.newSingleThreadExecutor();
			executor.execute(() -> {
				try {
					LOGGER.info("thread started waiting for computation of entity " + id);
					Entity entity = futureEntity.get();
					if (!id.equals(entity.identifier())) {
						throw new IllegalArgumentException("id of computed entity '" + entity.identifier()
								+ "' does not match reserved id '" + id + "'");
					}
					LOGGER.info("adding entity " + id);

					// TODO does 'this' really refer to the outer workspace
					// object here?
					synchronized (this) {
						futureEntities.remove(id);
						this.add(entity);
					}
				} catch (ExecutionException | InterruptedException e) {
					LOGGER.warning(() -> e.getMessage());
					LOGGER.info("removing future entity " + id);
					futureEntities.remove(id);
					// no entity to add, future will still be returned; for
					// non-future aware clients, get will throw illegal
					// argument exception
				}
			});
			executor.shutdown();
		}

		@Override
		public Set<String> ids() {
			return Sets.union(entities.keySet(), futureEntities.keySet());
		}

	}

	private final static class WorkspaceWithFileBackups extends DefaultWorkspace {

		private final Path path;

		private WorkspaceWithFileBackups(Path folder) {
			this.path = folder;
			restoreEntities();
		}

		private void deserialize(SerialForm form, Set<SerialForm> others) {
			if (this.contains(form.identifier())) {
				LOGGER.info("Skipping deserialization of '" + form.identifier()
						+ "' (entity with same id already in workspace)");
				return;
			}
			for (String dependency : form.dependencyIds()) {
				if (!this.contains(dependency)) {
					Optional<SerialForm> serialFormOfDependency = others.stream()
							.filter(o -> o.identifier().equals(dependency)).findFirst();
					if (!serialFormOfDependency.isPresent()) {
						LOGGER.warning("Cannot deserialize '" + form.identifier() + "' because of missing dependency '"
								+ dependency + "'");
						return;
					}
					deserialize(serialFormOfDependency.get(),
							Sets.difference(others, ImmutableSet.of(serialFormOfDependency)));
				}
			}
			Entity entity = form.get(this);
			this.add(entity);
			// entities.put(entity.identifier(), entity);
			System.out.println("Restored entity " + entity.identifier());
		}

		private void restoreEntities() {
			List<SerialForm> serialForms = readSerialForms();
			Consumer<SerialForm> action = f -> {
				this.deserialize(f, Sets.difference(ImmutableSet.copyOf(serialForms), ImmutableSet.of(f)));
			};
			serialForms.forEach(action);
		}

		private List<SerialForm> readSerialForms() {
			List<SerialForm> serialForms = new ArrayList<>();
			try {
				List<Path> jsonFiles = Files.list(path).filter(p -> p.toString().endsWith(".jrke")).collect(toList());
				for (Path file : jsonFiles) {
					try (BufferedReader reader = Files.newBufferedReader(file)) {
						String json = reader.lines().collect(Collectors.joining());
						serialForms.add(JsonSerialization.fromJson(json, SerialForm.class));
					}
				}
			} catch (IOException e) {
				throw new IllegalArgumentException("could not read directory content");
			}
			return serialForms;
		}

		@Override
		public void add(Entity entity) {
			super.add(entity);
			storeInFile(entity);
		}
		
		public void overwrite(Entity entity) {
			super.overwrite(entity);
			storeInFile(entity);
		}

		private void storeInFile(Entity entity) {
			if (entity instanceof HasSerialForm) {
				Path backupPath = path.resolve(entity.identifier() + ".jrke");
				String json = JsonSerialization.toPrettyJson(((HasSerialForm) entity).serialForm(), SerialForm.class);
				Charset charset = Charset.forName("UTF-8");
				try (BufferedWriter writer = Files.newBufferedWriter(backupPath, charset)) {
					writer.write(json);
					writer.flush();
					writer.close();
				} catch (IOException e) {
					LOGGER.severe(e.getMessage());
				}
			}
		}

	}

}
