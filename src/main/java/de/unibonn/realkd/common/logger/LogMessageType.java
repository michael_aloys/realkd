/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.common.logger;

/**
 * This enum defines the log message types that are accepted by the logger. Each
 * message type has its verbosity level, it is the same as the order in which
 * the constants are declared.
 */
public enum LogMessageType {
	/**
	 * level 0: General output concerning system components (e.g.
	 * "DataTable loaded", "System startup" etc.)
	 */
	GLOBAL_STATE_CHANGE,

	/**
	 * level 1: Standard log-level. Events that are related to components from
	 * the core interacting with other components. (e.g. UI)
	 */
	INTER_COMPONENT_MESSAGE,

	/**
	 * level 2: Relevant intra-component log events
	 */
	INTRA_COMPONENT_MESSAGE,

	/**
	 * level 3: (Debug level) Everything else that might be of interest (e.g.
	 * reward info)
	 */
	DEBUG_MESSAGE
}
