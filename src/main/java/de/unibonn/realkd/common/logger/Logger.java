/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.common.logger;

/*
 * Copyright (c) 2014 Fraunhofer IAIS. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * This class provides a light weight logging mechanism.
 * 
 * <p>
 * For each logging event the client four arguments has to be specified:
 * <ul>
 * <li>
 * supperChannel (optional): defines the folder name to which the log files of
 * the corresponding sub-channel should go. If the supper channel is undefined,
 * the log file will be created in the default folder.</li>
 * <li>
 * channel: defines the channel on which this logging event will publish the
 * message. It also defines the corresponding log file name.</li>
 * <li>
 * clientMsg: the message that the client wants to log.</li>
 * <li>
 * msgVerbosity: the verbosity associated with this logging event. Detailed
 * information see {@link LogMessageType}.</li>
 * </ul>
 * 
 * <p>
 * For each logging event, the message will be pushed to types of consumers:
 * Console and Files. The channel wise verbosity agreed by both consumers can be
 * specified in the property file: "consoleVerbosity.properties" and
 * "fileVerbosity.properties". If the verbosity level is undefined, the default
 * level will be assigned. see also {@link LogMessageType}.
 * 
 * <p>
 * This class is a member of the <a href="{@docRoot}/index.html">Logging
 * Framework</a>.
 * 
 * @author Bo Kang
 * 
 */
public class Logger {

	private static final String LOGGER_PROPERTIES_PACKAGE = "";// "/de/unibonn/creedo/common/properties/";

	private static final LogMessageType DEFAULT_VERBOSITY = LogMessageType.GLOBAL_STATE_CHANGE;

	public static final String DEFAULT_SUPERCHANNEL = "default";

	/**
	 * This constant is defined in order to get the name of class that calling
	 * log method. The current order of caller class in the stack is :
	 * 
	 * Thread - Caller - log(supperChannel) - log(defaultChannel) -
	 * composeLogContent
	 * 
	 * hence the current method in stack where the log is composed is 4 (start
	 * at 0). Warning: if the upper calling order has to be changed, please
	 * change this constant accordingly.
	 */
	private static final int BASE_LOGGER_CALLING_LEVEL = 4;

	private static final String PATH_TO_LOG_FOLDER = System
			.getProperty("user.home") + "/OCMlogs/";

	private static final Logger INSTANCE = new Logger();

	private static Map<String, LogMessageType> consoleVerbositySpec;

	private static Map<String, LogMessageType> fileVerbositySpec;

	private Logger() {
		loadConfig("consoleVerbosity.properties", "fileVerbosity.properties");
	}

	public static Logger getInstance() {
		return INSTANCE;
	}

	private void loadConfig(String consoleVerbosityConfig,
			String fileVerbosityConfig) {
		Properties properties = new Properties();
		Logger.consoleVerbositySpec = new HashMap<>();

		try {
			properties.load(getClass().getResourceAsStream(
					LOGGER_PROPERTIES_PACKAGE + consoleVerbosityConfig));
			for (String key : properties.stringPropertyNames()) {
				Logger.consoleVerbositySpec.put(key,
						LogMessageType.values()[Integer.valueOf(properties
								.getProperty((key)))]);
			}
			System.out.println("Loaded " + properties.size()
					+ " console verbosity settings");
		} catch (IOException e) {
			System.out
					.println("Error reading logger console-verbosity configuration file: "
							+ consoleVerbosityConfig);
		}

		properties = new Properties();
		Logger.fileVerbositySpec = new HashMap<>();

		try {
			properties.load(getClass().getResourceAsStream(
					LOGGER_PROPERTIES_PACKAGE + fileVerbosityConfig));
			for (String key : properties.stringPropertyNames()) {
				Logger.fileVerbositySpec.put(key,
						LogMessageType.values()[Integer.valueOf(properties
								.getProperty((key)))]);
			}
			System.out.println("Loaded " + properties.size()
					+ " file verbosity settings (log root folder is "
					+ PATH_TO_LOG_FOLDER + ")");
		} catch (IOException e) {
			System.out
					.println("Error reading logger file-verbosity configuration file: "
							+ fileVerbosityConfig);
		}
	}

	protected static void logFromChannel(String channel, String clientMsg,
			LogMessageType msgVerbosity) {
		performLog(DEFAULT_SUPERCHANNEL, channel, clientMsg, msgVerbosity, 2);
	}

	protected static void logFromChannel(String superChannel, String channel,
			String clientMsg, LogMessageType msgVerbosity) {
		performLog(superChannel, channel, clientMsg, msgVerbosity, 2);
	}

	public static void log(String channel, String clientMsg,
			LogMessageType msgVerbosity) {
		performLog(DEFAULT_SUPERCHANNEL, channel, clientMsg, msgVerbosity, 0);
	}

	public static void log(String superChannel, String channel,
			String clientMsg, LogMessageType msgVerbosity) {
		performLog(superChannel, channel, clientMsg, msgVerbosity, 0);
	}

	private static void performLog(String superChannel, String channel,
			String clientMsg, LogMessageType msgVerbosity, int callingLevelDelta) {
		String logMsg = composeLogContent(superChannel, channel, clientMsg,
				callingLevelDelta);
		pushToConsole(logMsg, channel, msgVerbosity);
		pushToFile(superChannel, channel, logMsg, msgVerbosity);
	}

	private static void pushToConsole(String logMsg, String channel,
			LogMessageType msgVerbosity) {
		if (hasValidConsoleVerbosity(channel, msgVerbosity)) {
			System.out.println(logMsg);
		}
	}

	private static void pushToFile(String superChannel, String channel,
			String logMsg, LogMessageType msgVerbosity) {
		if (hasValidFileVerbosity(channel, msgVerbosity)) {
			createFolderIfNoExist(PATH_TO_LOG_FOLDER);
			createFolderIfNoExist(PATH_TO_LOG_FOLDER + "/" + superChannel);

			String fileName = superChannel + "/" + channel + ".log";
			String pathToFile = PATH_TO_LOG_FOLDER + fileName;
			try {
				File file = createFileIfNoExist(pathToFile);
				FileWriter fw = new FileWriter(file, true);
				fw.write(logMsg + System.getProperty("line.separator"));
				fw.close();
			} catch (IOException e) {
				System.out.println("Error writing to file: " + pathToFile);
			}
		}
	}

	private static File createFileIfNoExist(String pathToFile)
			throws IOException {
		File file = new File(pathToFile);
		if (!file.exists()) {
			file.createNewFile();
		}
		return file;
	}

	private static void createFolderIfNoExist(String pathToFolder) {
		File file = new File(pathToFolder);

		if (!file.exists()) {
			file.mkdir();
		}
	}

	private static boolean hasValidConsoleVerbosity(String channel,
			LogMessageType msgVerbosity) {
		return getChannelVerbosity(channel, consoleVerbositySpec).compareTo(
				msgVerbosity) >= 0;
	}

	private static boolean hasValidFileVerbosity(String channel,
			LogMessageType msgVerbosity) {
		return getChannelVerbosity(channel, fileVerbositySpec).compareTo(
				msgVerbosity) >= 0;
	}

	private static String composeLogContent(String superChannel,
			String channel, String msg) {
		return composeLogContent(superChannel, channel, msg, 0);
	}

	private static String composeLogContent(String superChannel,
			String channel, String msg, int callingLevelDelta) {
		StringBuilder sb = new StringBuilder();
		Timestamp timeStamp = new Timestamp(new java.util.Date().getTime());

		int callingLevel = BASE_LOGGER_CALLING_LEVEL + callingLevelDelta;

		// TODO: Bo, actually this is still incorrect. It does not depend on
		// what is the superchannel, but if the intermediate function call with
		// one less argument has been used.
		if (!superChannel.equals(DEFAULT_SUPERCHANNEL)) {
			callingLevel -= 1;
		}
		String unqualifiedClassName = getUnqualifiedClassName(Thread
				.currentThread().getStackTrace()[callingLevel].getClassName());

		sb.append("[");
		sb.append(timeStamp);
		sb.append(" " + unqualifiedClassName);
		sb.append(" " + channel + "]");
		sb.append(" " + msg);
		return sb.toString();
	}

	private static String getUnqualifiedClassName(String qualifiedClassName) {
		String[] token = qualifiedClassName.split("\\.");
		return token[token.length - 1];
	}

	private static LogMessageType getChannelVerbosity(String channel,
			Map<String, LogMessageType> verbositySpec) {
		if (verbositySpec.containsKey(channel)) {
			return verbositySpec.get(channel);
		} else {
			return DEFAULT_VERBOSITY;
		}
	}
}
