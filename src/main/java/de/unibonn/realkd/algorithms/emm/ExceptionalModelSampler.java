/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.emm;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.common.DataTableParameter;
import de.unibonn.realkd.algorithms.common.MatchingPropositionalLogicParameter;
import de.unibonn.realkd.algorithms.common.PatternOptimizationFunction;
import de.unibonn.realkd.algorithms.sampling.ConsaptBasedSamplingMiner;
import de.unibonn.realkd.algorithms.sampling.DiscriminativityDistributionFactory;
import de.unibonn.realkd.algorithms.sampling.DistributionFactory;
import de.unibonn.realkd.algorithms.sampling.FrequencyDistributionFactory;
import de.unibonn.realkd.algorithms.sampling.SamplingParameters;
import de.unibonn.realkd.algorithms.sampling.SinglePatternPostProcessor;
import de.unibonn.realkd.algorithms.sampling.WeightedDiscriminativityDistributionFactory;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.emm.ExceptionalModelMining;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.emm.ModelDistanceMeasurementProcedure;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.models.DefaultModel;
import de.unibonn.realkd.patterns.models.Model;
import de.unibonn.realkd.patterns.models.ModelFactory;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroups;

/**
 * Wraps a Consapt sampling miner for randomized exceptional subgroup discovery.
 * 
 * @author Mario Boley
 * 
 * @author Sandy Moens
 * 
 * @since 0.1.0
 * 
 * @version 0.3.0
 *
 */
public final class ExceptionalModelSampler extends AbstractMiningAlgorithm {

	private final Parameter<PropositionalLogic> propositionalLogicParameter;
	private final Parameter<List<Attribute<?>>> targets;
	private final ModelClassParameter modelClassParameter;
	private final RangeEnumerableParameter<ModelDistanceMeasurementProcedure> distanceFunctionParameter;
	private final Parameter<DataTable> dataTableParameter;
	private final Parameter<PatternOptimizationFunction> targetFunctionParameter;
	private final Parameter<Set<Attribute<?>>> descriptorAttributesParameter;
	private final Parameter<DistributionFactory> distributionFactoryParameter;
	private final Parameter<SinglePatternPostProcessor> postProcessorParameter;
	private final Parameter<Integer> numberOfResultsParameter;
	private final Parameter<Integer> numberOfSeedsParameter;

	private ConsaptBasedSamplingMiner<ExceptionalModelPattern> sampler = null;

	private final List<DistributionFactory> distributionOptions;
	private final FrequencyDistributionFactory frequencyOption;
	private final DiscriminativityDistributionFactory discriminativityOption;
	// private final DistributionOption freqTimesDiscriminativityOption;
	// private final DistributionOption posFreqSquareTimesNegInvFreqOption;
	// private final DistributionOption discriminativitySquareOption;
	private final WeightedDiscriminativityDistributionFactory weightedDiscriminativityOption;

	public ExceptionalModelSampler(Workspace workspace) {
		this.dataTableParameter = new DataTableParameter(workspace);
		this.propositionalLogicParameter = new MatchingPropositionalLogicParameter(workspace, dataTableParameter);
		this.targets = EMMParameters.getEMMTargetAttributesParameter(dataTableParameter);
		this.modelClassParameter = new ModelClassParameter(targets);
		this.distanceFunctionParameter = EMMParameters.distanceFunctionParameter(modelClassParameter);
		this.descriptorAttributesParameter = EMMParameters.getEMMDescriptorAttributesParameter(dataTableParameter,
				targets);

		// TODO:
		// filter out object if at least one target
		// value is missing
		Predicate<Proposition> targetFilter = new TargetAttributePropositionFilter(dataTableParameter, targets);
		Predicate<Proposition> additionalPropFilter = prop -> !((prop instanceof AttributeBasedProposition)
				&& descriptorAttributesParameter.current()
						.contains(((AttributeBasedProposition<?>) prop).getAttribute()));

		Predicate<Proposition> propositionFilter = additionalPropFilter.and(targetFilter);

		frequencyOption = new FrequencyDistributionFactory(propositionFilter);
		discriminativityOption = new DiscriminativityDistributionFactory(dataTableParameter, targets,
				propositionFilter);
		// freqTimesDiscriminativityOption = option(new
		// DiscriminativityDistributionFactory(dataTableParameter, targets,
		// 1, 1, 1, propositionFilter));
		// posFreqSquareTimesNegInvFreqOption = option(new
		// DiscriminativityDistributionFactory(dataTableParameter, targets,
		// 0, 2, 1, propositionFilter));
		// discriminativitySquareOption = option(new
		// DiscriminativityDistributionFactory(dataTableParameter, targets,
		// 0, 2, 2, propositionFilter));
		weightedDiscriminativityOption = new WeightedDiscriminativityDistributionFactory(dataTableParameter, targets,
				propositionalLogicParameter, 0, 2, 1, new RowWeightComputer.UniformRowWeightComputer(),
				propositionFilter);
		this.distributionOptions = ImmutableList.of(frequencyOption, discriminativityOption,
				weightedDiscriminativityOption);

		this.distributionFactoryParameter = Parameters.rangeEnumerableParameter("Seed distribution",
				"The probability distribution on the pattern space that is used to generate random seeds for EMM pattern search",
				DistributionFactory.class, () -> this.distributionOptions, targets, propositionalLogicParameter,
				descriptorAttributesParameter);
		this.targetFunctionParameter = EMMParameters.emmTargetFunctionParameter(modelClassParameter);
		this.postProcessorParameter = SamplingParameters.postProcessingParameter();
		this.numberOfResultsParameter = SamplingParameters.numberOfResultsParameter();
		this.numberOfSeedsParameter = SamplingParameters.numberOfSeedsParameter(numberOfResultsParameter);

		registerParameter(dataTableParameter);
		registerParameter(targets);
		registerParameter(modelClassParameter);
		registerParameter(distanceFunctionParameter);
		registerParameter(propositionalLogicParameter);
		registerParameter(descriptorAttributesParameter);
		registerParameter(distributionFactoryParameter);
		registerParameter(numberOfResultsParameter);
		registerParameter(numberOfSeedsParameter);
		registerParameter(postProcessorParameter);
		registerParameter(targetFunctionParameter);
	}

	@Override
	protected Collection<Pattern> concreteCall() {

		final DataTable table = dataTableParameter.current();
		final List<Attribute<?>> targetAttr = targets.current();
		final ModelFactory modelFactory = modelClassParameter.current().get();
		final Model globalModel = modelFactory.getModel(table, targetAttr);
		final ModelDistanceMeasurementProcedure distanceMeasurementProc = distanceFunctionParameter.current();

		Function<LogicalDescriptor, ExceptionalModelPattern> toPattern = d -> {
			DefaultModel localModel = modelFactory.getModel(table, targetAttr, d.indices());
			return ExceptionalModelMining.emmPattern(Subgroups.subgroup(d, table, targetAttr, globalModel, localModel),
					distanceMeasurementProc, ImmutableList.of());
		};

		BiFunction<LogicalDescriptor, Pattern, Pattern> toPatternWithPrevious = (d, p) -> {
			if (p instanceof ExceptionalModelPattern
					&& ((Subgroup) p.descriptor()).extensionDescriptor().indices().equals(d.indices())) {
				return ExceptionalModelMining.emmPattern(
						Subgroups.subgroup(d, table, targetAttr, globalModel, ((Subgroup) p.descriptor()).localModel()),
						distanceMeasurementProc, ImmutableList.of());
			}
			DefaultModel localModel = modelFactory.getModel(table, targetAttr, d.indices());
			return ExceptionalModelMining.emmPattern(Subgroups.subgroup(d, table, targetAttr, globalModel, localModel),
					distanceMeasurementProc, ImmutableList.of());
		};

		sampler = new ConsaptBasedSamplingMiner<ExceptionalModelPattern>(
				// new
				// ParameterBoundLogicalDescriptorToEmmPatternMap(dataTableParameter,
				// targets, modelClassParameter,
				// distanceFunctionParameter),
				toPattern, toPatternWithPrevious,
				pattern -> ((ExceptionalModelPattern) pattern).descriptor().extensionDescriptor(),
				propositionalLogicParameter.current(),
				distributionFactoryParameter.current().getDistribution(propositionalLogicParameter.current()),
				targetFunctionParameter.current(), postProcessorParameter.current(),
				numberOfResultsParameter.current(), numberOfSeedsParameter.current());

		Collection<Pattern> result = sampler.call();
		sampler = null;
		return result;
	}

	@Override
	protected void onStopRequest() {
		if (sampler != null) {
			sampler.requestStop();
		}
	}

	public void setDiscriminativityOption() {
		this.distributionFactoryParameter.set(discriminativityOption);
	}

	public DiscriminativityDistributionFactory discriminativityOption() {
		return discriminativityOption;
	}

	public void setWeightedDiscriminativityOption() {
		this.distributionFactoryParameter.set(weightedDiscriminativityOption);
	}

	public DistributionFactory getDistributionFactory() {
		return distributionFactoryParameter.current();
	}

	public List<Attribute<?>> getTargetAttributes() {
		return this.targets.current();
	}

	@Override
	public String toString() {
		return "EMMSampler|" + getDistributionFactory() + "|" + targetFunctionParameter.current();
	}

	@Override
	public String getName() {
		return "2-Step Subgroup Sampling";
	}

	@Override
	public String getDescription() {
		return "2-Step direct pattern sampling for discovering outstanding subgroups.";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.EXCEPTIONAL_SUBGROUP_DISCOVERY;
	}

	public Parameter<PropositionalLogic> getPropositionalLogicParameter() {
		return this.propositionalLogicParameter;
	}

	public Parameter<DataTable> getDataTableParameter() {
		return dataTableParameter;
	}

	public Parameter<List<Attribute<?>>> getTargetAttributesParameter() {
		return targets;
	}

	public ModelClassParameter getModelClassParameter() {
		return modelClassParameter;
	}

	public RangeEnumerableParameter<ModelDistanceMeasurementProcedure> getModelDistanceFunctionParameter() {
		return distanceFunctionParameter;
	}

	public void setNumberOfResults(int numberOfResults) {
		numberOfResultsParameter.set(numberOfResults);
	}

	public Parameter<DistributionFactory> getDistributionFactoryParameter() {
		return this.distributionFactoryParameter;
	}

	public Parameter<Set<Attribute<?>>> getDescriptionAttributesParameter() {
		return descriptorAttributesParameter;
	}

	public Parameter<?> getTargetFunctionParameter() {
		return targetFunctionParameter;
	}

	public Parameter<Integer> getNumberOfResultsParameter() {
		return numberOfResultsParameter;
	}
	
	public Parameter<Integer> getNumberOfSeedsParameter() {
		return numberOfSeedsParameter;
	}

}
