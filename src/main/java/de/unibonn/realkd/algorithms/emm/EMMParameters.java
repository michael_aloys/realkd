/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.algorithms.emm;

import static com.google.common.base.Preconditions.checkArgument;
import static de.unibonn.realkd.patterns.emm.ExceptionalModelMining.MANHATTEN_MEAN_DEV_MEASUREMENT_PROCEDURE;
import static de.unibonn.realkd.patterns.emm.ExceptionalModelMining.MEDIAN_DEV_MEASUREMENT_PROCEDURE;
import static de.unibonn.realkd.patterns.emm.ExceptionalModelMining.NEG_MEAN_DEV_PROCEDURE;
import static de.unibonn.realkd.patterns.emm.ExceptionalModelMining.POS_MEAN_DEV_PROCEDURE;
import static de.unibonn.realkd.util.Lists.listOrEmpty;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.realkd.algorithms.common.MiningParameters;
import de.unibonn.realkd.algorithms.common.PatternOptimizationFunction;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter.CollectionComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.common.parameter.ValueValidator;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.emm.ExceptionalModelMining;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.emm.ModelDistanceMeasurementProcedure;
import de.unibonn.realkd.patterns.models.cumulative.EmpiricalCumulativeDistributionFactory;
import de.unibonn.realkd.patterns.models.gaussian.GaussianModelFactory;
import de.unibonn.realkd.patterns.models.mean.MetricEmpiricalDistributionFactory;
import de.unibonn.realkd.patterns.models.regression.LeastSquareRegressionModelFactory;
import de.unibonn.realkd.patterns.models.regression.TheilSenLinearRegressionModelFactory;
import de.unibonn.realkd.patterns.models.table.ContingencyTableModelFactory;
import de.unibonn.realkd.patterns.models.weibull.FixedShapeWeibullModelFactory;

/**
 * Utility class that provides factory methods for exceptional model mining
 * parameters.
 * 
 * @author Mario Boley
 *
 * @since 0.1.0
 * 
 * @version 0.3.2
 * 
 * 
 */
public class EMMParameters {

	/**
	 * Convenience method for providing an attribute selection parameter for
	 * choosing a non-empty collection of target attributes in exceptional model
	 * mining with default name and description.
	 * 
	 * @see {@link MiningParameters#getAttributeSelectionParameter(String, String, Parameter, Predicate)}
	 * 
	 */
	public static SubCollectionParameter<Attribute<? extends Object>, List<Attribute<? extends Object>>> getEMMTargetAttributesParameter(
			Parameter<DataTable> dataTableParameter, Predicate<Attribute<?>> filterPredicate) {
		ValueValidator<List<Attribute<? extends Object>>> validator = attributeList -> !attributeList.isEmpty()
				&& dataTableParameter.current().getAttributes().containsAll(attributeList);
		return MiningParameters.getAttributeSelectionParameter("Target attributes",
				"List of attributes for which patterns should show special characteristics", dataTableParameter,
				filterPredicate, validator);
	}

	/**
	 * Convenience method for creating attribute selection parameter for EMM
	 * targets with no attribute filter predicate.
	 * 
	 * @see {@link #getEMMTargetAttributesParameter(Parameter, Predicate)
	 * 
	 */
	public static Parameter<List<Attribute<? extends Object>>> getEMMTargetAttributesParameter(
			Parameter<DataTable> dataTableParameter) {
		return getEMMTargetAttributesParameter(dataTableParameter, attribute -> true);
	}

	/**
	 * <p>
	 * Creates a parameter for filtering out possible extension descriptor
	 * attributes from some data table. The valid range is given by all
	 * attributes that do not relate to any selected target attribute (i.e., all
	 * attributes that are neither an target attribute or a part of a joint
	 * macro-attribute with one), because propositions relating to those
	 * attributes are supposed to be filtered out anyway by EMM algorithms.
	 * </p>
	 * 
	 * @param dataTableParameter
	 *            parameter that holds selection of data table and, thus, the
	 *            underlying range of all available attributes
	 * 
	 * @param targetAttributesParameter
	 *            parameter that holds selection of target attributes, which
	 *            induce filter
	 * 
	 */
	public static SubCollectionParameter<Attribute<?>, Set<Attribute<?>>> getEMMDescriptorAttributesParameter(
			Parameter<DataTable> dataTableParameter, Parameter<List<Attribute<?>>> targetAttributesParameter) {

		CollectionComputer<Set<Attribute<?>>> collectionComputer = new CollectionComputer<Set<Attribute<?>>>() {

			@Override
			public Set<Attribute<?>> computeCollection() {
				DataTable dataTable = dataTableParameter.current();
				List<Attribute<?>> targetAttributes = targetAttributesParameter.current();
				Predicate<? super Attribute<?>> filterPredicate = attribute -> !targetAttributes.contains(attribute)
						&& !dataTable.isPartOfMacroAttributeWithAtLeastOneOf(attribute, targetAttributes);

				return dataTable.getAttributes().stream().filter(filterPredicate)
						.collect(Collectors.toCollection(LinkedHashSet::new));
			}

		};

		Supplier<Set<Attribute<?>>> initializer = () -> ImmutableSet.of();

		return DefaultSubCollectionParameter.subSetParameter("Attribute filter",
				"Attributes not to be used in descriptors.", collectionComputer, initializer, dataTableParameter,
				targetAttributesParameter);
	}

	/**
	 * @return the name of the parameters created by
	 *         {@link #distanceFunctionParameter(ModelClassParameter)}
	 */
	public static String distanceFunctionParameterName() {
		return "Deviation Measure";
	}

	/**
	 * 
	 * @param modelFactory
	 *            the model factory parameter that the resulting distance
	 *            function parameter depends on
	 * @return a parameter for choosing a model distance function that matches
	 *         an already selected model class
	 */
	public static RangeEnumerableParameter<ModelDistanceMeasurementProcedure> distanceFunctionParameter(
			final ModelClassParameter modelFactory) {

		Class<ModelDistanceMeasurementProcedure> type = ModelDistanceMeasurementProcedure.class;
		String description = "The function for measuring the deviation of the local population from the global population.";

		List<Function<ModelClassParameter, List<ModelDistanceMeasurementProcedure>>> providers = ImmutableList.of(
				factory -> listOrEmpty(factory.current().get() instanceof MetricEmpiricalDistributionFactory,
						MANHATTEN_MEAN_DEV_MEASUREMENT_PROCEDURE),
				factory -> listOrEmpty(
						factory.current().get() instanceof MetricEmpiricalDistributionFactory
								&& factory.attributes().current().size() == 1,
						POS_MEAN_DEV_PROCEDURE, NEG_MEAN_DEV_PROCEDURE, MEDIAN_DEV_MEASUREMENT_PROCEDURE),
				factory -> listOrEmpty(factory.current().get() instanceof ContingencyTableModelFactory,
						ExceptionalModelMining.TVD_MEASUREMENT_PROCEDURE, ExceptionalModelMining.HELLINGER_OF_TABLES),
				factory -> listOrEmpty(factory.current().get() instanceof GaussianModelFactory,
						ExceptionalModelMining.TVD_MEASUREMENT_PROCEDURE,
						ExceptionalModelMining.HELLINGER_OF_GAUSSIANS_MEASUREMENT_PROCEDURE,
						ExceptionalModelMining.KLD_OF_GAUSSIANS_MEASUREMENT_PROCEDURE),
				factory -> listOrEmpty(factory.current().get() instanceof FixedShapeWeibullModelFactory,
						ExceptionalModelMining.HELLINGER_DIST_FOR_IDENTICALLY_SHAPED_WEIBULL_DISTRIBUTIONS),
				factory -> listOrEmpty(factory.current().get() instanceof EmpiricalCumulativeDistributionFactory,
						ExceptionalModelMining.CJS_DIV_PROCEDURE, ExceptionalModelMining.EMP_KOLMOGOROV_SMIRNOV),
				factory -> listOrEmpty(
						factory.current().get() instanceof TheilSenLinearRegressionModelFactory
								|| factory.current().get() instanceof LeastSquareRegressionModelFactory,
						ExceptionalModelMining.COS_DIST_PROCEDURE));

		String name = distanceFunctionParameterName();
		return Parameters.dependentRangeEnumerableParameter(name, description, type, modelFactory, providers);
	}

	public static Parameter<PatternOptimizationFunction> emmTargetFunctionParameter(ModelClassParameter modelFactory) {
		List<Function<ModelClassParameter, List<PatternOptimizationFunction>>> providers = ImmutableList
				.of(m -> EMM_TARGET_FUNCTION_BASE_OPTIONS,
						m -> listOrEmpty(
								m.current() == m.empirical_distribution_option && m.attributes().current().size() == 1,
								DISPERSION_CORRECTED_TARGET_FUNCTION));

		return Parameters.dependentRangeEnumerableParameter(EMM_TARGET_FUNCTION_PARAMETER_NAME,
				"The function which will be optimized by the algorithm", PatternOptimizationFunction.class,
				modelFactory, providers);
		// return
		// Parameters.rangeEnumerableParameter(EMMParameters.EMM_TARGET_FUNCTION_PARAMETER_NAME,
		// "The function which will be optimized by the algorithm",
		// PatternOptimizationFunction.class,
		// ()->EMMParameters.EMM_TARGET_FUNCTION_BASE_OPTIONS);
	}

	public static final String EMM_TARGET_FUNCTION_PARAMETER_NAME = "Target function";

	private static final PatternOptimizationFunction DISPERSION_CORRECTED_TARGET_FUNCTION = new PatternOptimizationFunction() {

		@Override
		public Double apply(Pattern pattern) {
			double aamdGain = (pattern.value(QualityMeasureId.GLOBAL_AAMD) - pattern.value(QualityMeasureId.LOCAL_AAMD))
					/ pattern.value(QualityMeasureId.GLOBAL_AAMD);
			return ((ExceptionalModelPattern) pattern).value(QualityMeasureId.FREQUENCY) * aamdGain
					* Math.max(pattern.value(((ExceptionalModelPattern) pattern).getDeviationMeasure()), 0);
		}

		@Override
		public String toString() {
			return "frequency times aamd-gain times deviation";
		}

	};

	static final List<PatternOptimizationFunction> EMM_TARGET_FUNCTION_BASE_OPTIONS = ImmutableList
			.of(new PatternOptimizationFunction() {

				@Override
				public Double apply(Pattern pattern) {
					checkArgument(
							(pattern instanceof ExceptionalModelPattern)
									&& pattern.hasMeasure(QualityMeasureId.FREQUENCY),
							"Target function only defined for exceptional model patterns with frequency.");

					return ((ExceptionalModelPattern) pattern).value(QualityMeasureId.FREQUENCY)
							* Math.max(pattern.value(((ExceptionalModelPattern) pattern).getDeviationMeasure()), 0);
				}

				@Override
				public String toString() {
					return "frequency times deviation";
				}

			}, new PatternOptimizationFunction() {

				@Override
				public Double apply(Pattern pattern) {
					checkArgument(
							(pattern instanceof ExceptionalModelPattern)
									&& pattern.hasMeasure(QualityMeasureId.FREQUENCY),
							"Target function only defined for exceptional model patterns with frequency.");

					return Math.sqrt(pattern.value(QualityMeasureId.FREQUENCY))
							* Math.max(pattern.value(((ExceptionalModelPattern) pattern).getDeviationMeasure()), 0);
				}

				@Override
				public String toString() {
					return "sqrt(frequency) times deviation";
				}

			}, new PatternOptimizationFunction() {

				private double entropy(double p) {
					return (-1 * p * Math.log(p) - (1 - p) * Math.log(1 - p)) / Math.log(2);
				}

				@Override
				public Double apply(Pattern pattern) {
					checkArgument(
							(pattern instanceof ExceptionalModelPattern)
									&& pattern.hasMeasure(QualityMeasureId.FREQUENCY),
							"Target function only defined for exceptional model patterns with frequency.");

					double freq = pattern.value(QualityMeasureId.FREQUENCY);
					return entropy(freq)
							* Math.max(pattern.value(((ExceptionalModelPattern) pattern).getDeviationMeasure()), 0);
				}

				@Override
				public String toString() {
					return "H(frequency) times deviation";
				}

			});
	// ,
	// new PatternOptimizationFunction() {
	//
	// @Override
	// public Double apply(Pattern pattern) {
	// checkArgument((pattern instanceof ExceptionalModelPattern),
	// "Target function only defined for exceptional model patterns.");
	//
	// return Math.max(pattern.value(((ExceptionalModelPattern)
	// pattern).getDeviationMeasure()), 0);
	// }
	//
	// @Override
	// public String toString() {
	// return "deviation";
	// }
	//
	// });

}
