package de.unibonn.realkd.algorithms.common;

import static com.google.common.base.Preconditions.checkArgument;
import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalLogic;
import de.unibonn.realkd.patterns.Measure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

public interface PatternConstraint {

	public abstract boolean satisfies(Pattern pattern);

	public static PatternConstraint POSITIVE_FREQUENCY = new PatternConstraint() {

		@Override
		public boolean satisfies(Pattern pattern) {
			checkArgument(pattern.hasMeasure(QualityMeasureId.FREQUENCY),
					"Constraint only defined for patterns with frequency.");
			// if (!(pattern instanceof LogicallyDescribedLocalPattern)) {
			// throw new IllegalArgumentException(
			// "Constraint only defined for logically described pattern");
			// }
			return pattern.value(QualityMeasureId.FREQUENCY) > 0;
		}
	};

	public static PatternConstraint DESCRIPTOR_DOES_NOT_CONTAIN_TWO_ELEMENTS_REFERRING_TO_SAME_META_ATTRIBUTE = new PatternConstraint() {

		@Override
		public boolean satisfies(Pattern pattern) {
			if (!(pattern.descriptor() instanceof LogicalDescriptor)) {
				throw new IllegalArgumentException(
						"Constraint only defined for logically described pattern");
			}
			// LogicallyDescribedLocalPattern logicalPattern =
			// (LogicallyDescribedLocalPattern) pattern;
			LogicalDescriptor description = (LogicalDescriptor) pattern
					.descriptor();// logicalPattern.getDescription();
			for (int i = 0; i < description.getElements().size(); i++) {
				for (int j = i + 1; j < description.getElements().size(); j++) {
					PropositionalLogic logic = description
							.getPropositionalLogic();
					Proposition firstElement = description.getElement(i);
					Proposition secondElement = description.getElement(j);
					if (logic instanceof TableBasedPropositionalLogic
							&& firstElement instanceof AttributeBasedProposition<?>
							&& secondElement instanceof AttributeBasedProposition<?>
							&& ((TableBasedPropositionalLogic) logic)
									.getDatatable().isPartOfMacroAttributeWith(
											((AttributeBasedProposition<?>) firstElement).getAttribute(),
											((AttributeBasedProposition<?>) secondElement).getAttribute())) {

						return false;

					}
				}
			}
			return true;
		}
	};

	public static class MinimumMeasureValeConstraint implements
			PatternConstraint {

		private final double threshold;

		private final Measure measure;

		public MinimumMeasureValeConstraint(
				Measure interestingnessMeasure, double threshold) {
			this.threshold = threshold;
			this.measure = interestingnessMeasure;
		}

		@Override
		public boolean satisfies(Pattern pattern) {
			if (!pattern.hasMeasure(measure)) {
				throw new IllegalArgumentException(
						"Constraint only defined for patterns that have "
								+ measure.getName());
			}
			return pattern.value(measure) >= threshold;
		}

	}

}
