package de.unibonn.realkd.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Strings {
	
	private Strings() {
		; //not to be instantiated
	}

	/**
	 * Converts a string in the form [e1, e2, e3,...] to a list of strings with
	 * entries 'e1, e2, e3,...' the elements of which are trimmed of leading and
	 * trailing white-spaces.
	 * 
	 */
	public static List<String> jsonArrayToStringList(String argument) {
		argument = argument.substring(1, argument.length() - 1);
		if (argument.trim().length() == 0) {
			return Arrays.asList();
		}
		String[] split = argument.split(",");
		List<String> result = new ArrayList<>();
		for (String string : split) {
			String trimmed = string.trim();
			result.add(trimmed);
		}
		return result;
	}

	/**
	 * 
	 * @param input
	 *            the input string
	 * @param i
	 *            the number of characters to be chopped off the end
	 * @return input string minus the last i characters
	 */
	public static String chop(String input, int i) {
		return input.substring(0, input.length() - i);
	}

}
