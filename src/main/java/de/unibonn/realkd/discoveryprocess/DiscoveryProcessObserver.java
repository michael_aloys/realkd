package de.unibonn.realkd.discoveryprocess;

import de.unibonn.realkd.patterns.Pattern;

/** 
 * Interface for all classes that need to receive updates from DiscoveryProcess, e.g., ModelTrainers.
 * 
 * @author mboley
 *
 */
public interface DiscoveryProcessObserver {
	
	public void justBeganNewRound();
	
	public void roundEnded();
	
	public void markAsSeen(Pattern p);
	
	public void aboutToSavePattern(Pattern p);
	
	public void aboutToDeletePatternFromCandidates(Pattern p);
	
	public void aboutToDeletePatternFromResults(Pattern p);
	
}
