package de.unibonn.realkd.visualization.pattern;

import java.util.List;
import java.util.Set;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.DefaultCategoricAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.visualization.JFChartPainter;
import de.unibonn.realkd.visualization.ReducedEnglishDecimalFormat;

public class TargetHistogram implements PatternVisualization {

	private static final String SUBGROUP_LABEL = "subgroup";

	private static final String GLOBAL_LABEL = "global";

	TargetHistogram() {
		;
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		return (pattern.descriptor() instanceof Subgroup
				&& ((Subgroup) pattern.descriptor()).targetAttributes().size() == 1);
	}

	@Override
	public JFreeChart draw(Pattern pattern) {
		JFChartPainter painter = JFChartPainter.getInstance();

		Data data = new Data();

		Attribute<?> targetAttribute = ((Subgroup) pattern.descriptor()).targetAttributes().get(0);
		if (targetAttribute instanceof MetricAttribute) {
			data = getNumericHistogramSpecs((MetricAttribute) targetAttribute, pattern);
		} else if (targetAttribute instanceof DefaultCategoricAttribute) {
			data = getCategoricHistgramSpecs((DefaultCategoricAttribute) targetAttribute, pattern);
		}

		return painter.createLayeredBarChart("", data.series, data.column, data.values);
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}

	private static double[] getNumericHistogramValues(Subgroup descriptor, int numOfBins) {
		double[] res = new double[2 * numOfBins];

		MetricAttribute attr = ((MetricAttribute) descriptor.targetAttributes().get(0));
		double range = attr.max() - attr.min();

		Set<Integer> supportSet = descriptor.indices();

		List<Integer> rowIndices = attr.sortedNonMissingRowIndices();
		int pos = 0;
		for (int i = 0; i < numOfBins; i++) {
			double bucketBound = attr.min() + (i + 1) * range / (double) numOfBins;
			res[i + numOfBins] = 0.0;
			while (pos < rowIndices.size() && ((MetricAttribute) attr).value(rowIndices.get(pos)) <= bucketBound) {
				res[i + numOfBins] += 1.0;
				if (supportSet.contains(rowIndices.get(pos))) {
					res[i] += 1.0;
				}
				pos += 1;
			}
			res[i + numOfBins] /= rowIndices.size();
			res[i] /= supportSet.size();
		}

		return res;
	}

	private static int getNumberOfBinsForNumeric(MetricAttribute attribute) {
		return (int) Math.round(Math.ceil(Math.log(attribute.numberOfNonMissingValues()) / Math.log(2)));
	}

	private static class Data {
		String[] series;
		String[] column;
		double[] values;
	}

	private static Data getCategoricHistgramSpecs(DefaultCategoricAttribute targetAttribute, Pattern pattern) {
		int numberOfCategories = ((DefaultCategoricAttribute) targetAttribute).categories().size();
		Data data = new Data();
		data.series = new String[numberOfCategories * 2];
		data.column = new String[numberOfCategories * 2];
		data.values = new double[numberOfCategories * 2];
		Set<Integer> supportSet = (pattern.descriptor() instanceof SubPopulationDescriptor)
				? ((SubPopulationDescriptor) pattern.descriptor()).indices() : pattern.population().objectIds();
		// Set<Integer> supportSet = pattern.getSupportSet();

		List<Double> CategoryFrequenciesOnRows = ((DefaultCategoricAttribute) targetAttribute)
				.getCategoryFrequenciesOnRows(supportSet);
		List<Double> CategoryFrequencies = ((DefaultCategoricAttribute) targetAttribute).categoryFrequencies();

		for (int i = 0; i < numberOfCategories; i++) {
			data.series[i] = SUBGROUP_LABEL;
			data.series[numberOfCategories + i] = GLOBAL_LABEL;

			data.column[i] = ((DefaultCategoricAttribute) targetAttribute).categories().get(i);
			data.column[numberOfCategories + i] = ((DefaultCategoricAttribute) targetAttribute).categories().get(i);

			data.values[i] = CategoryFrequenciesOnRows.get(i);
			data.values[numberOfCategories + i] = CategoryFrequencies.get(i);
		}
		return data;
	}

	private static Data getNumericHistogramSpecs(MetricAttribute targetAttribute, Pattern pattern) {
		int numOfBins = getNumberOfBinsForNumeric(targetAttribute);
		Data data = new Data();
		data.series = new String[numOfBins * 2];
		data.column = new String[numOfBins * 2];

		ReducedEnglishDecimalFormat formatter = new ReducedEnglishDecimalFormat();
		double range = targetAttribute.max() - targetAttribute.min();
		for (int i = 0; i < numOfBins; i++) {
			data.series[i] = SUBGROUP_LABEL;
			data.series[numOfBins + i] = GLOBAL_LABEL;

			data.column[i] = formatter.format(targetAttribute.min() + (i + 1) * range / (double) numOfBins);
			data.column[numOfBins + i] = formatter.format(targetAttribute.min() + (i + 1) * range / (double) numOfBins);
		}

		data.values = getNumericHistogramValues((Subgroup) pattern.descriptor(), numOfBins);
		return data;
	}

}
