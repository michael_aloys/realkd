package de.unibonn.realkd.visualization.pattern;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.jfree.chart.JFreeChart;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.visualization.JFChartPainter;

public class BoxPlotTargetShiftVisualization implements PatternVisualization {

	/**
	 * constructor only to be invoked from Visualization Register
	 */
	BoxPlotTargetShiftVisualization() {
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		if (!(pattern.descriptor() instanceof Subgroup)) {
			return false;
		}

		Subgroup descriptor = (Subgroup) pattern.descriptor();

		return (descriptor.targetAttributes().size() == 1
				&& descriptor.targetAttributes().get(0) instanceof MetricAttribute);
	}

	@Override
	public JFreeChart draw(Pattern pattern) {
		Subgroup descriptor = (Subgroup) pattern.descriptor();
		JFChartPainter painter = JFChartPainter.getInstance();
		MetricAttribute targetAttribute = (MetricAttribute) descriptor.targetAttributes().get(0);
		Set<Integer> supportSet = descriptor.indices();
		List<Double> subgroupValues = supportSet.stream().filter(i -> !(targetAttribute.valueMissing(i)))
				.map(i -> targetAttribute.value(i)).collect(Collectors.toList());
		List<Double> globalValues=ImmutableList.copyOf(targetAttribute.nonMissingValues());

		DefaultBoxAndWhiskerCategoryDataset boxDataset = new DefaultBoxAndWhiskerCategoryDataset();
		boxDataset.add(subgroupValues, "subgroup", "");
		boxDataset.add(globalValues, "global", "");

		return painter.createBoxPlotChart("", boxDataset, targetAttribute.name());
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}

}
