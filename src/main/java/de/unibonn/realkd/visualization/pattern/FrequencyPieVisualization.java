package de.unibonn.realkd.visualization.pattern;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.visualization.JFChartPainter;

public class FrequencyPieVisualization implements PatternVisualization {

	/**
	 * constructor only to be invoked from Visualization Register
	 */
	FrequencyPieVisualization() {
		;
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		if (pattern instanceof Association) {
			return false;
		}
		else if (pattern.hasMeasure(QualityMeasureId.FREQUENCY)) {
			return true;
		}
		else {
			return false;
		}
	}


	@Override
	public JFreeChart draw(Pattern pattern) {

		JFChartPainter painter = JFChartPainter.getInstance();

		String[] items = { "Frequency", "" };
		double freqency = pattern.value(QualityMeasureId.FREQUENCY);
		double[] values = { freqency, 1 - freqency };
		boolean isPatternView = true;
		// String result;

		return painter.createPieChart("", items, values, isPatternView);
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}
	
}
