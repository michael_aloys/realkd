package de.unibonn.realkd.visualization.pattern;

import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.function.Function2D;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.models.UnivariateContinuousModel;
import de.unibonn.realkd.visualization.JFChartPainter;
import de.unibonn.realkd.visualization.UIStyle;

/**
 * Visualization of EMM patterns with univariate probabilistic models. Draws
 * density function of local and global model by sampling it on a regular grid
 * with 50 points from the empirical min to the empirical max value of the
 * target attribute.
 * 
 * @author Ruofang Xu
 * 
 * @since 0.0.1
 * 
 * @version 0.2.2
 *
 */
public class UnivariateDensityPlot implements PatternVisualization {
//
//	private static final String SUBGROUP_LABEL = "subgroup";
//
//	private static final String GLOBAL_LABEL = "global";

	/**
	 * constructor only to be invoked from Visualization Register
	 */
	UnivariateDensityPlot() {
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		return (pattern instanceof ExceptionalModelPattern
				&& ((ExceptionalModelPattern) pattern).descriptor()
						.referenceModel() instanceof UnivariateContinuousModel
				&& ((ExceptionalModelPattern) pattern).descriptor().indices().size() > 1);
	}

	@Override
	public JFreeChart draw(Pattern pattern) {
		UnivariateContinuousModel globalModel = (UnivariateContinuousModel) ((ExceptionalModelPattern) pattern)
				.descriptor().referenceModel();
		UnivariateContinuousModel localModel = (UnivariateContinuousModel) ((ExceptionalModelPattern) pattern)
				.descriptor().localModel();

		// double std = sqrt(model.getVariance());
		// double min = model.getMean() - 4 * std;
		// double max = model.getMean() + 4 * std;
		MetricAttribute target = (MetricAttribute) ((ExceptionalModelPattern) pattern).descriptor()
				.targetAttributes().get(0);

		return createGaussians("", ImmutableList.of(globalModel, localModel), target.min(), target.max(),
				((ExceptionalModelPattern) pattern).descriptor().targetAttributes().get(0).name());
	}
	
	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}

	private JFreeChart createGaussians(String title, List<UnivariateContinuousModel> models, double min, double max,
			String xAxisLabel) {
		XYSeriesCollection gaussians = new XYSeriesCollection();
		int i = 0;
		for (UnivariateContinuousModel model : models) {
			// if (model.getVariance() == 0) {
			// gaussians.addSeries(new XYSeries(""));
			// } else {

			Function2D normal = new Function2D() {
				@Override
				public double getValue(double x) {
					return model.getDensityFunction().apply(x);
				}
			};
			XYDataset dataset = DatasetUtilities.sampleFunction2D(normal, min, max, 50, "Normal" + i++);
			gaussians.addSeries(((XYSeriesCollection) dataset).getSeries(0));
			// }
		}
		final JFreeChart chart = ChartFactory.createXYLineChart(title, xAxisLabel, "", gaussians,
				PlotOrientation.VERTICAL, false, true, false);

		JFChartPainter.getInstance().formatJFreeChart(chart);

		XYPlot normalPlot = (XYPlot) chart.getPlot();
		normalPlot.getRenderer().setSeriesPaint(1, UIStyle.getInstance().localPaint);
		normalPlot.getRenderer().setSeriesPaint(0, UIStyle.getInstance().globalPaint);

		NumberAxis domainAxis = (NumberAxis) normalPlot.getDomainAxis();
		JFChartPainter.getInstance().formatAxis(domainAxis);

		NumberAxis rangeAxis = (NumberAxis) normalPlot.getRangeAxis();
		JFChartPainter.getInstance().formatAxis(rangeAxis);

		return chart;

		// ChartRenderingInfo info = new ChartRenderingInfo();
		//
		// return formatHtmlURL(session, chart, info);
	}

}
