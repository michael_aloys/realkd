package de.unibonn.realkd.visualization.pattern;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.visualization.JFChartPainter;

public class LiftLogProbabilityStackedBarChart implements PatternVisualization {

	LiftLogProbabilityStackedBarChart() {
		;
	}

	@Override
	public boolean isApplicable(Pattern pattern) {
		// return pattern instanceof Association;
		return (pattern.descriptor() instanceof LogicalDescriptor);
	}

	@Override
	public JFreeChart draw(Pattern pattern) {

		JFChartPainter painter = JFChartPainter.getInstance();

		// LogicalDescriptor descriptor = (LogicalDescriptor)
		// pattern.getDescriptor();

		LogicalDescriptor descriptor = (LogicalDescriptor) pattern
				.descriptor();
		PropositionalLogic propositionalLogic = descriptor
				.getPropositionalLogic();

		int length = descriptor.size(); // ((Association)
										// pattern).getIndividualFrequences().size();
		String series[] = new String[length + 1];
		String column[] = new String[length + 1];
		double[] values = new double[length + 1];

		// values[0] = Math.log(((Association) pattern).getFrequency());
		values[0] = Math.log(descriptor.indices().size()
				/ (double) propositionalLogic.population().size());
		series[0] = "s0";
		column[0] = "frequency";

		int i = 1;

		for (Proposition prop : descriptor.getElements()) {
			values[i] = Math.log((double) propositionalLogic.getSupportSet(
					prop.getId()).size()
					/ propositionalLogic.population().size());
			series[i] = "s" + i;
			column[i] = "expected freq.";
			i++;
		}

		return painter.createStackedBarChart("", series, column, values);
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}

}
