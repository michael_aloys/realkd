package de.unibonn.realkd.visualization.pattern;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;
import de.unibonn.realkd.patterns.TableSubspaceDescriptor;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.pmm.PureModelSubgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.visualization.JFChartPainter;

/**
 * A 2d-scatterplot that visualizes either the target attributes of a subgroup
 * pattern or all attributes that a pattern refers to (in case not a subgroup).
 * In both cases, the visualization is only applicable when there are exactly
 * two attributes considered.
 * 
 * @author rxu
 *
 */
public class TwoNumericAttributePatternScatterPlot implements PatternVisualization {

	@Override
	public boolean isApplicable(Pattern pattern) {
		if (!(pattern.descriptor() instanceof SubPopulationDescriptor)
				|| !(pattern.descriptor() instanceof TableSubspaceDescriptor)) {
			return false;
		}
		return checkExactlyTwoNumericAttributes(getRelevantAttributes(pattern));
	}

	private List<Attribute<?>> getRelevantAttributes(Pattern pattern) {
		if (pattern instanceof ExceptionalModelPattern || pattern instanceof PureModelSubgroup) {
			return ((Subgroup) pattern.descriptor()).targetAttributes();
		} else {
			return ((TableSubspaceDescriptor) pattern.descriptor()).getReferencedAttributes();
		}
	}

	private boolean checkExactlyTwoNumericAttributes(List<Attribute<?>> attributes) {
		if (attributes.size() != 2) {
			return false;
		}
		for (Attribute<?> attribute : attributes) {
			if (!(attribute instanceof MetricAttribute)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public JFreeChart draw(Pattern pattern) {

		JFChartPainter painter = JFChartPainter.getInstance();

		List<Attribute<?>> attributes = getRelevantAttributes(pattern);

		List<List<JFChartPainter.Point>> pointLists = newArrayList();
		SortedSet<Integer> complementRows = new TreeSet<>(pattern.population().objectIds());
		complementRows.removeAll(((SubPopulationDescriptor) pattern.descriptor()).indices());
		pointLists.add(JFChartPainter.createPoints(complementRows, (MetricAttribute) attributes.get(0),
				(MetricAttribute) attributes.get(1)));

		pointLists.add(JFChartPainter.createPoints(((SubPopulationDescriptor) pattern.descriptor()).indices(),
				(MetricAttribute) attributes.get(0), (MetricAttribute) attributes.get(1)));

		return painter.createPointCloud("", pointLists, attributes.get(0).name(), attributes.get(1).name());
	}

	@Override
	public JFreeChart drawDetailed(Pattern pattern) {
		return draw(pattern);
	}

}
